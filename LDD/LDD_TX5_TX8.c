/*
 * MA38435_TX5_TX8.c
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */

#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "CMIS_MSA.h"
#include "Master_I2C1_PB34.h"
#include "GD_FlahMap.h"
#include "LDD_TX5_TX8.h"
#include "MCU_GPIO_Customize_Define.h"


struct LDD_TX5_TX8_MEMORY   LDD_TX5_TX8_MEMORY_MAP;

//-----------------------------------------------------------------------------------------------------------------//
//-------------------------------------LDD_TX5_TX8 I2C Write-------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MALD38435_TX5_TX8_Control_Write_ALL()
{
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x03 , LDD_TX5_TX8_MEMORY_MAP.IO_Control   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x05 , LDD_TX5_TX8_MEMORY_MAP.I2C_ADR_MODE );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x10 , LDD_TX5_TX8_MEMORY_MAP.CH_MODE      );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x16 , LDD_TX5_TX8_MEMORY_MAP.LOS_MASK     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x19 , LDD_TX5_TX8_MEMORY_MAP.LOS_ALARM    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1B , LDD_TX5_TX8_MEMORY_MAP.TX_F_A_MASK  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1C , LDD_TX5_TX8_MEMORY_MAP.TX_F_Alarm   );
    //always use default value and ignore tx faulut
    Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1D , 0x7F                                      );
//	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1D , LDD_TX5_TX8_MEMORY_MAP.Ignore_TXF   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x21 , LDD_TX5_TX8_MEMORY_MAP.LOS_THRS     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x22 , LDD_TX5_TX8_MEMORY_MAP.LOS_HYST     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x25 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH0   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x26 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH1   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x27 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH2   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x28 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH3   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x40 , LDD_TX5_TX8_MEMORY_MAP.OUTPUT_MUTE  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x41 , LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4A , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH0  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4B , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH1  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4C , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH2  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4D , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH3  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4F , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH0  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x50 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH1  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x51 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH2  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x52 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH3  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x5E , LDD_TX5_TX8_MEMORY_MAP.IBurnIn      );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x5F , LDD_TX5_TX8_MEMORY_MAP.BurnIn_EN    );

	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x42 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH0    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x43 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH1    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x44 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH2    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x45 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH3    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x46 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH0     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x47 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH1     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x48 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH2     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x49 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH3     );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38435_TX5_TX8 I2C READ     ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MALD38435_TX5_TX8_Control_READ_ALL()
{
	// Read only Byte
	LDD_TX5_TX8_MEMORY_MAP.CHIP_ID        =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x00 );
	LDD_TX5_TX8_MEMORY_MAP.REVID          =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x01 );
	LDD_TX5_TX8_MEMORY_MAP.LOS_Status     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x17 );
	LDD_TX5_TX8_MEMORY_MAP.TX_Status      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x18 );
	LDD_TX5_TX8_MEMORY_MAP.Tx_Fault_State =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x1A );
	// Read/Write Byte
	LDD_TX5_TX8_MEMORY_MAP.IO_Control     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x03 );
	LDD_TX5_TX8_MEMORY_MAP.I2C_ADR_MODE   =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x05 );
	LDD_TX5_TX8_MEMORY_MAP.CH_MODE        =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x10 );
	LDD_TX5_TX8_MEMORY_MAP.LOS_MASK       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x16 );
	LDD_TX5_TX8_MEMORY_MAP.LOS_ALARM      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x19 );
	LDD_TX5_TX8_MEMORY_MAP.TX_F_A_MASK    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x1B );
	LDD_TX5_TX8_MEMORY_MAP.TX_F_Alarm     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x1C );
	LDD_TX5_TX8_MEMORY_MAP.Ignore_TXF     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x1D );

	LDD_TX5_TX8_MEMORY_MAP.LOS_THRS       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x21 );
	LDD_TX5_TX8_MEMORY_MAP.LOS_HYST       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x22 );
	LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH0     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x25 );
	LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH1     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x26 );
	LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH2     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x27 );
	LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH3     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x28 );
	LDD_TX5_TX8_MEMORY_MAP.OUTPUT_MUTE    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x40 );
	LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE     =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x41 );

	LDD_TX5_TX8_MEMORY_MAP.IBias_CH0      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x42 );
	LDD_TX5_TX8_MEMORY_MAP.IBias_CH1      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x43 );
	LDD_TX5_TX8_MEMORY_MAP.IBias_CH2      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x44 );
	LDD_TX5_TX8_MEMORY_MAP.IBias_CH3      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x45 );
	LDD_TX5_TX8_MEMORY_MAP.Imod_CH0       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x46 );
	LDD_TX5_TX8_MEMORY_MAP.Imod_CH1       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x47 );
	LDD_TX5_TX8_MEMORY_MAP.Imod_CH2       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x48 );
	LDD_TX5_TX8_MEMORY_MAP.Imod_CH3       =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x49 );

	LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH0    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x4A );
	LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH1    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x4B );
	LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH2    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x4C );
	LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH3    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x4D );
	LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH0    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x4F );
	LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH1    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x50 );
	LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH2    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x51 );
	LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH3    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x52 );

	LDD_TX5_TX8_MEMORY_MAP.IBurnIn        =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x5E );
	LDD_TX5_TX8_MEMORY_MAP.BurnIn_EN      =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x5F );
	LDD_TX5_TX8_MEMORY_MAP.ADC_Config0    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x60 );
	LDD_TX5_TX8_MEMORY_MAP.ADC_Config2    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x62 );

	LDD_TX5_TX8_MEMORY_MAP.Driver_ADC_CHSEL    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x91 );
	LDD_TX5_TX8_MEMORY_MAP.Driver_ADC_Measmt   =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x92 );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

//--------------------------------------//
//-----------Caller is Main.c-----------//
//--------------------------------------//

void LDD_TX5_TX8_PowerOn_Reset()
{
    Master_I2C1_ByteWrite_PB34(LDD_SlaveAddress , 0x02 , 0xAA);
    delay_1ms(1);
    Master_I2C1_ByteWrite_PB34(LDD_SlaveAddress , 0x02 , 0x00);
    delay_1ms(1);
}

void LDD_TX5_TX8_PowerOn_init()
{
    // Power on Disable Laser output
    Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1D , 0x7F );
    Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x41 , 0x0F );

	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x03 , LDD_TX5_TX8_MEMORY_MAP.IO_Control   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x05 , LDD_TX5_TX8_MEMORY_MAP.I2C_ADR_MODE );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x10 , LDD_TX5_TX8_MEMORY_MAP.CH_MODE      );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x16 , LDD_TX5_TX8_MEMORY_MAP.LOS_MASK     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x19 , LDD_TX5_TX8_MEMORY_MAP.LOS_ALARM    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1B , LDD_TX5_TX8_MEMORY_MAP.TX_F_A_MASK  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1C , LDD_TX5_TX8_MEMORY_MAP.TX_F_Alarm   );

	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x21 , LDD_TX5_TX8_MEMORY_MAP.LOS_THRS     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x22 , LDD_TX5_TX8_MEMORY_MAP.LOS_HYST     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x25 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH0   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x26 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH1   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x27 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH2   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x28 , LDD_TX5_TX8_MEMORY_MAP.Rvcsel_CH3   );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x40 , LDD_TX5_TX8_MEMORY_MAP.OUTPUT_MUTE  );

	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4A , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH0  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4B , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH1  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4C , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH2  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4D , LDD_TX5_TX8_MEMORY_MAP.AC_Gain_CH3  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x4F , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH0  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x50 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH1  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x51 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH2  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x52 , LDD_TX5_TX8_MEMORY_MAP.DC_Gain_CH3  );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x5E , LDD_TX5_TX8_MEMORY_MAP.IBurnIn      );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x5F , LDD_TX5_TX8_MEMORY_MAP.BurnIn_EN    );

	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x42 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH0    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x43 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH1    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x44 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH2    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x45 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH3    );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x46 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH0     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x47 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH1     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x48 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH2     );
	Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x49 , LDD_TX5_TX8_MEMORY_MAP.Imod_CH3     );
}

//--------------------------------------//
//-----Caller is MSA_StateMachine.c-----//
//--------------------------------------//

void LDD_TX5_TX8_Tx_Disable(uint8_t Channel_Control)
{
    //Bit0:TX5
    //Bit1:TX6
    //Bit3:TX7
    //Bit3:TX8
    
    //Tx1 Disable
    if(Channel_Control&0x01)
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x01 ;     
    }
    //Tx1 Enable
    else
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x01 ;
    }
    //Tx2 Disable
    if(Channel_Control&0x02)
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x02 ;     
    }
    //Tx2 Enable
    else
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x02 ;
    }
    //Tx3 Disable
    if(Channel_Control&0x04)
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x04 ;     
    }
    //Tx3 Enable
    else
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x04 ;
    }
    //Tx4 Disable
    if(Channel_Control&0x08)
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE |= 0x08 ;     
    }
    //Tx4 Enable
    else
    {
        LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE &= ~0x08 ;
    }
    //Write value to LDD
    Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x1D , 0x7F );
    Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x41 , LDD_TX5_TX8_MEMORY_MAP.TX_DISABLE );
}

//--------------------------------------//
//-------Caller Slave_I2C0_PB67.c-------//
//--------------------------------------//

void LDD_TX5_TX8_WRITE_P84()
{
	MALD38435_TX5_TX8_Control_Write_ALL();
}

void LDD_TX5_TX8_READ_P84()
{
	MALD38435_TX5_TX8_Control_READ_ALL();
	LDD_TX5_TX8_MEMORY_MAP.IC_RSVD_04    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x04 );
	LDD_TX5_TX8_MEMORY_MAP.IC_RSVD_1E    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x1E );
	LDD_TX5_TX8_MEMORY_MAP.IC_RSVD_29    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x29 );
	LDD_TX5_TX8_MEMORY_MAP.IC_RSVD_5A    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x5A );
	LDD_TX5_TX8_MEMORY_MAP.IC_RSVD_64    =  Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x64 );
}

//--------------------------------------//
//-----------Caller MSA_DDMI.c----------//
//--------------------------------------//

// High Low Temperatuer Bias mod Control
void IBIAS_LUT_CONTROL_TX5_TX8(uint8_t CHSEL )
{
    switch( CHSEL )
    {
        case 0 :
                LDD_TX5_TX8_MEMORY_MAP.IBias_CH0 = GDMCU_FMC_READ_DATA(FS_Ibias0_PA0 + Temperature_Index);
                Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x42 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH0    );
                break;
        case 1 :
                LDD_TX5_TX8_MEMORY_MAP.IBias_CH1 = GDMCU_FMC_READ_DATA(FS_Ibias1_PA1 + Temperature_Index);
                Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x43 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH1    );
                break;
        case 2 :
                LDD_TX5_TX8_MEMORY_MAP.IBias_CH2 = GDMCU_FMC_READ_DATA(FS_Ibias2_PA2 + Temperature_Index);
                Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x44 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH2    );
                break;
        case 3 :
                LDD_TX5_TX8_MEMORY_MAP.IBias_CH3 = GDMCU_FMC_READ_DATA(FS_Ibias3_PA3 + Temperature_Index);
                Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x45 , LDD_TX5_TX8_MEMORY_MAP.IBias_CH3    );
                break;
        default :
                break;
    }
}

uint16_t LDD_TX5_TX8_BIAS(uint8_t Bias_Channel)
{
    uint8_t BIAS_CH=0x00;
	uint16_t TX_BIAS_CURRENT = 0 ;
    uint8_t  Data_MSB = 0;
    uint8_t  Data_LSB = 0;    
	uint16_t VAR2,VAR3 ;
    
    if(Bias_Channel==0)
        BIAS_CH=0x00;
    else if(Bias_Channel==1)
        BIAS_CH=0x20;
    else if(Bias_Channel==2)
        BIAS_CH=0x40;
    else if(Bias_Channel==3)
        BIAS_CH=0x60;
    
	// Set Reg0x60h = 00h, Reg0x91h = 00/20/40/60h (select channel), Reg0x92h = 04h
	// Read Reg0x68h[7:0] and Reg0x69h[3:0],
	// Store VAR2 = The decimal value of Reg0x68h[7:0] and Reg0x69h[3:0] together
	// Set Reg0x60h = 00h, Reg0x91h = 00/20/40/60h (select channel), Reg0x92h=08h
	// Read Reg0x68h[7:0] and Reg0x69h[3:0]
	// Store VAR3 = The decimal value of Reg0x68h[7:0] and Reg0x69h[3:0] together
	// Calculate IBIAS = |VAR2 - VAR3| x VAR1 x 40.1uA
    
    if(Bias_Channel<4)
    {
        Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x60 , 0x10    );
        Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x91 , BIAS_CH );
        Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress , 0x92 , 0x08    );
        delay_1ms(2);
        Data_MSB = Master_I2C1_ByteREAD_PB34(LDD_SlaveAddress ,0x68);
        Data_LSB = Master_I2C1_ByteREAD_PB34(LDD_SlaveAddress ,0x69);
        VAR3 = (uint16_t)(Data_MSB<<4)|(Data_LSB&0xF0);
        Master_I2C1_ByteWrite_PB34( LDD_SlaveAddress,0x92,0x04);
        delay_1ms(1);
        Data_MSB = Master_I2C1_ByteREAD_PB34(LDD_SlaveAddress,0x68);
        Data_LSB = Master_I2C1_ByteREAD_PB34(LDD_SlaveAddress,0x69);
        VAR2 = (uint16_t)(Data_MSB<<4)|(Data_LSB&0xF0);
        
        TX_BIAS_CURRENT = ( VAR2 - VAR3 )*20;
    }
    else
        TX_BIAS_CURRENT=0xFFFF;
    
	return TX_BIAS_CURRENT;
}

//--------------------------------------//
//------------Caller MSA_AW.c-----------//
//--------------------------------------//

uint8_t LDD_TX5_TX8_LOS()
{
    uint8_t LOS_Data=0x00;
    LOS_Data = Master_I2C1_ByteREAD_PB34( LDD_SlaveAddress , 0x17 );
    return LOS_Data;
}