#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "Master_I2C2_PC78.h"
#include "Calibration_Struct.h"
#include "DSP.h"
#include "math.h"

// SNR Data
uint32_t slicer_threshold[3]={0};
uint32_t slicer_target[4]={0};
uint32_t slicer_mean_value[4]={0};
float slicer_sigma_value[4]={0};
double slicer_p_value[4]={0};
uint32_t slicer_p_location[4]={0};
double slicer_v_value[3]={0};
uint32_t slicer_v_location[3]={0};
uint16_t DSP_LineSide_SNR=0x0000;
uint16_t DSP_LineSide_LTP=0x0000;

uint32_t COMMAND_4700_DATA[18]={0};

//-----------------------------------------------------------------------------------------------------//
// DSP Line Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TX_FIR_SET( uint8_t Lane_CH , uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    if(DSP_FW_Version>=0xD00B)
    {
        COMMAND_4700_DATA[0]  = 0x00000040 | (New_Tokan<<16);
        COMMAND_4700_DATA[16] = 0x00000000 ;
    }
    else
    {
        COMMAND_4700_DATA[0]  = 0x0000003C | (New_Tokan<<16);
    }
    COMMAND_4700_DATA[1]  = 0x00000002 ;
    COMMAND_4700_DATA[2]  = 0x00000079 ;
    if(Lane_CH==0)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH0) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH0) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH0) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH0) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        else
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH0) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH0) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH0) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST2_CH0) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH0) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST3_CH0) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH0 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH0
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH0 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH0 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH0 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH0 << 16
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH0 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH0 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==1)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH1) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH1) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH1) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH1) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        else
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH1) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH1) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH1) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST2_CH1) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH1) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST3_CH1) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH1 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH1
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH1 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH1 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH1 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH1 << 16
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH1 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH1 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==2)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH2) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH2) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH2) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH2) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        else
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH2) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH2) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH2) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST2_CH2) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH2) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST3_CH2) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH2 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH2
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH2 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH2 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH2 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH2 << 16
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH2 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH2 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008012 );
    }
    else if(Lane_CH==3)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH3) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST2_CH3) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH3) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST3_CH3) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3 << 8
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3 << 16
                                                   | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        else
        {
            if(Signal_Status!=NRZ_to_NRZ)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   |Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE2_CH3) << 16 ;
                
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH3) << 16  
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH3) ;
            
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST2_CH3) << 16 
                                               | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH3) ;
            // Bit16-31 is POST4 when choose 7TAPs control
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST3_CH3) ;
            
            if(DSP_FW_Version>=0xD00B)
            {
                COMMAND_4700_DATA[7]  = 0x00000000 ;
                COMMAND_4700_DATA[8]  = 0x00000000 ;
                COMMAND_4700_DATA[9]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH3 << 24 ;
                // Bit24-31  is dsp_graycode 0- Disable; 1- Enable
                COMMAND_4700_DATA[10] = 0x01000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH3
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH3 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH3 << 16 ;
            }
            else
            {
                // Bit0-Bit7 is symbol_swap 0- Disable; 1- Enable
                COMMAND_4700_DATA[7]  = 0x00000000 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH3 << 8
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH3 << 16
                                                   | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH3 << 24 ;
                // Bit8-15  is dsp_graycode 0- Disable; 1- Enable
                // Bit16-23 is dsp_high_swing 0- Disable; 1- Enable
                // Bit24-31 is dsp_skewp 3bits value[0,7]
                COMMAND_4700_DATA[8]  = 0x00000100 | DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH3 ;
                COMMAND_4700_DATA[9]  = 0x00000000 ;
                COMMAND_4700_DATA[10] = 0x00000000 ;
            }
        }
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008012 );
    }

    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,PHY_ID);
}

//-----------------------------------------------------------------------------------------------------//
// DSP Line Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL, uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000002 | (New_Tokan<<16);
    if(Lane_CH==0)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH0);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH0);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH0);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH0);
        }
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008004 );
    }
    else if(Lane_CH==1)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH1);
            else
               COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH1); 
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH1);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH1);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008004 );        
    }
    else if(Lane_CH==2)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH2);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH2);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH2);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH2);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008004 );      
    }
    else if(Lane_CH==3)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_TX_Polarity_CH3);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_TX_Polarity_CH3);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_RX_Polarity_CH3);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_RX_Polarity_CH3);  
        }
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008004 );  
    }  
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94, PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0, PHY_ID);
}

//-----------------------------------------------------------------------------------------------------//
// DSP LineSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable, uint8_t PHY_ID)
{  
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000008 | (New_Tokan<<16);
    if(Lane_CH==0)    
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {      
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {         
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008006 );
    }
    // set all channels
    else if(Lane_CH==4)   
    {        
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x00008006 );
    }
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84, PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0, PHY_ID);
}

void DSP_Line_Side_TX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable)
{
    // CH0-7
    if(Lane_CH==All_CH)
    {
        DSP_LineSide_TRX_Squelch_SET(4 ,TX_Side, Enable, DSP_PHY_ID_0);
        DSP_LineSide_TRX_Squelch_SET(4 ,TX_Side, Enable, DSP_PHY_ID_1);
    }
    // CH0-3
    else if(Lane_CH<4)
        DSP_LineSide_TRX_Squelch_SET(Lane_CH ,TX_Side, Enable, DSP_PHY_ID_0);
    // CH4-7
    else if(Lane_CH<8)
        DSP_LineSide_TRX_Squelch_SET(Lane_CH-4 ,TX_Side, Enable, DSP_PHY_ID_1);
}

void DSP_LineSide_RX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable)
{
    // CH0-7
    if(Lane_CH==All_CH)
    {
        DSP_LineSide_TRX_Squelch_SET(4 ,RX_Side, Enable, DSP_PHY_ID_0);
        DSP_LineSide_TRX_Squelch_SET(4 ,RX_Side, Enable, DSP_PHY_ID_1);
    }
    // CH0-3
    else if(Lane_CH<4)
        DSP_LineSide_TRX_Squelch_SET(Lane_CH ,RX_Side, Enable, DSP_PHY_ID_0);
    // CH4-7
    else if(Lane_CH<8)
        DSP_LineSide_TRX_Squelch_SET(Lane_CH-4 ,RX_Side, Enable, DSP_PHY_ID_1);
}
//-----------------------------------------------------------------------------------------------------//
// DSP Line Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  Remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP_LineSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable, uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000002 | (New_Tokan<<16);
    if(Lane_CH==0)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800A );
    }
    //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94, PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0, PHY_ID);
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_Rx_Info_Control
//----------------------------------------------------------------------------------//
void DSP_Line_Side_Rx_Info_SET(uint8_t Lane_CH, uint8_t PHY_ID)
{
    uint32_t Peaking_Filter_Buffter = 0x00000000;
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000044 | (New_Tokan<<16);
    //Lane Select
    if(Lane_CH==0)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x00008012 );
        //Update Peaking Filter Value
        if(PHY_ID==DSP_PHY_ID_0)
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_Peaking_Filter_CH0;
        else
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_Peaking_Filter_CH0;
    }
    else if(Lane_CH==1)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x00008012 );
        //Update Peaking Filter Value
        if(PHY_ID==DSP_PHY_ID_0)
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_Peaking_Filter_CH1;
        else
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_Peaking_Filter_CH1;
    }
    else if(Lane_CH==2)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x00008012 );
        //Update Peaking Filter Value
        if(PHY_ID==DSP_PHY_ID_0)
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_Peaking_Filter_CH2;
        else
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_Peaking_Filter_CH2;
    }
    else if(Lane_CH==3)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x00008012 );
        //Update Peaking Filter Value
        if(PHY_ID==DSP_PHY_ID_0)
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_Peaking_Filter_CH3;
        else
            Peaking_Filter_Buffter|=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_Peaking_Filter_CH3;
    }

    COMMAND_4700_DATA[1]  = 0x00000001;                                 //0x00047004
    COMMAND_4700_DATA[2]  = 0x01000002;                                 //0x00047008    Config Function Select By Bit
    COMMAND_4700_DATA[3]  = Peaking_Filter_Buffter<<24;                 //0x0004700C    Peaking Filter Range[0-31]
    COMMAND_4700_DATA[4]  = 0x00000000;                                 //0x00047010  
    COMMAND_4700_DATA[5]  = 0x00000000;                                 //0x00047014
    COMMAND_4700_DATA[6]  = 0x00000000;                                 //0x00047018
    COMMAND_4700_DATA[7]  = 0x00000000;                                 //0x0004701C
    COMMAND_4700_DATA[8]  = 0x00000000;                                 //0x00047020
    COMMAND_4700_DATA[9]  = 0x00000000;                                 //0x00047024
    COMMAND_4700_DATA[10] = 0x00000000;                                 //0x00047028
    COMMAND_4700_DATA[11] = 0x00000000;                                 //0x0004702C
    COMMAND_4700_DATA[12] = 0x00000000;                                 //0x00047030
    COMMAND_4700_DATA[13] = 0x00000000;                                 //0x00047034
    COMMAND_4700_DATA[14] = 0x00000000;                                 //0x00047038
    COMMAND_4700_DATA[15] = 0x00010000;                                 //0x0004703C  Firmware Default:00 , High Power Mode:01 , Med Power Mode:02 , Low Power Mode:03
    COMMAND_4700_DATA[16] = 0x00000000;                                 //0x00047040
    COMMAND_4700_DATA[17] = 0x00000000;                                 //0x00047044

    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,PHY_ID);
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_ALL_CH14_Control_P86
//----------------------------------------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86()
{
    DSP_LineSide_TRX_Polarity_SET( 0 , TX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 1 , TX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 2 , TX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 3 , TX_Side , DSP_PHY_ID_0);
    
    DSP_LineSide_TRX_Polarity_SET( 0 , RX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 1 , RX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 2 , RX_Side , DSP_PHY_ID_0);
    DSP_LineSide_TRX_Polarity_SET( 3 , RX_Side , DSP_PHY_ID_0);
    
    DSP_LineSide_TX_FIR_SET(0, DSP_PHY_ID_0);
    DSP_LineSide_TX_FIR_SET(1, DSP_PHY_ID_0);
    DSP_LineSide_TX_FIR_SET(2, DSP_PHY_ID_0);
    DSP_LineSide_TX_FIR_SET(3, DSP_PHY_ID_0);
    
    //Rx Info Config
    DSP_Line_Side_Rx_Info_SET(0, DSP_PHY_ID_0);
    DSP_Line_Side_Rx_Info_SET(1, DSP_PHY_ID_0);
    DSP_Line_Side_Rx_Info_SET(2, DSP_PHY_ID_0);
    DSP_Line_Side_Rx_Info_SET(3, DSP_PHY_ID_0);
}
//----------------------------------------------------------------------------------//
// DSP Line_Side_ALL_CH58_Control_P8C
//----------------------------------------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P8C()
{
    DSP_LineSide_TRX_Polarity_SET( 0 , TX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 1 , TX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 2 , TX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 3 , TX_Side , DSP_PHY_ID_1);
    
    DSP_LineSide_TRX_Polarity_SET( 0 , RX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 1 , RX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 2 , RX_Side , DSP_PHY_ID_1);
    DSP_LineSide_TRX_Polarity_SET( 3 , RX_Side , DSP_PHY_ID_1);
    
    DSP_LineSide_TX_FIR_SET(0, DSP_PHY_ID_1);
    DSP_LineSide_TX_FIR_SET(1, DSP_PHY_ID_1);
    DSP_LineSide_TX_FIR_SET(2, DSP_PHY_ID_1);
    DSP_LineSide_TX_FIR_SET(3, DSP_PHY_ID_1);
    
    //Rx Info Config
    DSP_Line_Side_Rx_Info_SET(0, DSP_PHY_ID_1);
    DSP_Line_Side_Rx_Info_SET(1, DSP_PHY_ID_1);
    DSP_Line_Side_Rx_Info_SET(2, DSP_PHY_ID_1);
    DSP_Line_Side_Rx_Info_SET(3, DSP_PHY_ID_1);
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_CDR Lock
//----------------------------------------------------------------------------------//
uint8_t DSP_Line_Side_CDR_Lock()
{
    uint8_t CDR_LOCK_STATUS = 0x00;
    // Rx LOL bit0 - bit3 , Lock:1,nolock:0
    // Line Side Input
    //Get Lane4-Lane7 LOL Data
    CDR_LOCK_STATUS = (~BRCM_Control_READ_Data(0x5201CA34,DSP_PHY_ID_1))&0x0F;
    CDR_LOCK_STATUS = (CDR_LOCK_STATUS<<4);
    //Get Lane0-Lane3 LOL Data
    CDR_LOCK_STATUS |= (~BRCM_Control_READ_Data(0x5201CA34,DSP_PHY_ID_0))&0x0F;
    
    return CDR_LOCK_STATUS;
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_LOS LOL
//----------------------------------------------------------------------------------//

void DSP_Line_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL)
{
    uint16_t LOL_LOS_STATUS,CDR_LOL_STAUTS;
    uint32_t Data_Buffer;
    // Read CH0-CH3 LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA64,DSP_PHY_ID_0);
    // LOS Bit0-7
    *LOS=LOL_LOS_STATUS&0x000F;
    // LOL Bit8-15
    *LOL=(LOL_LOS_STATUS>>8)&0x000F;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA34,DSP_PHY_ID_0);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA6C,Data_Buffer,BRCM_Default_Length,DSP_PHY_ID_0);
        BRCM_Control_READ_Data (0x5201CA64,DSP_PHY_ID_0);
        BRCM_Control_READ_Data (0x5201CA34,DSP_PHY_ID_0);
        BRCM_Control_WRITE_Data(0x5201CA6C,0x00000000,BRCM_Default_Length,DSP_PHY_ID_0);
    }
    // Read CH4-CH7 LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA64,DSP_PHY_ID_1);
    // LOS Bit0-7
    *LOS|=(LOL_LOS_STATUS&0x000F)<<4;
    // LOL Bit8-15
    *LOL|=((LOL_LOS_STATUS>>8)&0x000F)<<4;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA34,DSP_PHY_ID_1);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA6C,Data_Buffer,BRCM_Default_Length,DSP_PHY_ID_1);
        BRCM_Control_READ_Data (0x5201CA64,DSP_PHY_ID_1);
        BRCM_Control_READ_Data (0x5201CA34,DSP_PHY_ID_1);
        BRCM_Control_WRITE_Data(0x5201CA6C,0x00000000,BRCM_Default_Length,DSP_PHY_ID_1);
    }
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_Loopback_Control
//----------------------------------------------------------------------------------//
void DSP_Line_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{   
    if(Lane_CH<4)
        //Lane0 to Lane3 Control
        DSP_LineSide_Loopback_SET( Digital_LoopBack_mode , Lane_CH , Enable , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_LineSide_Loopback_SET( Digital_LoopBack_mode , (Lane_CH-4) , Enable , DSP_PHY_ID_1);
}

void DSP_Line_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH<4)
        //Lane0 to Lane3 Control
        DSP_LineSide_Loopback_SET( Remote_Loopback_mode , Lane_CH , Enable , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_LineSide_Loopback_SET( Remote_Loopback_mode , (Lane_CH-4) , Enable , DSP_PHY_ID_1);
}
//----------------------------------------------------------------------------------//
// DSP Line_Side_PRBS_Control
//----------------------------------------------------------------------------------//
void DSP_Line_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000002C | (New_Tokan<<16);
    //Lane Select
    if((Lane_CH==0)||(Lane_CH==4))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000001 , 0x5201CA94 , 0x0000800C );
    }
    else if((Lane_CH==1)||(Lane_CH==5))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000002 , 0x5201CA94 , 0x0000800C );
    }
    else if((Lane_CH==2)||(Lane_CH==6))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000004 , 0x5201CA94 , 0x0000800C );
    }
    else if((Lane_CH==3)||(Lane_CH==7))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000008 , 0x5201CA94 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    //SSPRQ Generator
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_LineSide)
        COMMAND_4700_DATA[3] = 0x00000000 | Pattern;
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000000;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    //Lane0-Lane3
    if(Lane_CH<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
    }
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1);
    }
}

//----------------------------------------------------------------------------------//
// DSP Line_Side_PRBS_Checker
//----------------------------------------------------------------------------------//
void DSP_Line_Side_PRBS_Checker_Enable(uint16_t Prbs_mode , uint32_t Lane , uint8_t Enable)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000002C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    
    if(Enable)
        COMMAND_4700_DATA[2] = 0x00000001;
    else
        COMMAND_4700_DATA[2] = 0x00000000;
    // Prbs mode
    // [7:0]        
    COMMAND_4700_DATA[3] = (0x00000000 | Prbs_mode);
    COMMAND_4700_DATA[4] =  0x00000000;
    COMMAND_4700_DATA[5] =  0x00000000;
    COMMAND_4700_DATA[6] =  0x00000000;
    COMMAND_4700_DATA[7] =  0x00000000;
    COMMAND_4700_DATA[8] =  0x00000000;
    COMMAND_4700_DATA[9] =  0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    // set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane==All_CH)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x0000800C );
        // DSP Lane0 - Lane3
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);   
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        // DSP Lane4 - Lane7
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);      
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1);
    }
    // DSP Lane0 - Lane3
    else if(Lane<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000 | (0x01<<Lane) , 0x5201CA94 , 0x0000800C );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);   
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
    }
    // DSP Lane4 - Lane7
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000 | (0x01<<(Lane-4)) , 0x5201CA94 , 0x0000800C );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);      
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1);
    }
}

void DSP_Line_Side_PRBS_Clear(uint32_t Lane)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x00000018 | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    // set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane==All_CH)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x0000800E );
        // DSP Lane0 - Lane3 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        // DSP Lane4 - Lane7 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1); 
    }
    else if(Lane==CH_03)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x0000800E );
        // DSP Lane0 - Lane3 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
    }
    else if(Lane==CH_47)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x0000000F , 0x5201CA94 , 0x0000800E );
        // DSP Lane4 - Lane7 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1); 
    }
    // DSP Lane0 - Lane3 
    else if(Lane<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000 | (0x01<<Lane), 0x5201CA94 , 0x0000800E );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_0);
    }
    // DSP Lane4 - Lane7 
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000 | (0x01<<(Lane-4)), 0x5201CA94 , 0x0000800E );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA9C,0x5201CAA0,DSP_PHY_ID_1); 
    }
}

uint64_t DSP_Line_Side_PRBS_Get_Error_Count(uint32_t Lane)
{
    uint64_t GetErrorCount=0;
    uint32_t ReadBuffer[7]={0};
    uint64_t GetMSB_EC=0;
    uint64_t GetLSB_EC=0;
    uint8_t  SEL_PHY=0;
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // PHY Select
    if(Lane<4)
        SEL_PHY=DSP_PHY_ID_0;
    else
        SEL_PHY=DSP_PHY_ID_1;
    
    COMMAND_4700_DATA[0] = 0x00000018 | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(SEL_PHY);
    GET_DSP_FW_VERSION(SEL_PHY);
    //Set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane<4)
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000|(0x01<<Lane) , 0x5201CA94 , 0x0000800F );
    else
        DSP87580_CAPI_CW_CMD( 0x5201CA98 , 0x00000000|(0x01<<(Lane-4)) , 0x5201CA94 , 0x0000800F );
    // Write command to dsp fw command control
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA94,SEL_PHY);     
    // INTF_CAPI2FW_HOST_CMD_RESPONSE_GPREG : cmd_response_address
    ReadBuffer[0] = BRCM_Control_READ_Data(0x5201CA9C,SEL_PHY);
    // INTF_CAPI2FW_HOST_RSP_LANE_MASK_GPREG : rsp_lane_mask_address
    ReadBuffer[1] = BRCM_Control_READ_Data(0x5201CAA0,SEL_PHY);
    ReadBuffer[2] = BRCM_Control_READ_Data(0x00047400,SEL_PHY);
    // CAPI_PRBS_MONITOR
    ReadBuffer[3] = BRCM_Control_READ_Data(0x00047404,SEL_PHY);
    // lock
    ReadBuffer[4] = BRCM_Control_READ_Data(0x00047408,SEL_PHY);
    // lock loss
    ReadBuffer[5] = BRCM_Control_READ_Data(0x0004740C,SEL_PHY); 
    ReadBuffer[6] = BRCM_Control_READ_Data(0x00047410,SEL_PHY);  
    // Get Error count  
    GetLSB_EC = BRCM_Control_READ_Data(0x00047414,SEL_PHY); 
    GetMSB_EC = BRCM_Control_READ_Data(0x00047418,SEL_PHY); 
    
    BRCM_Control_WRITE_Data(0x5201CA84,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA94,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA74,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA9C,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CAA0,0x00000000,BRCM_Default_Length,SEL_PHY);
    
    if(ReadBuffer[4]==1)
        GetErrorCount = (GetMSB_EC<<32|GetLSB_EC);
    else
        GetErrorCount = 0xFFFFFFFFFFFFFFFF;
                                      
    return GetErrorCount;
}

uint8_t DSP_Line_Side_Get_Pattern_Type(uint8_t Pattern_Coding)
{
    uint8_t PRBS_TYPE=0xFF;
    
    switch(Pattern_Coding)
    {
        // PAM4
        case PRBS_31Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS31Q_LineSide;
                        break;
        case PRBS_23Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS23Q_LineSide;
                        break;
        case PRBS_15Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS15Q_LineSide;
                        break;
        case PRBS_13Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS13Q_LineSide;
                        break;
        // not support it
        case PRBS_9Q  :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS9Q_LineSide;
                        break;
        // not support it
        case PRBS_7Q  :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS7Q_LineSide;
                        break;
        // NRZ
        case PRBS_31  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS31Q_LineSide;
                        break;
        case PRBS_23  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS23Q_LineSide;
                        break;
        case PRBS_15  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS15Q_LineSide;
                        break;
        case PRBS_13  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS13Q_LineSide;
                        break;
        case PRBS_9  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS9Q_LineSide;
                        break;
        case PRBS_7  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS7Q_LineSide;
                        break;        
        default      :
                        break;
    }
    return PRBS_TYPE;
}

uint16_t DEC_EQUAL_TO_HEX(uint16_t Data_Value)
{
    uint16_t Data_Buffer = 0;
    uint16_t Hex_Value = 0;
    // Data_Value Ex:1745 dec will convert to 1745 hex
    // Get Bit12-Bit15 Hex Data
    Data_Buffer = Data_Value/1000 ;
    Hex_Value |= 0x1000*Data_Buffer; 
    
    // Get Bit8-Bit11 Hex Data
    Data_Buffer = 0 ;
    // Remove Thousand value
    Data_Buffer = Data_Value%1000;
    Data_Buffer = Data_Buffer/100;
    Hex_Value |= 0x100*Data_Buffer; 
    
    //Get Bit4-Bit7 Hex Data
    Data_Buffer = 0 ;
    // Remove Hundred value
    Data_Buffer = Data_Value%100;
    Data_Buffer = Data_Buffer/10;
    Hex_Value |= 0x10*Data_Buffer; 
    
    //Get Bit0-Bit3 Hex Data
    Data_Buffer = 0 ;
    // Remove Ten value
    Data_Buffer = Data_Value%10;
    
    Hex_Value|=Data_Buffer;
    
    return Hex_Value;
}

uint16_t Calulate_SNR_DATA()
{
    uint8_t For_count=2,Iconnt=0;
    float SNR_Buffer=0,snr_lvl[3]={0};
    uint16_t SNR_DATA=0x0000;
    
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        SNR_Buffer=(float)(slicer_mean_value[Iconnt+1]-slicer_mean_value[Iconnt]);
        snr_lvl[Iconnt]=SNR_Buffer/(slicer_sigma_value[Iconnt]+slicer_sigma_value[Iconnt+1]);
        // snr value= 10*log10*256
        snr_lvl[Iconnt]=(log10(snr_lvl[Iconnt]))*2560;
    }
    // Get min data
    Iconnt=0;
    SNR_DATA=snr_lvl[0];
    for(Iconnt=0;Iconnt<For_count;Iconnt++)
    {
        if(SNR_DATA>snr_lvl[Iconnt+1])
            SNR_DATA=snr_lvl[Iconnt+1];
    }
    SNR_DATA=DEC_EQUAL_TO_HEX(SNR_DATA);
    return SNR_DATA;
}

uint16_t Calulate_LTP_DATA()
{
    uint8_t For_count=2,Iconnt=0;
    double LTP_Buffer=0,ltp_lvl[3]={65535};
    uint16_t LTP_DATA=0x0000;
    
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        LTP_Buffer=(slicer_p_value[Iconnt+1]+slicer_p_value[Iconnt]);
        LTP_Buffer=LTP_Buffer/2;
        ltp_lvl[Iconnt]=LTP_Buffer/slicer_v_value[Iconnt];
        // ltp value= 10*log10*256
        ltp_lvl[Iconnt]=(log10(ltp_lvl[Iconnt]))*2560;
    }
    // Get min data
    Iconnt=0;
    LTP_DATA=ltp_lvl[0];
    for(Iconnt=0;Iconnt<For_count;Iconnt++)
    {
        if(LTP_DATA>ltp_lvl[Iconnt+1])
            LTP_DATA=ltp_lvl[Iconnt+1];
    }
    LTP_DATA=DEC_EQUAL_TO_HEX(LTP_DATA);
    return LTP_DATA;
}
//-----------------------------------------------------------------------------------------------------//
// DSP87540 Line Side SNR LTP
// 
//-----------------------------------------------------------------------------------------------------//
void DSP_Line_Side_SNR_LTP_GET(uint8_t Lane_CH,uint8_t PHY_ID)
{
    uint16_t SNR_DATA=0x0000;
    uint32_t Write_Buffer=0x00047004,Data_Buffer=0x00000000;
    uint16_t For_count=22,Iconnt=0;
    
    BRCM_Control_READ_Data(0x5201CA94,DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data(0x00047000,0x0000005C,BRCM_Default_Length,PHY_ID);
    // Write 0x00047004 to 0x0004705C
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        BRCM_Control_WRITE_Data(Write_Buffer,0xCCCCCCCC,BRCM_Default_Length,PHY_ID);
        Write_Buffer+=0x00000004;
    }
    // Write Lane
    BRCM_Control_WRITE_Data(0x5201CA98,0x00000001<<Lane_CH,BRCM_Default_Length,PHY_ID);
    // Write Command
    BRCM_Control_WRITE_Data(0x5201CA94,0x00008017,BRCM_Default_Length,PHY_ID);
    // Read for until ready
    Iconnt=0;
    For_count=1500;
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Data_Buffer=BRCM_Control_READ_Data(0x5201CA9C,PHY_ID);
        if(Data_Buffer==0x00008000)
            break;
        else
            delay_1ms(1);
    }
    // Read Select Lane
    BRCM_Control_READ_Data(0x5201CAA0,PHY_ID);
    // Read 0x00047400
    BRCM_Control_READ_Data(0x00047400,PHY_ID);
    //-----------------------------------------------------------//
    // Read slicer_threshold[0] and slicer_threshold[1]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047404,PHY_ID);
    slicer_threshold[0]=Data_Buffer&0x0000FFFF;
    slicer_threshold[1]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_threshold[2] and slicer_target[0]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047408,PHY_ID);
    slicer_threshold[2]=Data_Buffer&0x0000FFFF;
    slicer_target[0]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_target[1] and slicer_target[3]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x0004740C,PHY_ID);
    slicer_target[1]=Data_Buffer&0x0000FFFF;
    slicer_target[2]=(Data_Buffer>>16)&0x0000FFFF;
    slicer_target[3]=BRCM_Control_READ_Data(0x00047410,PHY_ID)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_mean_value[0] - slicer_mean_value[3]
    //-----------------------------------------------------------//
    slicer_mean_value[0]=BRCM_Control_READ_Data(0x00047414,PHY_ID);
    slicer_mean_value[1]=BRCM_Control_READ_Data(0x00047418,PHY_ID);
    slicer_mean_value[2]=BRCM_Control_READ_Data(0x0004741C,PHY_ID);
    slicer_mean_value[3]=BRCM_Control_READ_Data(0x00047420,PHY_ID);
    //-----------------------------------------------------------//
    // Read slicer_sigma_value[0] - slicer_sigma_value[3]
    //-----------------------------------------------------------//
    slicer_sigma_value[0]=(float)sqrt(BRCM_Control_READ_Data(0x00047424,PHY_ID));
    slicer_sigma_value[1]=(float)sqrt(BRCM_Control_READ_Data(0x00047428,PHY_ID));
    slicer_sigma_value[2]=(float)sqrt(BRCM_Control_READ_Data(0x0004742C,PHY_ID));
    slicer_sigma_value[3]=(float)sqrt(BRCM_Control_READ_Data(0x00047430,PHY_ID));
    //-----------------------------------------------------------//
    // Read slicer_p_value[0] - slicer_p_value[3]
    //-----------------------------------------------------------//
    slicer_p_value[0]=BRCM_Control_READ_Data(0x00047434,PHY_ID);
    slicer_p_value[1]=BRCM_Control_READ_Data(0x00047438,PHY_ID);
    slicer_p_value[2]=BRCM_Control_READ_Data(0x0004743C,PHY_ID);
    slicer_p_value[3]=BRCM_Control_READ_Data(0x00047440,PHY_ID);
    //-----------------------------------------------------------//
    // Read slicer_p_location[0] - slicer_p_location[1]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047444,PHY_ID);
    slicer_p_location[0]=Data_Buffer&0x0000FFFF;
    slicer_p_location[1]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_p_location[2] - slicer_p_location[3]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047448,PHY_ID);
    slicer_p_location[2]=Data_Buffer&0x0000FFFF;
    slicer_p_location[3]=(Data_Buffer>>16)&0x0000FFFF;
    //-----------------------------------------------------------//
    // Read slicer_v_value[0] - slicer_v_value[2]
    //-----------------------------------------------------------//
    slicer_v_value[0]=BRCM_Control_READ_Data(0x0004744C,PHY_ID);
    slicer_v_value[1]=BRCM_Control_READ_Data(0x00047450,PHY_ID);
    slicer_v_value[2]=BRCM_Control_READ_Data(0x00047454,PHY_ID);
    //-----------------------------------------------------------//
    // Read slicer_v_location[0] - slicer_v_location[2]
    //-----------------------------------------------------------//
    Data_Buffer=BRCM_Control_READ_Data(0x00047458,PHY_ID);
    slicer_v_location[0]=Data_Buffer&0x0000FFFF;
    slicer_v_location[1]=(Data_Buffer>>16)&0x0000FFFF;
    slicer_v_location[2]=BRCM_Control_READ_Data(0x0004745C,PHY_ID)&0x0000FFFF;
    // Clear Register data
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length ,PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length ,PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length ,PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CA9C , 0x00000000 , BRCM_Default_Length ,PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CAA0 , 0x00000000 , BRCM_Default_Length ,PHY_ID);
    
    DSP_LineSide_SNR=Calulate_SNR_DATA();
    DSP_LineSide_LTP=Calulate_LTP_DATA();
}
