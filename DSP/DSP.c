#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "Master_I2C1_PB34.h"
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
#include "GD_FlahMap.h"

uint32_t INTF_CAPI_SW_CMD_ADR0    = 0x5201CA78;
uint32_t INTF_CAPI_SW_CMD_DATA0   = 0x0000800F;
uint32_t INTF_CAPI_SW_CMD_ADR1    = 0x5201CA74;
uint32_t INTF_CAPI_SW_CMD_DATA1   = 0x00008008;

struct DSP_Direct_Control_MEMORY DSP_Direct_Control_MEMORY_MAP;
struct DSP_Line_Side_PHY0_MEMORY DSP_Line_Side_PHY0_MEMORY_MAP;
struct DSP_System_Side_PHY0_MEMORY DSP_System_Side_PHY0_MEMORY_MAP;
struct DSP_Line_Side_PHY1_MEMORY DSP_Line_Side_PHY1_MEMORY_MAP;
struct DSP_System_Side_PHY1_MEMORY DSP_System_Side_PHY1_MEMORY_MAP;

uint8_t DSP_Ready_Flag_0 = 0 ;
uint8_t DSP_Ready_Flag_1 = 0 ;
uint8_t DSP_VCC_Flag = 0;
uint16_t DSP_FW_Version = 0;
uint16_t cmd_tocken = 0;
 
void BRCM_Control_WRITE_Data( uint32_t BRCM_ADDR , uint32_t Data_Value , uint16_t Data_Length , uint8_t PHY_ID)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t Write_data_buffer_0,Write_data_buffer_1,Write_data_buffer_2,Write_data_buffer_3;
	uint8_t LEN_LSB,LEN_MSB ;
    
	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;

	LEN_LSB = ( Data_Length & 0xFF ) ;
	LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;

	Write_data_buffer_0 = Data_Value ;
	Write_data_buffer_1 = Data_Value >> 8 ;
    Write_data_buffer_2 = Data_Value >> 16 ;
    Write_data_buffer_3 = Data_Value >> 24 ;
    
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_CTRL , 0x03 );
	// Step5 : Write Data
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_WFIFO , Write_data_buffer_0 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_WFIFO , Write_data_buffer_1 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_WFIFO , Write_data_buffer_2 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_WFIFO , Write_data_buffer_3 );   
}

uint32_t BRCM_Control_READ_Data( uint32_t BRCM_ADDR , uint8_t PHY_ID)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t IIcount = 0;
	uint8_t Read_data_buffer_0,Read_data_buffer_1,Read_data_buffer_2,Read_data_buffer_3;
	uint32_t  return_DATA = 0x00000000 ;
	uint8_t TEMP_BUUFFER;
    
	ADDR_Byte0 = BRCM_ADDR ;
	ADDR_Byte1 = BRCM_ADDR>>8 ;
	ADDR_Byte2 = BRCM_ADDR>>16 ;
	ADDR_Byte3 = BRCM_ADDR>>24 ;
    
	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_LEN0 , 4 );
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_LEN1 , 0 );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C1_ByteWrite_PB34( PHY_ID , BRCM_IND_CTRL , 0x01 );
	// Step5 : Write Data
	Read_data_buffer_0 = Master_I2C1_ByteREAD_PB34( PHY_ID , BRCM_RFIFO );
	Read_data_buffer_1 = Master_I2C1_ByteREAD_PB34( PHY_ID , BRCM_RFIFO );
	Read_data_buffer_2 = Master_I2C1_ByteREAD_PB34( PHY_ID , BRCM_RFIFO );
	Read_data_buffer_3 = Master_I2C1_ByteREAD_PB34( PHY_ID , BRCM_RFIFO );
    
    return_DATA |= Read_data_buffer_0 ;
    return_DATA |= Read_data_buffer_1 <<8 ;
    return_DATA |= Read_data_buffer_2 <<16 ;
    return_DATA |= Read_data_buffer_3 <<24 ;
    
	return return_DATA;
}

void BRCM_WRITE_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length , uint8_t PHY_ID)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount ;
    uint8_t I2C_SlaveAddress ;
    
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 
    
    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
    
    if(PHY_ID==0)
        I2C_SlaveAddress=DSP_PHY_ID_0;
    else if(PHY_ID==1)
        I2C_SlaveAddress=DSP_PHY_ID_1;
    else
        I2C_SlaveAddress=PHY_ID;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x03 write command
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_CTRL , 0x03 );
    
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
        Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_WFIFO , *DataBuffer++ );
}
 
void BRCM_READ_Data( uint8_t *BRCM_ADDR , uint8_t *DataBuffer , uint8_t Data_Length , uint8_t PHY_ID)
{
	uint8_t ADDR_Byte3,ADDR_Byte2,ADDR_Byte1,ADDR_Byte0;
	uint8_t LEN_LSB,LEN_MSB ;
	uint8_t IIcount = 0 , Count = 0 ;
    uint8_t I2C_SlaveAddress ;
    
    // GD MCU MSB & LSB Swap
	ADDR_Byte3 = *BRCM_ADDR++ ; 
	ADDR_Byte2 = *BRCM_ADDR++ ; 
	ADDR_Byte1 = *BRCM_ADDR++ ; 
	ADDR_Byte0 = *BRCM_ADDR++ ; 

    LEN_LSB = Data_Length ;
    LEN_MSB = 0x00 ;
	//LEN_LSB = ( Data_Length & 0xFF ) ;
	//LEN_MSB = ( ( Data_Length >> 8 ) & 0xFF ) ;
    
    if(PHY_ID==0)
        I2C_SlaveAddress=DSP_PHY_ID_0;
    else if(PHY_ID==1)
        I2C_SlaveAddress=DSP_PHY_ID_1;
    else
        I2C_SlaveAddress=PHY_ID;

	// Step1 : Write 0xFF to QSFP PAGE SELECT Control register
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_PAGE_SELECT , 0xFF );
	// Step2 : Write INDADDR0 - INDADDR3
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR0 , ADDR_Byte0 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR1 , ADDR_Byte1 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR2 , ADDR_Byte2 );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_ADDR3 , ADDR_Byte3 );
	// Step3 : Write Data Length
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_LEN0 , LEN_LSB );
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_LEN1 , LEN_MSB );
	// Step4 : Write Data to WFIFO 0x01 READ command
	Master_I2C1_ByteWrite_PB34( I2C_SlaveAddress , BRCM_IND_CTRL , 0x01 );
        
	for(IIcount=0;IIcount<LEN_LSB;IIcount++)
       *DataBuffer++ = Master_I2C1_ByteREAD_PB34( I2C_SlaveAddress , BRCM_RFIFO );      
}

//-----------------------------------------------------------------------------------------------------//
//--------------------------------Chip Function--------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
void intf_util_assign_tocken(uint32_t* cmd_tocken_ptr)
{
    cmd_tocken++;
    *cmd_tocken_ptr = cmd_tocken;
}
void capi_clr_dual_die_sync_up_st(uint8_t PHY_ID)
{
    uint32_t Data_Buffer =0x00000000 ,dual_die_handle_active;
    
    dual_die_handle_active=BRCM_Control_READ_Data(0x5201CA08,PHY_ID);
    //  die not ready
    if(dual_die_handle_active!=0)
        delay_1ms(1);
    
    /*Die 0: cAPI clear below all bits before initializing config command to FW*/
    if(PHY_ID==DSP_PHY_ID_0)
    {
        for(uint8_t PHYID=0;PHYID<2;PHYID++)
        {
            Data_Buffer=BRCM_Control_READ_Data(0x5201C9E8,DSP_PHY_ID_0);
            BRCM_Control_WRITE_Data( 0x5201C9E8 , 0x00000002 , BRCM_Default_Length  , DSP_PHY_ID_0);
            Data_Buffer=BRCM_Control_READ_Data(0x5201C9E8,DSP_PHY_ID_0);
            BRCM_Control_WRITE_Data( 0x5201C9E8 , 0x00000000 , BRCM_Default_Length  , DSP_PHY_ID_0);
            Data_Buffer=BRCM_Control_READ_Data(0x5201C9EC,DSP_PHY_ID_0);
            BRCM_Control_WRITE_Data( 0x5201C9EC , 0x00000000 , BRCM_Default_Length  , DSP_PHY_ID_0);
        }
    }
}

void capi_config_dual_die_sync_up_handle(uint8_t PHY_ID)
{
    uint16_t II_Count = 0 ;
    uint32_t dual_die_handle_active,rdy_st;
    uint8_t  start_cfg=1;
    // DUAL_DIE_SYNC_ACTIVE_FLAG
    dual_die_handle_active=BRCM_Control_READ_Data(0x5201CA08,PHY_ID);
    //  die not ready
    if(dual_die_handle_active!=0)
        delay_1ms(1);
    /*Die 2: cAPI check both die status, if matched expectation, cAPI set START_CFG flag to notify fw*/
    if(PHY_ID==DSP_PHY_ID_1)
    {
        for(uint8_t i=0;i<100;i++)
        {
            /*check both dies' cmode ready status flag*/
            rdy_st=BRCM_Control_READ_Data(0x5201C9E8,DSP_PHY_ID_0);
            if((rdy_st&0x03)!=0x03)
                start_cfg &= 0;
            rdy_st=BRCM_Control_READ_Data(0x5201C9E8,DSP_PHY_ID_1);
            if((rdy_st&0x03)!=0x03)
                start_cfg &= 0;
            /*set the both dies' start config flag*/
            if(start_cfg)
            {
                BRCM_Control_READ_Data(0x5201C9EC,DSP_PHY_ID_0);
                BRCM_Control_WRITE_Data( 0x5201C9EC , 0x00000001 , BRCM_Default_Length  , DSP_PHY_ID_0);
                BRCM_Control_READ_Data(0x5201C9EC,DSP_PHY_ID_1);
                BRCM_Control_WRITE_Data( 0x5201C9EC , 0x00000001 , BRCM_Default_Length  , DSP_PHY_ID_1);
                break;
            }
            else
                delay_1ms(1);
        }
    }
}

uint32_t DSP_GET_CHIP_ID(uint8_t PHY_ID)
{
    uint32_t CHIP_ID;
    uint8_t PHYID_ADDRESS;
    // Get Phy Address
    if(PHY_ID==0)
        PHYID_ADDRESS=DSP_PHY_ID_0;
    else
        PHYID_ADDRESS=DSP_PHY_ID_1;
    
    CHIP_ID=(BRCM_Control_READ_Data(0x5201D004,PHYID_ADDRESS)&0x0000000F)<<16;
    CHIP_ID|=BRCM_Control_READ_Data(0x5201D000,PHYID_ADDRESS);
    
   return CHIP_ID;
}

void GET_DSP_FW_VERSION(uint8_t PHY_ID)
{
    uint32_t i;
    // Get DSP FW Version
    for(i=0;i<10;i++)
    {
        DSP_FW_Version=BRCM_Control_READ_Data(0x5201C810,PHY_ID);
        if(DSP_FW_Version>=0xD00C)
            break;
        else
            delay_1ms(1);
    }
}

//-----------------------------------------------------------------------------------------------------//
// DSP87580 Chip Status uc Ready/Avs Done/SPI_Dev_ready
//-----------------------------------------------------------------------------------------------------//

void DSP87580_EEPROM_AVS_UcReady_CHECK(uint8_t PHY_ID)
{
	uint16_t  JJ_Count = 0 ;
	uint16_t  II_Count = 0 ;
    uint32_t  DataBuffer = 0 ;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
    while(1)
    {
        DataBuffer = BRCM_Control_READ_Data(0x5201D4A0,PHY_ID);
        // AVS & EEPROM Download & uMIC Ready value is 0x0B
        if( DataBuffer == 0x0B )
        {   
            if(PHY_ID==DSP_PHY_ID_0)
                DSP_Ready_Flag_0 = 1;
            if(PHY_ID==DSP_PHY_ID_1)
                DSP_Ready_Flag_1 = 1;
            capi_config_dual_die_sync_up_handle(PHY_ID);
            break;
        }
        else
        {
            JJ_Count++;
            delay_1ms(10);
            
            if( JJ_Count >= 200 )
            {
                JJ_Count = 0 ;
                if(PHY_ID==DSP_PHY_ID_0)
                    DSP_Ready_Flag_0 = 0 ;
                if(PHY_ID==DSP_PHY_ID_1)
                    DSP_Ready_Flag_1 = 0 ;
                break;
            }
        }
    }
    GET_DSP_FW_VERSION(PHY_ID);
}

//-----------------------------------------------------------------------------------------------------//
// DSP87580_CAPI_CW_CMD
//-----------------------------------------------------------------------------------------------------//
void DSP87580_CAPI_CW_CMD(uint32_t INTF_ADR0,uint32_t INTF_DATA0,uint32_t INTF_ADR1,uint32_t INTF_DATA1)
{
    INTF_CAPI_SW_CMD_ADR0 = INTF_ADR0 ;
    INTF_CAPI_SW_CMD_DATA0 = INTF_DATA0 ;
    INTF_CAPI_SW_CMD_ADR1 = INTF_ADR1 ;
    INTF_CAPI_SW_CMD_DATA1 = INTF_DATA1 ;
}

void DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(uint32_t Read_ADDRESS, uint8_t PHY_ID)
{
    uint32_t Read_Buffer = 0;
    uint16_t Iconnt = 0;
    uint16_t For_count = 0;
    uint32_t Base_Offset = 0;
    uint16_t DataSize_Buffer =0;
    
    Read_Buffer = BRCM_Control_READ_Data(Read_ADDRESS,PHY_ID);
    
    DataSize_Buffer = COMMAND_4700_DATA[0] ;
    For_count = DataSize_Buffer/4 ;
    
    if(For_count>=1)
        For_count = For_count - 1;
    // Step 1 : set data size and command
    //data size                      Bit0-15 : capi_config_info_t
    //intf_util_assign_tocken Bit16-31: This utility incrementally generate a new token
    BRCM_Control_WRITE_Data( COMMAND_Base_ADR , COMMAND_4700_DATA[0] , BRCM_Default_Length , PHY_ID);
    //ref_clk CAPI_REF_CLK_FRQ_156_25_MHZ_ETHERNET
    //func_mode Bit16-31: CAPI_MODE_200G     
    for(Iconnt=0;Iconnt<=For_count;Iconnt++)
    {
        Base_Offset = ( Iconnt+1 ) ;
        BRCM_Control_WRITE_Data( COMMAND_Base_ADR + ( Base_Offset*4 ) , COMMAND_4700_DATA[Base_Offset] , BRCM_Default_Length , PHY_ID);
    }
    //INTF_CAPI2FW_CW_CMD_LANE_MASK_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR0 , INTF_CAPI_SW_CMD_DATA0 , BRCM_Default_Length , PHY_ID );
    //INTF_CAPI2FW_CW_CMD_REQUEST_GPREG
    BRCM_Control_WRITE_Data( INTF_CAPI_SW_CMD_ADR1 , INTF_CAPI_SW_CMD_DATA1 , BRCM_Default_Length , PHY_ID );
}

//-----------------------------------------------------------------------------------------------------//
// DSP87580 intf_capi2fw_command_response
//-----------------------------------------------------------------------------------------------------//
void DSP87580_CAPI_Command_response(uint32_t CHK_ADR0,uint32_t CHK_ADR1, uint8_t PHY_ID)
{
    uint32_t Read_Buffer;
    uint32_t IIcount = 0 ;
    uint32_t Time_out_count = 20;
    // Check whether the FW has processed the command 
    // INTF_CAPI2FW_CW_CMD_RESPONSE_GPREG = 0x00008000
    while(1)
    {
        Read_Buffer = BRCM_Control_READ_Data(CHK_ADR0 , PHY_ID);      
        if(Read_Buffer==0x00008000)
            break;
        else
        {
            for(IIcount=0;IIcount<20000;IIcount++);
            
            if((Time_out_count--) == 0)
                break;
        }       
    }    
    // This is optional since the tocken verification is already completed 
    // INTF_CAPI2FW_CW_RSP_LANE_MASK_GPREG = 0x0000800F
    Read_Buffer = BRCM_Control_READ_Data(CHK_ADR1 , PHY_ID);
    
    // clear register data 
    BRCM_Control_WRITE_Data( 0x5201CA84 , 0x00000000 , BRCM_Default_Length  , PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CA94 , 0x00000000 , BRCM_Default_Length  , PHY_ID);
    BRCM_Control_WRITE_Data( 0x5201CA74 , 0x00000000 , BRCM_Default_Length  , PHY_ID);
    BRCM_Control_WRITE_Data( CHK_ADR0   , 0x00000000 , BRCM_Default_Length  , PHY_ID);
    BRCM_Control_WRITE_Data( CHK_ADR1   , 0x00000000 , BRCM_Default_Length  , PHY_ID); 
}

//-----------------------------------------------------------------------------------------------------//
// DSP87580 line_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87580_line_lane_cfg_done(uint8_t PHY_ID)
{   
    if((DSP_MODE_SET==Mode8_4X25G_NRZ_2X50G_PAM4_1PORT)||(DSP_MODE_SET==Mode17_4x25G_NRZ_2x53G_PAM4_1PORT))
    {
        BRCM_Control_READ_Data(0x5201CDB8,PHY_ID);  
        BRCM_Control_READ_Data(0x5201CB40,PHY_ID);    
        BRCM_Control_READ_Data(0x5201CB44,PHY_ID); 
        BRCM_Control_READ_Data(0x5201CB80,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB84,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB88,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB8C,PHY_ID);
    }
    else if(DSP_MODE_SET==Mode5_4x26G_NRZ_2x53G_PAM4_1PORT)
    {
        BRCM_Control_READ_Data(0x5201CDB8,PHY_ID);    
        BRCM_Control_READ_Data(0x5201CB40,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB44,PHY_ID);   
        BRCM_Control_READ_Data(0x5201CB80,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB84,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB88,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB8C,PHY_ID);
    }
    else if(DSP_MODE_SET==Mode12_4x26G_NRZ_4x26G_NRZ_1PORT)
    {
        BRCM_Control_READ_Data(0x5201CDB8,PHY_ID);    
        BRCM_Control_READ_Data(0x5201CB40,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB44,PHY_ID);   
        BRCM_Control_READ_Data(0x5201CB80,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB84,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB88,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB8C,PHY_ID);
    }
    else
    {
        BRCM_Control_READ_Data(0x5201CDB8,PHY_ID);    
        BRCM_Control_READ_Data(0x5201CB40,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB44,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB48,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB4C,PHY_ID);    
        BRCM_Control_READ_Data(0x5201CB80,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB84,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB88,PHY_ID);
        BRCM_Control_READ_Data(0x5201CB8C,PHY_ID);
    }
}
//-----------------------------------------------------------------------------------------------------//
// DSP87580 host_lane_cfg_done
//-----------------------------------------------------------------------------------------------------//
void DSP87580_host_lane_cfg_done(uint8_t PHY_ID)
{
    BRCM_Control_READ_Data(0x5201D494,PHY_ID);
    BRCM_Control_READ_Data(0x5201D494,PHY_ID);
    BRCM_Control_READ_Data(0x5201D494,PHY_ID);
    BRCM_Control_READ_Data(0x5201D494,PHY_ID);  
    BRCM_Control_READ_Data(0x5201D490,PHY_ID);
    BRCM_Control_READ_Data(0x5201D490,PHY_ID);
    BRCM_Control_READ_Data(0x5201D490,PHY_ID);
    BRCM_Control_READ_Data(0x5201D490,PHY_ID);
}
//-----------------------------------------------------------------------------------------------------//
// DSP Internal Voltage Control
//-----------------------------------------------------------------------------------------------------//
void DSP_SET_0V75()
{
    uint32_t Read_Buffer = 0;
    uint16_t Data_Buffer = 0x0000;
    
    Data_Buffer=Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.DSP_0V75);
    //Default Voltage
    if(Data_Buffer==0)
        Data_Buffer=750;
    else if(Data_Buffer>1000)
        Data_Buffer=750;
    //calulate voltage
    Data_Buffer=((((Data_Buffer*1000)-500000)/3125)&0xFF)<<8;
        
    //* Do a read modify write to regC *///
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x60B20000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0);
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x50B20000|Data_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    
    //* Toggle by writing 1 to reg0[1] and then 0 to complete the transaction *//
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x60820000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0)|0x00000002;
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x50820000|Read_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x60820000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0)&0x0000FFFD;
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x50820000|Read_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
}

void DSP_SET_0V90()
{
    uint32_t Read_Buffer = 0;
    uint16_t Data_Buffer = 0x0000;
    Data_Buffer=Swap_Bytes(DSP_Line_Side_PHY0_MEMORY_MAP.DSP_0V90);
    //Default Voltage
    if(Data_Buffer==0)
        Data_Buffer=900;
    else if(Data_Buffer>1100)
        Data_Buffer=900;
    
    //calulate voltage
    Data_Buffer=((((Data_Buffer*1000)-500000)/3125)&0xFF)<<8;
    
    //* Do a read modify write to regC *///
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x61320000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0);
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51320000|Data_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    
    //* Toggle by writing 1 to reg0[1] and then 0 to complete the transaction *//
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x61020000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0)|0x00000002;
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51020000|Read_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 , 0x61020000 , BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B004,DSP_PHY_ID_0)&0x0000FFFD;
    
    BRCM_Control_WRITE_Data( 0x4000B000 , 0X00000286 , BRCM_Default_Length , DSP_PHY_ID_0);
    BRCM_Control_WRITE_Data( 0x4000B004 ,(0x51020000|Read_Buffer), BRCM_Default_Length , DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
    Read_Buffer=BRCM_Control_READ_Data(0x4000B000,DSP_PHY_ID_0);
}

void DSP_LineSide_TX_FIR_Initialize()
{
    uint8_t Data_Buffer[128];
    
    if(Signal_Status==NRZ_to_NRZ)
    {
        // Line Side Tx FIR Reset for NRZ
        // Pre1 CH0-3
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH0 = 0xF8FF;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH1 = 0xF8FF;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH2 = 0xF8FF;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_PRE1_CH3 = 0xF8FF;
        // Pre1 CH4-7
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH0 = 0xF8FF;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH1 = 0xF8FF;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH2 = 0xF8FF;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_PRE1_CH3 = 0xF8FF;
        // Main CH0-3
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH0 = 0x7600;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH1 = 0x7600;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH2 = 0x7600;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_Main_CH3 = 0x7600;
        // Main CH4-7
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH0 = 0x7600;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH1 = 0x7600;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH2 = 0x7600;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_Main_CH3 = 0x7600;
        // Post1 CH0-3
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH0 = 0x0000;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH1 = 0x0000;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH2 = 0x0000;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_Side_POST1_CH3 = 0x0000;
        // Post1 CH4-7
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH0 = 0x0000;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH1 = 0x0000;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH2 = 0x0000;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_Side_POST1_CH3 = 0x0000;
        // Level Shift CH0-3
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH0 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH0 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH0 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH0 = 0x00;
        
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH1 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH1 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH1 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH1 = 0x00;
        
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH2 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH2 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH2 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH2 = 0x00;
        
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_00_CH3 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_01_CH3 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_10_CH3 = 0x00;
        DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_LShift_11_CH3 = 0x00;
        // Level Shift CH4-7
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH0 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH0 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH0 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH0 = 0x00;
        
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH1 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH1 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH1 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH1 = 0x00;
        
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH2 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH2 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH2 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH2 = 0x00;
        
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_00_CH3 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_01_CH3 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_10_CH3 = 0x00;
        DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_LShift_11_CH3 = 0x00;
    }
    else
    {
        // DSP Line-side CH0 - CH3
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86        , &Data_Buffer[0] , 128 );
        memcpy(  &DSP_Line_Side_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // DSP Line-side CH4 - CH7
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P8C        , &Data_Buffer[0] , 128 );
        memcpy(  &DSP_Line_Side_PHY1_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // DSP System-side CH0 - CH3
        GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87        , &Data_Buffer[0] , 128 );
        memcpy(  &DSP_System_Side_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // DSP System-side CH4 - CH7
        GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P8E        , &Data_Buffer[0] , 128 );
        memcpy(  &DSP_System_Side_PHY1_MEMORY_MAP   , &Data_Buffer[0] , 128 );
    }
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------------------------//
// Trigger_CMD_Update_DSP_REG
//----------------------------------------------------------------------------------//
void Trigger_CMD_Update_DSP_REG()
{
	Line_Side_ALL_CH1_CH4_Control_P86();
	System_Side_ALL_CH1_CH4_Control_P87();
    Line_Side_ALL_CH1_CH4_Control_P8C();
	System_Side_ALL_CH1_CH4_Control_P8E();
    DSP_SET_0V75();
    DSP_SET_0V90();
}

void TEST_DSP_Command_Direct_Control()
{
    uint32_t Read_temp_buffer ;
	if( DSP_Direct_Control_MEMORY_MAP.RW_EN == 0x01 )
	{
		if( DSP_Direct_Control_MEMORY_MAP.Direct_Control == 0x00 )
            BRCM_WRITE_Data( &DSP_Direct_Control_MEMORY_MAP.BRCM_REG_ADDR_0 , &DSP_Direct_Control_MEMORY_MAP.WRITE_BUFFER_0 , DSP_Direct_Control_MEMORY_MAP.LENGITH_LSB  , DSP_Direct_Control_MEMORY_MAP.Phy_ID);
		else if( DSP_Direct_Control_MEMORY_MAP.Direct_Control == 0x01 )
            BRCM_READ_Data( &DSP_Direct_Control_MEMORY_MAP.BRCM_REG_ADDR_0 , &DSP_Direct_Control_MEMORY_MAP.READ_BUFFER_0 , DSP_Direct_Control_MEMORY_MAP.LENGITH_LSB  , DSP_Direct_Control_MEMORY_MAP.Phy_ID);

		DSP_Direct_Control_MEMORY_MAP.RW_EN = 0x00 ;
	}
}

//----------------------------------------------------------------------------------//
// DSP87580 Initialize 
//----------------------------------------------------------------------------------//
void DSP_Init()
{ 
    if(DSP_VCC_Flag==0)
        delay_1ms(500);
    else
        delay_1ms(200);
    DSP87580_EEPROM_AVS_UcReady_CHECK(DSP_PHY_ID_0);
    DSP87580_EEPROM_AVS_UcReady_CHECK(DSP_PHY_ID_1);
    DSP_LineSide_TX_FIR_Initialize();
    if((DSP_Ready_Flag_0)&&(DSP_Ready_Flag_1))
    {
        SET_DSP_CHIP_MODE();
        //Config 0V75 and 0V90
        if(DSP_VCC_Flag==0)
        {
            DSP_SET_0V75();
            DSP_SET_0V90();
            DSP_VCC_Flag=1;
        }
    }
}

//-----------------------------------------------------------------------------------------------------//
// DSP 87580 CHIP MODE
//-----------------------------------------------------------------------------------------------------//
//Mode1
void SET_CHIP_MODE_4X53G_PAM4_4X53G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    // Step1 : DSP87540_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW
    // Step2 : DSP87540_CAPI_Command_response
    // Step3 : Delay 20 ms
    // Step4 : is_chip_mode_config_in_progress >= 200
    // Step5 : port_mode_cfg_log if port_mode_cfg_log != 0xF =>FW report the CHIP MODE CONFIG fail
    // Step6 : line_lane_cfg_done
    // Step7 : host_lane_cfg_done
    // COMMAND Vaule definition
    // 4700 Data Size
    // 4704 Function mode & refernce CLK
    // 4708 Mux_type & FEC Term
    // 470C Host FEC type & Line FEC Type
    // 4710 LW_BR & BH_BR
    // 4714 Line_Lane_Mask & Line_Lane_modulation
    // 4718 Line_Lane_Mask & Line_Lane_modulation
    // 471C Power Down & Status
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00020000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0001;
    COMMAND_4700_DATA[7] = 0x00000002;  
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0000;
}
//Chip Mode2
void SET_CHIP_MODE_2X53G_PAM4_2X53G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0001;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0100;
}
//Chip Mode3
void SET_CHIP_MODE_4X25G_NRZ_4X25G_NRZ(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00020002;
    COMMAND_4700_DATA[5] = 0x000F0000;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0200;
}
//Chip Mode4
void SET_CHIP_MODE_1X53G_PAM4_1X53G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00040000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0001;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0300;
}
//Chip Mode5
void SET_CHIP_MODE_4X26G_NRZ_2X53G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000003;
    COMMAND_4700_DATA[5] = 0x00030001;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0400;
}
//Chip Mode6
void SET_CHIP_MODE_2X26G_NRZ_1X53G_PAM4(uint8_t PHY_ID)
{

}
//Chip Mode7
void SET_CHIP_MODE_2X25G_NRZ_1X50G_PAM4(uint8_t PHY_ID)
{

}
//Chip Mode8
void SET_CHIP_MODE_4X25G_NRZ_2X50G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00010002;
    COMMAND_4700_DATA[5] = 0x00030001;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0700;
}
//Chip Mode9
void SET_CHIP_MODE_1X10G_NRZ_1X10G_NRZ(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00060000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00040004;
    COMMAND_4700_DATA[5] = 0x000F0000;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0800;
}
//Chip Mode10
void SET_CHIP_MODE_2X26G_NRZ_1X53G_PAM4_M1(uint8_t PHY_ID)
{

}
//Chip Mode11
void SET_CHIP_MODE_2X25G_NRZ_1X50G_PAM4_M1(uint8_t PHY_ID)
{

}
//Chip Mode12
void SET_CHIP_MODE_4X26G_NRZ_4X26G_NRZ(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00030003;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0B00;
}
//Chip Mode13
void SET_CHIP_MODE_1X25G_NRZ_1X25G_NRZ(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00050000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00020002;
    COMMAND_4700_DATA[5] = 0x000F0000;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x0C00;
}
//Chip Mode14
void SET_CHIP_MODE_2X26G_NRZ_2X26G_NRZ(uint8_t PHY_ID)
{

}
//Chip Mode15
void SET_CHIP_MODE_2X25G_NRZ_2X25G_NRZ(uint8_t PHY_ID)
{

}
//Chip Mode16
void SET_CHIP_MODE_2X25G_NRZ_1X53G_PAM4(uint8_t PHY_ID)
{

}
//Chip Mode17
void SET_CHIP_MODE_4X25G_NRZ_2X53G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000004;
    COMMAND_4700_DATA[3] = 0x00010002;
    COMMAND_4700_DATA[4] = 0x00000002;
    COMMAND_4700_DATA[5] = 0x00030001;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x00008003 , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x1000;
}

//Chip Mode20
void SET_CHIP_MODE_4X25G_NRZ_4X25G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00020002;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0000;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x1300;
}

//Chip Mode21
void SET_CHIP_MODE_2X50G_PAM4_2X50G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00030000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00010001;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0001;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x1400;
}

//Chip Mode22
void SET_CHIP_MODE_1X50G_PAM4_1X50G_PAM4(uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000001C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00040000;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00010001;
    COMMAND_4700_DATA[5] = 0x000F0001;
    COMMAND_4700_DATA[6] = 0x000F0001;
    COMMAND_4700_DATA[7] = 0x00000002; 
    DSP87580_CAPI_CW_CMD( 0x5201CA78 , 0x0000800F , 0x5201CA74 , 0x00008008 );
    // Write Process
    capi_clr_dual_die_sync_up_st(PHY_ID);
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA74,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA7C,0x5201CA80,PHY_ID);
    delay_1ms(100);
    BRCM_Control_READ_Data(0x5201D4AC,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    BRCM_Control_READ_Data(0x5201D480,PHY_ID);
    DSP87580_line_lane_cfg_done(PHY_ID);
    DSP87580_host_lane_cfg_done(PHY_ID);
    capi_config_dual_die_sync_up_handle(PHY_ID);
    // For GUI to Read Current Mode
    DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE=0x1500;
}

void SET_DSP_CHIP_MODE()
{
    //mode1(Default Mode)
	if( DSP_MODE_SET == Mode1_4x53G_PAM4_4x53G_PAM4_1PORT )
	{
        //Set Chip Mode
        SET_CHIP_MODE_4X53G_PAM4_4X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X53G_PAM4_4X53G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode2
    if( DSP_MODE_SET == Mode2_2x53G_PAM4_2x53G_PAM4_2PORT )
	{
        //Set Chip Mode
        SET_CHIP_MODE_2X53G_PAM4_2X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_2X53G_PAM4_2X53G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode3
    else if ( DSP_MODE_SET == Mode3_4x25G_NRZ_4x25G_NRZ_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X25G_NRZ_4X25G_NRZ(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X25G_NRZ_4X25G_NRZ(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode4
    else if ( DSP_MODE_SET == Mode4_1x53G_PAM4_1x53G_PAM4_4PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_1X53G_PAM4_1X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X53G_PAM4_1X53G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode5
    else if ( DSP_MODE_SET == Mode5_4x26G_NRZ_2x53G_PAM4_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X26G_NRZ_2X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X26G_NRZ_2X53G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode8
    else if ( DSP_MODE_SET == Mode8_4X25G_NRZ_2X50G_PAM4_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X25G_NRZ_2X50G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X25G_NRZ_2X50G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode9
    else if ( DSP_MODE_SET == Mode9_1x10G_NRZ_1x10G_NRZ_4PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_1X10G_NRZ_1X10G_NRZ(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X10G_NRZ_1X10G_NRZ(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode12
    else if ( DSP_MODE_SET == Mode12_4x26G_NRZ_4x26G_NRZ_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X26G_NRZ_4X26G_NRZ(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X26G_NRZ_4X26G_NRZ(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode13
    else if ( DSP_MODE_SET == Mode13_1x25G_NRZ_1x25G_NRZ_4PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_1X25G_NRZ_1X25G_NRZ(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X25G_NRZ_1X25G_NRZ(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode17
    else if ( DSP_MODE_SET == Mode17_4x25G_NRZ_2x53G_PAM4_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X25G_NRZ_2X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X25G_NRZ_2X53G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode20
    else if ( DSP_MODE_SET == Mode20_4X25G_NRZ_4X25G_PAM4_1PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_4X25G_NRZ_4X25G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_4X25G_NRZ_4X25G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode20
    else if ( DSP_MODE_SET == Mode21_2X50G_PAM4_2X50G_PAM4_2PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_2X50G_PAM4_2X50G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_2X50G_PAM4_2X50G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
    //mode21
    else if ( DSP_MODE_SET == Mode22_1X50G_PAM4_1X50G_PAM4_4PORT )
    {
        //Set Chip Mode
        SET_CHIP_MODE_1X50G_PAM4_1X50G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X50G_PAM4_1X50G_PAM4(DSP_PHY_ID_1);
        // System/Line side FIR Setting 
        Line_Side_ALL_CH1_CH4_Control_P86();
        Line_Side_ALL_CH1_CH4_Control_P8C();
        System_Side_ALL_CH1_CH4_Control_P87();
        System_Side_ALL_CH1_CH4_Control_P8E();
    }
}

void Get_DSP_Chip_Mode()
{
    uint8_t Data_Buffer;
    //Get DSP Chip Mode LSB Data
    Data_Buffer = DSP_Line_Side_PHY0_MEMORY_MAP.DSP_CHIP_MODE>>8;

    //Mode 1 CHIP_MODE_4X53G_PAM4_4X53G_PAM4 DSP Default Mode
   	if(Data_Buffer==0x00)
    {
        DSP_MODE_SET=Mode1_4x53G_PAM4_4x53G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 2 CHIP_MODE_2X53G_PAM4_2X53G_PAM4
    else if(Data_Buffer==0x01)
    {
        DSP_MODE_SET=Mode2_2x53G_PAM4_2x53G_PAM4_2PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 3 CHIP_MODE_4X25G_NRZ_4X25G_NRZ
    else if(Data_Buffer==0x02)
    {
        DSP_MODE_SET=Mode3_4x25G_NRZ_4x25G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 4 CHIP_MODE_1X53G_PAM4_1X53G_PAM4
    else if(Data_Buffer==0x03)
    {
        DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 5 CHIP_MODE_4X26G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x04)
    {
        DSP_MODE_SET=Mode5_4x26G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 6 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x05)
    {   
        DSP_MODE_SET=Mode6_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 7 CHIP_MODE_2X25G_NRZ_1X50G_PAM4
    else if(Data_Buffer==0x06)
    {
        DSP_MODE_SET=Mode7_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 8 CHIP_MODE_4X25G_NRZ_2X50G_PAM4
    else if(Data_Buffer==0x07)
    {
        DSP_MODE_SET=Mode8_4X25G_NRZ_2X50G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 9 CHIP_MODE_1X10G_NRZ_1X10G_NRZ
    else if(Data_Buffer==0x08)
    {
        DSP_MODE_SET=Mode9_1x10G_NRZ_1x10G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 10 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x09)
    {
        DSP_MODE_SET=Mode10_2x26G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 11 CHIP_MODE_2X26G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0A)
    {
        DSP_MODE_SET=Mode11_2x25G_NRZ_1x50G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 12 CHIP_MODE_4X26G_NRZ_4X26G_NRZ
    else if(Data_Buffer==0x0B)
    {
        DSP_MODE_SET=Mode12_4x26G_NRZ_4x26G_NRZ_1PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 13 CHIP_MODE_1X25G_NRZ_1X25G_NRZ
    else if(Data_Buffer==0x0C)
    {
        DSP_MODE_SET=Mode13_1x25G_NRZ_1x25G_NRZ_4PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 14 CHIP_MODE_2X26G_NRZ_2X26G_NRZ
    else if(Data_Buffer==0x0D)
    {
        DSP_MODE_SET=Mode14_2x26G_NRZ_2x26G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 15 CHIP_MODE_2X25G_NRZ_2X25G_NRZ
    else if(Data_Buffer==0x0E)
    {
        DSP_MODE_SET=Mode15_2x25G_NRZ_2x25G_NRZ_2PORT;
        //NRZ to NRZ Mode
        Signal_Status=NRZ_to_NRZ;
    }
    //Mode 16 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x0F)
    {
        DSP_MODE_SET=Mode16_2x25G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 17 CHIP_MODE_4X25G_NRZ_2X53G_PAM4
    else if(Data_Buffer==0x10)
    {
        DSP_MODE_SET=Mode17_4x25G_NRZ_2x53G_PAM4_1PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
    //Mode 18 CHIP_MODE_4X50G_PAM4_4X50G_PAM4
    else if(Data_Buffer==0x11)
    {
        DSP_MODE_SET=Mode18_4x50G_PAM4_4x50G_PAM4_1PORT;
        //PAM4 to PAM4 Mode
        Signal_Status=PAM4_to_PAM4;
    }
    //Mode 19 CHIP_MODE_2X25G_NRZ_1X53G_PAM4
    else if(Data_Buffer==0x12)
    {
        DSP_MODE_SET=Mode19_2x50G_NRZ_1x53G_PAM4_2PORT;
        //NRZ to PAM4 Mode
        Signal_Status=NRZ_to_PAM4;
    }
}

//-----------------------------------------------------------------------------------------------------//
// Get DSP87580 temperature 
//-----------------------------------------------------------------------------------------------------/
uint16_t GET_DSP_Temperature(uint8_t PHY_ID)
{
    uint32_t ReadDSP_Buffer ;    
    uint16_t GET_Temperature = 0 ;
    
    ReadDSP_Buffer = BRCM_Control_READ_Data(0x5201C800,PHY_ID);
    // Check bit15 = 1 Temperature data is correct
    // Alibaba Byte70-71 DSP dia temperature
    GET_Temperature = ReadDSP_Buffer ;
    
    return GET_Temperature;
}
//-----------------------------------------------------------------------------------------------------//
// MSA Set Chip Mode
//-----------------------------------------------------------------------------------------------------/
void MSA_SET_CHIP_MODE()
{
    // Config again when different modulation
    DSP_LineSide_TX_FIR_Initialize();
    //Set Chip Mode
    if(DSP_MODE_SET==Mode4_1x53G_PAM4_1x53G_PAM4_4PORT)
    {
        SET_CHIP_MODE_1X53G_PAM4_1X53G_PAM4(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X53G_PAM4_1X53G_PAM4(DSP_PHY_ID_1);
    }
    else if(DSP_MODE_SET==Mode13_1x25G_NRZ_1x25G_NRZ_4PORT)
    {
        SET_CHIP_MODE_1X25G_NRZ_1X25G_NRZ(DSP_PHY_ID_0);
        SET_CHIP_MODE_1X25G_NRZ_1X25G_NRZ(DSP_PHY_ID_1);
    }
}