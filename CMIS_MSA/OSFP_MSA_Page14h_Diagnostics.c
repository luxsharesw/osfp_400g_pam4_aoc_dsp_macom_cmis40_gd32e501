#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "GD_FlahMap.h"
#include "string.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
//------------------------------------------------------------------------------------------//
// Page14h Flags
//------------------------------------------------------------------------------------------//
uint8_t Trigger_SNR_Flag = 0xFF;
uint8_t SNR_Real_Data = 0;
uint8_t Update_Page86_Flash = 0x00;
uint8_t Update_Page8C_Flash = 0x00;
uint8_t L_Host_Checker_LOL=0;
uint8_t L_Media_Checker_LOL=0;
uint8_t Diagnostics_Selector_Flag=0;
//-----------------------------------------------------------------------------------------------------//
// Page14h Latched Diagnostics Flags                                                                   //
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Page14h Controls                                                                                    //
//-----------------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------//
// Error Information Registers
//------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------//
// Host Side Error Count & Total bits Lane0-7 Diagnostics Selector 02h&03h Byte192-255
// Error Count Lane1 or 5 192-199
// Total Bits  Lane1 or 5 200-207
// Error Count Lane2 or 6 208-215
// Total Bits  Lane2 or 6 216-223
// Error Count Lane3 or 7 224-231
// Total Bits  Lane3 or 7 232-239
// Error Count Lane4 or 8 240-247
// Total Bits  Lane4 or 8 248-255
//------------------------------------------------------------------------------------------//
void Reply_Error_Count(uint8_t lane, uint64_t ErrorCount)
{
    uint8_t Start_Lane;
    
    // Get start lane
    Start_Lane=64+(lane*16);
    // Little-endian format (LSB first)
    for(uint8_t index=0;index<8;index++)
    {
        // LSB First
        QSFPDD_P14[Start_Lane+index]=ErrorCount>>(8*index);
    }
}

void Host_Side_Error_Count()
{
    uint64_t Error_Data=0;
    if(QSFPDD_P14[0]==0x02)
    {
        if(Diagnostics_Selector_Flag==0x01)
        {
            for(uint8_t lane=0;lane<4;lane++)
            {
                // Checker Enable
                if(QSFPDD_P13[32]&(0x01<<lane))
                {
                    Error_Data=DSP_System_Side_PRBS_Get_Error_Count(lane);
                    if(Error_Data==0xFFFFFFFFFFFFFFFF)
                    {
                        L_Host_Checker_LOL|=0x01<<lane;
                        QSFPDD_P14[10]|=0x01<<lane;
                    }
                    else
                        L_Host_Checker_LOL&=~(0x01<<lane);
                }
                // Checker Disable
                else
                {
                    L_Host_Checker_LOL|=0x01<<lane;
                    QSFPDD_P14[10]|=0x01<<lane;
                    Error_Data=0xFFFFFFFFFFFFFFFF;
                }
                Reply_Error_Count(lane, Error_Data);
            }
            Diagnostics_Selector_Flag&=~0x01;
        }
    }
    if(QSFPDD_P14[0]==0x03)
    {
        if(Diagnostics_Selector_Flag==0x02)
        {
            for(uint8_t lane=0;lane<4;lane++)
            {
                // Checker Enable
                if(QSFPDD_P13[32]&(0x10<<lane))
                {
                    Error_Data=DSP_System_Side_PRBS_Get_Error_Count(lane+4);
                    if(Error_Data==0xFFFFFFFFFFFFFFFF)
                    {
                        L_Host_Checker_LOL|=0x10<<lane;
                        QSFPDD_P14[10]|=0x10<<lane;
                    }
                    else
                        L_Host_Checker_LOL&=~(0x10<<lane);
                }
                // Checker Disable
                else
                {
                    L_Host_Checker_LOL|=0x10<<lane;
                    QSFPDD_P14[10]|=0x10<<lane;
                    Error_Data=0xFFFFFFFFFFFFFFFF;
                }
                Reply_Error_Count(lane, Error_Data);
            }
            Diagnostics_Selector_Flag&=~0x02;
        }
    }
}
//------------------------------------------------------------------------------------------//
// Media Side Error Count & Total bits Lane0-7 Diagnostics Selector 04h&05h Byte192-255
//------------------------------------------------------------------------------------------//
void Media_Side_Error_Count()
{
    uint64_t Error_Data;
    uint8_t Start_Lane;
    if(QSFPDD_P14[0]==0x04)
    {
        if(Diagnostics_Selector_Flag==0x04)
        {
            for(uint8_t lane=0;lane<4;lane++)
            {
                // Checker Enable
                if(QSFPDD_P13[40]&(0x01<<lane))
                {
                    Error_Data=DSP_Line_Side_PRBS_Get_Error_Count(lane);
                    if(Error_Data==0xFFFFFFFFFFFFFFFF)
                    {
                        L_Media_Checker_LOL|=0x01<<lane;
                        QSFPDD_P14[11]|=0x01<<lane;
                    }
                    else
                        L_Media_Checker_LOL&=~(0x01<<lane);
                }
                // Checker Disable
                else
                {
                    L_Media_Checker_LOL|=0x01<<lane;
                    QSFPDD_P14[11]|=0x01<<lane;
                    Error_Data=0xFFFFFFFFFFFFFFFF;
                }
                Reply_Error_Count(lane, Error_Data);
            }
            Diagnostics_Selector_Flag&=~0x04;
        }
    }
    if(QSFPDD_P14[0]==0x05)
    {
        if(Diagnostics_Selector_Flag==0x08)
        {
            for(uint8_t lane=0;lane<4;lane++)
            {
                // Checker Enable
                if(QSFPDD_P13[40]&(0x10<<lane))
                {
                    Error_Data=DSP_Line_Side_PRBS_Get_Error_Count(lane+4);
                    if(Error_Data==0xFFFFFFFFFFFFFFFF)
                    {
                        L_Media_Checker_LOL|=0x10<<lane;
                        QSFPDD_P14[11]|=0x10<<lane;
                    }
                    else
                        L_Media_Checker_LOL&=~(0x10<<lane);
                }
                // Checker Disable
                else
                {
                    L_Media_Checker_LOL|=0x10<<lane;
                    QSFPDD_P14[11]|=0x10<<lane;
                    Error_Data=0xFFFFFFFFFFFFFFFF;
                }
                Reply_Error_Count(lane, Error_Data);
            }
            Diagnostics_Selector_Flag&=~0x08;
        }
    }
}

//------------------------------------------------------------------------------------------//
// Media Side SNR Diagnostics Selector 06h Byte208-221
//------------------------------------------------------------------------------------------//
void Media_Side_SNR()
{
    uint8_t Data_Buffer[128];
    
    if(QSFPDD_P14[0]==0x06)
    {
        // Media Lane 0-3 SNR
        // Read Page86 Data from Flash
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86 , &Data_Buffer[0] , 128 );
        // Copy Read Data to Page86
        memcpy(  &DSP_Line_Side_PHY0_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // Lane0
        if(Rx_LOL_Buffer&0x01)
        {
            QSFPDD_P14[112]=0x00;
            QSFPDD_P14[113]=0x00;
            Trigger_SNR_Flag|=0x01;
        }
        // Get Real Data
        else if(Trigger_SNR_Flag&0x01)
        {
            if(SNR_Real_Data&0x01)
            {
                DSP_Line_Side_SNR_LTP_GET(0,DSP_PHY_ID_0);
                // MSB
                QSFPDD_P14[113]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[112]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH0=QSFPDD_P14[113];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH0=QSFPDD_P14[112];
                Trigger_SNR_Flag&=~0x01;
                SNR_Real_Data&=~0x01;
                Update_Page86_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[113]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH0;
                QSFPDD_P14[112]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH0;
                SNR_Real_Data|=0x01;
            }
        }
        // Lane1
        if(Rx_LOL_Buffer&0x02)
        {
            QSFPDD_P14[114]=0x00;
            QSFPDD_P14[115]=0x00;
            Trigger_SNR_Flag|=0x02;
        }
        else if(Trigger_SNR_Flag&0x02)
        {
            if(SNR_Real_Data&0x02)
            {
                DSP_Line_Side_SNR_LTP_GET(1,DSP_PHY_ID_0);
                // MSB
                QSFPDD_P14[115]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[114]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH1=QSFPDD_P14[115];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH1=QSFPDD_P14[114];
                Trigger_SNR_Flag&=~0x02;
                SNR_Real_Data&=~0x02;
                Update_Page86_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[115]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH1;
                QSFPDD_P14[114]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH1;
                SNR_Real_Data|=0x02;
            }
        }
        // Lane2
        if(Rx_LOL_Buffer&0x04)
        {
            QSFPDD_P14[116]=0x00;
            QSFPDD_P14[117]=0x00;
            Trigger_SNR_Flag|=0x04;
        }
        else if(Trigger_SNR_Flag&0x04)
        {
            if(SNR_Real_Data&0x04)
            {
                DSP_Line_Side_SNR_LTP_GET(2,DSP_PHY_ID_0);
                // MSB
                QSFPDD_P14[117]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[116]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH2=QSFPDD_P14[117];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH2=QSFPDD_P14[116];
                Trigger_SNR_Flag&=~0x04;
                SNR_Real_Data&=~0x04;
                Update_Page86_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[117]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH2;
                QSFPDD_P14[116]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH2;
                SNR_Real_Data|=0x04;
            }
        }
        // Lane3
        if(Rx_LOL_Buffer&0x08)
        {
            QSFPDD_P14[118]=0x00;
            QSFPDD_P14[119]=0x00;
            Trigger_SNR_Flag|=0x08;
        }
        else if(Trigger_SNR_Flag&0x08)
        {
            if(SNR_Real_Data&0x08)
            {
                DSP_Line_Side_SNR_LTP_GET(3,DSP_PHY_ID_0);
                // MSB
                QSFPDD_P14[119]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[118]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH3=QSFPDD_P14[119];
                DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH3=QSFPDD_P14[118];
                Trigger_SNR_Flag&=~0x08;
                SNR_Real_Data&=~0x08;
                Update_Page86_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[119]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_MSB_CH3;
                QSFPDD_P14[118]=DSP_Line_Side_PHY0_MEMORY_MAP.Line_side_SNR_LSB_CH3;
                SNR_Real_Data|=0x08;
            }
        }
        if(Update_Page86_Flash)
        {
            // Save Last SNR Data to Flash
            GDMCU_Flash_Erase(FS_DSP_LS_P86);
            memcpy(  &Data_Buffer[0]   , &DSP_Line_Side_PHY0_MEMORY_MAP , 128 );
            GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_LS_P86  , &Data_Buffer[0] , 128 );
            Update_Page86_Flash=0;
        }
        // Media Lane 4-7 SNR
        // Read Page8C Data from Flash
        GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P8C , &Data_Buffer[0] , 128 );
        // Copy Read Data to Page8C
        memcpy(  &DSP_Line_Side_PHY1_MEMORY_MAP   , &Data_Buffer[0] , 128 );
        // Lane4
        if(Rx_LOL_Buffer&0x10)
        {
            QSFPDD_P14[120]=0x00;
            QSFPDD_P14[121]=0x00;
            Trigger_SNR_Flag|=0x10;
        }
        // Get Real Data
        else if(Trigger_SNR_Flag&0x10)
        {
            if(SNR_Real_Data&0x10)
            {
                DSP_Line_Side_SNR_LTP_GET(0,DSP_PHY_ID_1);
                // MSB
                QSFPDD_P14[121]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[120]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH0=QSFPDD_P14[121];
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH0=QSFPDD_P14[120];
                Trigger_SNR_Flag&=~0x10;
                SNR_Real_Data&=~0x10;
                Update_Page8C_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[121]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH0;
                QSFPDD_P14[120]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH0;
                SNR_Real_Data|=0x10;
            }
        }
        // Lane5
        if(Rx_LOL_Buffer&0x20)
        {
            QSFPDD_P14[122]=0x00;
            QSFPDD_P14[123]=0x00;
            Trigger_SNR_Flag|=0x20;
        }
        // Get Real Data
        else if(Trigger_SNR_Flag&0x20)
        {
            if(SNR_Real_Data&0x20)
            {
                DSP_Line_Side_SNR_LTP_GET(1,DSP_PHY_ID_1);
                // MSB
                QSFPDD_P14[123]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[122]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH1=QSFPDD_P14[123];
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH1=QSFPDD_P14[122];
                Trigger_SNR_Flag&=~0x20;
                SNR_Real_Data&=~0x20;
                Update_Page8C_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[123]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH1;
                QSFPDD_P14[122]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH1;
                SNR_Real_Data|=0x20;
            }
        }
        // Lane6
        if(Rx_LOL_Buffer&0x40)
        {
            QSFPDD_P14[124]=0x00;
            QSFPDD_P14[125]=0x00;
            Trigger_SNR_Flag|=0x40;
        }
        // Get Real Data
        else if(Trigger_SNR_Flag&0x40)
        {
            if(SNR_Real_Data&0x40)
            {
                DSP_Line_Side_SNR_LTP_GET(2,DSP_PHY_ID_1);
                // MSB
                QSFPDD_P14[125]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[124]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH2=QSFPDD_P14[125];
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH2=QSFPDD_P14[124];
                Trigger_SNR_Flag&=~0x40;
                SNR_Real_Data&=~0x40;
                Update_Page8C_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[125]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH2;
                QSFPDD_P14[124]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH2;
                SNR_Real_Data|=0x40;
            }
        }
        // Lane7
        if(Rx_LOL_Buffer&0x80)
        {
            QSFPDD_P14[126]=0x00;
            QSFPDD_P14[127]=0x00;
            Trigger_SNR_Flag|=0x80;
        }
        // Get Real Data
        else if(Trigger_SNR_Flag&0x80)
        {
            if(SNR_Real_Data&0x80)
            {
                DSP_Line_Side_SNR_LTP_GET(3,DSP_PHY_ID_1);
                // MSB
                QSFPDD_P14[127]=DSP_LineSide_SNR>>8;
                // LSB
                QSFPDD_P14[126]=DSP_LineSide_SNR;
                // Save last value to flash
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH3=QSFPDD_P14[127];
                DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH3=QSFPDD_P14[126];
                Trigger_SNR_Flag&=~0x80;
                SNR_Real_Data&=~0x80;
                Update_Page8C_Flash|=1;
            }
            else
            {
                // Get last vaule to show first
                QSFPDD_P14[127]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_MSB_CH3;
                QSFPDD_P14[126]=DSP_Line_Side_PHY1_MEMORY_MAP.Line_side_SNR_LSB_CH3;
                SNR_Real_Data|=0x80;
            }
        }
        if(Update_Page8C_Flash)
        {
            // Save Last SNR Data to Flash
            GDMCU_Flash_Erase(FS_DSP_LS_P8C);
            memcpy(  &Data_Buffer[0]   , &DSP_Line_Side_PHY1_MEMORY_MAP , 128 );
            GDMCU_FMC_BytesWRITE_FUNCTION( FS_DSP_LS_P8C  , &Data_Buffer[0] , 128 );
            Update_Page8C_Flash=0;
        }
    }
}

void CMIS_PAGE14h_Diagnostics_Function()
{
    uint8_t Data[64]={0};
    
    // Diagnostics Selector 00h Byte192-255 Reserved clear to 0x00
    if((QSFPDD_P14[0]==0x00)||(QSFPDD_P14[0]==0x01))
        memcpy(  &QSFPDD_P14[64]   , &Data[0] , 64 );
    // Diagnostics Selector 01h - not support
    // Not Gated (Continuous) Error Counters, Individual Lanes,
    // Diagnostics Selector 02h 03h - Host_Side_Error_Count
    // Diagnostics Selector 04h 05h - Media_Side_Error_Count
    Host_Side_Error_Count();
    Media_Side_Error_Count();
    // Diagnostics Selector 06h
    Media_Side_SNR();
}