//----------------------------------------------//
// Data SRAM include
//----------------------------------------------//
//----------------------------------------------------//
//AW
//----------------------------------------------------//
#define ADDR_DPCF          6
#define ADDR_TXFAULT       7
#define ADDR_TXLOS         8
#define ADDR_TXCDR_LOL     9
#define ADDR_TX_EQ_FAULT  10
#define ADDR_TXP_HA       11
#define ADDR_TXP_LA       12
#define ADDR_TXP_HW       13
#define ADDR_TXP_LW       14
#define ADDR_TXB_HA       15
#define ADDR_TXB_LA       16
#define ADDR_TXB_HW       17
#define ADDR_TXB_LW       18
#define ADDR_RXLOS        19
#define ADDR_RXCDR_LOL    20
#define ADDR_RXP_HA       21
#define ADDR_RXP_LA       22
#define ADDR_RXP_HW       23
#define ADDR_RXP_LW       24
#define ADDR_HOST_CHK_LOL 25
#define ADDR_MEDIA_CHK_LOL 26
//---------------------------------------------------//
// Configuration Error Code Page11h 202-205
// 202 Lane1 0-3   Lane2 4-7
// 203 Lane3 0-3   Lane4 4-7
// 204 Lane5 0-3   Lane6 4-7
// 205 Lane7 0-3   Lane8 4-7
// feedback error code define
//---------------------------------------------------//
#define No_Status                      0x00
#define Config_In_Process              0x00
#define Accepted                       0x01
#define Rejected_Unknown               0x02
#define Rejected_Invalid_Code          0x03
#define Rejected_Invalid_Combo         0x04
#define Rejected_Invalid_SI            0x05
#define Rejected_In_Use                0x06
#define Rejected_Incomplete_Lane_Info  0x07
#define All_CH  8
#define CH_03   9
#define CH_47  10
//----------------------------------------------------//
//Rx Output Control Max Limit - PAM4
//Rx Output Control Max Limit - NRZ
//----------------------------------------------------//
#define Pre_Cursor_Max_Vaule        7
#define Post_Cursor_Max_Vaule       7
#define AMP_Max_Vaule               3
#define Max_Output_Value           16
//----------------------------------------------------//
//Signal Status
//----------------------------------------------------//
#define PAM4_to_PAM4 0
#define NRZ_to_NRZ   1
#define NRZ_to_PAM4  2
//--------------------------------------------------------//
//MODULE State Byte 3
//--------------------------------------------------------//
#define MODULE_MGMT_INIT    0x0
#define MODULE_LOW_PWR      0x2
#define MODULE_PWR_UP       0x4
#define MODULE_READY        0x6
#define MODULE_PWR_DN       0x8
#define MODULE_FAULT        0xA
//--------------------------------------------------------//
// Data Path State Indicator Encodings  Page11h 128 - 131
//--------------------------------------------------------//
#define DataPathDeactivated  0x1
#define DataPathInit         0x2
#define DataPathDeinit       0x3
#define DataPathActivated    0x4
#define DataPathTxTurnOn     0x5
#define DataPathTxTurnOff    0x6
#define DataPathInitialized  0x7
//----------------------------------------------------//
// Rx output value DSP Write or not
//----------------------------------------------------//
#define DSP_Write_Enable  1
#define DSP_Write_Disable 0
//----------------------------------------------------//
// Pattern Coding
//----------------------------------------------------//
#define PRBS_31Q    0
#define PRBS_31     1
#define PRBS_23Q    2
#define PRBS_23     3
#define PRBS_15Q    4
#define PRBS_15     5
#define PRBS_13Q    6
#define PRBS_13     7
#define PRBS_9Q     8
#define PRBS_9      9
#define PRBS_7Q     10
#define PRBS_7      11
#define SSPRQ       12
#define Unknown     255
//----------------------------------------------//
// GD32E501_Initialize.c
//----------------------------------------------//
void GD32E501_Power_on_Initial();
void Timer_Initialize();
//----------------------------------------------//
// GD32E501_ADC_Control.c
//----------------------------------------------//
uint16_t GET_ADC_Value_Data(uint16_t ADC_CH);
uint16_t GET_GD_Temperature();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Sequencing.c
//----------------------------------------------//
void ModSelL_Function();
void PowerOn_Sequencing_Control();
void SET_Power_Control(uint8_t SET_Value);
uint8_t Get_Power_C_Status();
void MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL();
void RESET_L_Function();
void PowerOn_Reset_Check();
void Get_CMP_Current_State();
//----------------------------------------------//
// QSFP56_SR4_PowerOn_Table.c
//----------------------------------------------//
void PowerON_Table_Init();
extern uint8_t QSFPDD_A0[272];
extern uint8_t QSFPDD_P0[128];
extern uint8_t QSFPDD_P1[128];
extern uint8_t QSFPDD_P2[128];
extern uint8_t QSFPDD_P3[128];
extern uint8_t QSFPDD_P10[128];
extern uint8_t QSFPDD_P11[128];
extern uint8_t QSFPDD_P13[128];
extern uint8_t QSFPDD_P14[128];
extern uint8_t QSFPDD_P9F[128];
extern uint8_t QSFPDD_PFF[144];
extern uint8_t CTEL_Table[16];
extern uint8_t PAM4_Pre_Table[16];
extern uint8_t PAM4_AMP_Table[16];
extern uint8_t PAM4_Post_Table[16];
extern uint8_t NRZ_Pre_Table[16];
extern uint8_t NRZ_AMP_Table[16];
extern uint8_t NRZ_Post_Table[16];
extern uint8_t PW_LEVE1[4];
//----------------------------------------------//
// GD32E501_Flash_RW.c
//----------------------------------------------//
void GDMCU_Flash_Erase(uint32_t FMC_ADR);
void GDMCU_FMC_READ_FUNCTION(uint32_t FMC_ADR,uint8_t *DataBuffer,uint16_t DATAL);
void GDMCU_FMC_BytesWRITE_FUNCTION(uint32_t FMC_ADR,uint8_t *Write_data,uint8_t DATAL);
uint8_t GDMCU_FMC_READ_DATA(uint32_t FMC_ADR);
uint16_t Swap_Bytes(uint16_t Data);
//----------------------------------------------//
// QSFPDD_MSA_AW.c
//----------------------------------------------//
void DDMI_AW_Monitor();
void Clear_Flag(uint8_t Current_AD);
void Clear_VCC_TEMP_Flag();
void Clear_Module_state_Byte8();
void CMIS40_TXLOS_LOL_AW();
void CMIS40_RXLOS_LOL_AW();
extern uint8_t Tx_LOS_Buffer;
extern uint8_t Tx_LOL_Buffer;
extern uint8_t Rx_LOS_Buffer;
extern uint8_t Rx_LOL_Buffer;
extern void IntL_Function();
extern uint8_t IntL_Trigger_Flag;
extern void QSFPDD_Temperature_VCC_AW();
extern void QSFPDD_RxPower_AW();
extern uint8_t Get_Support_DataPathState(uint8_t Lane);
//----------------------------------------------//
// QSFPDD_MSA_DDMI.c
//----------------------------------------------//
void Temperature_Monitor(int16_t tempsensor_value);
void VCC_Monitor( uint16_t VCC_Vaule);
void QSFPDD_DDMI_StateMachine(uint8_t StateMachine );
void Initialize_TRx_DDMI();
void QSFPDD_DDMI_ONLY_ADC_GET();
void MSA_DDMI_Function();
extern int8_t Temperature_Index; 
extern uint8_t PowerON_AWEN_flag;
//----------------------------------------------//
// QSFPDD_MSA_StateMachine.c
//----------------------------------------------//
void QSFPDD_MSA_StateMachine();
extern uint8_t Signal_Status;
extern uint8_t Module_Status;
extern uint8_t DSP_MODE_SET;
extern uint8_t Bef_DSP_MODE_SET;
extern uint8_t DDMI_Trigger_Flag;
//----------------------------------------------//
// Slave_I2C0_PB67.c
//----------------------------------------------//
void SRMA_Flash_Function();
//----------------------------------------------//
// QSFPDD_MSA_Optional_Control.c
//----------------------------------------------//
void Module_State(uint8_t ModuleState);
void Get_Module_Power_Monitor();
uint8_t Get_CH_Data (uint8_t Data,uint8_t CH);
//----------------------------------------------//
// QSFPDD_MSA_DatapathStateMachine.c
//----------------------------------------------//
void MSA_DataPath_StateMachine();
uint8_t DataPathDeactivateS(uint8_t Lane);
uint8_t LowPwrS();
uint8_t LowPwrExS();
void ResetS();
extern uint8_t Get_DataPathReDeinitS();
//----------------------------------------------//
// QSFPDD_MSA_Page10h_Control.c
//----------------------------------------------//
// 4Port 2Lanes
#define Lane_0_1    0
#define Lane_2_3    1
#define Lane_4_5    2
#define Lane_6_7    3
// 2Port 4Lanes
#define Lane_0_3    4
#define Lane_4_7    5
// 1Port 8Lanes
#define Lane_0_7    6
// 8Port 1Lane
#define Lane_Single 7
// Data Rate
#define PAM_53G_IB  0
#define PAM_53G     1
#define NRZ_25G     2

void MSA_DataPathState(uint8_t  DataPathState);
void MSA_Lane_DataPathState(uint8_t Lane, uint8_t DataPathState);
void Tx_Disable(uint8_t Lane,uint8_t Disable);
void Tx_Force_Squelch(uint8_t Lane,uint8_t Squelch_Value);
void CMIS_PAGE10h_Control();
void Apply_DataPathInit_Immediate(uint8_t Module_State);
void Rx_Output_Disable(uint8_t Control_Data);
extern uint8_t Tx_Los_Enable_Flag;
extern uint8_t Rx_Los_Enable_Flag;
extern uint8_t Bef_Tx_Disable;
extern uint8_t TxDIS_Power_flag;
extern uint8_t Bef_Rx_output;
extern uint8_t Mode_Changed_Flag;
extern uint8_t Tx_Disable_Flag;
extern uint8_t Active_AppSelCode[8];
extern uint8_t Auto_Squelch_Tx_Flag;
extern uint8_t Rx_output_flag;
//----------------------------------------------//
// QSFPDD_MSA_Page13h_Diagnostics.c
//----------------------------------------------//
// Page13h Byte177 Bit7 Reset all lanes
#define Individual_lane 0
#define All_lanes_banks 1
// Page13h Byte177 Bit1-3 Gate Time
#define Not_gated               0
#define gate_time_5_sec         1
#define gate_time_10_sec        2
#define gate_time_30_sec        3
#define gate_time_60_sec        4
#define gate_time_120_sec       5
#define gate_time_300_sec       6
uint8_t Get_SystemSide_PRBS_Pattern(uint8_t Data);
void CMIS_PAGE13h_Diagnostics_Function();
//----------------------------------------------//
//----------------------------------------------//
// gd32e501_it.c
//----------------------------------------------//
extern uint8_t INT_RSTn_G_Flag;
extern uint8_t LPWn_PRSn_G_Flag;
//----------------------------------------------//
// QSFPDD_MSA_Page14h_Diagnostics2.c
//----------------------------------------------//
void CMIS_PAGE14h_Diagnostics_Function();
extern uint8_t L_Host_Checker_LOL;
extern uint8_t L_Media_Checker_LOL;
extern uint8_t Diagnostics_Selector_Flag;