#include "gd32e501.h"
#include "CMIS_MSA.h"
#include "MCU_GPIO_Customize_Define.h"
//----------------------------------------//
// GPIO initialize
// Analogy
//----------------------------------------//
// PA1  Analogy     - P1V8 Monitor     - ADC_CHANNEL_1  ADC_IN1
// PB0  Analogy     - P3V3 Rx Monitor  - ADC_CHANNEL_8  ADC_IN8
// PB2  Analogy     - P3V3 Tx Monitor  - ADC_CHANNEL_4  ADC_IN4
// PB5  Analogy     - RSSI_1 Monitor   - ADC_CHANNEL_7  ADC_IN7
// PC3  Analogy     - RSSI_2 Monitor   - ADC_CHANNEL_13 ADC_IN13
// Temperature sensro                  - ADC_CHANNEL_16    

uint16_t GET_ADC_Value_Data(uint16_t ADC_CH)
{
    uint16_t adc_value_mV;
    uint32_t TimeOut_count;
    /* ADC regular channel config */
    adc_inserted_channel_config(0U, ADC_CH, ADC_SAMPLETIME_55POINT5);
    // ADC software trigger enable
    adc_software_trigger_enable(ADC_INSERTED_CHANNEL);
    
    for(TimeOut_count=0;TimeOut_count<100;TimeOut_count++);
    
    if(adc_flag_get(ADC_FLAG_EOIC))
        adc_flag_clear(ADC_FLAG_EOIC);

    adc_value_mV = ADC_IDATA0*ADC_CONV_Value_100uV;   
    
    return adc_value_mV;
}

uint16_t GET_GD_Temperature()
{
    int16_t Temperature;
    uint16_t adc_value_mV;
    uint32_t TimeOut_count;
    /* ADC regular channel config */
    adc_inserted_channel_config(0U, GD_Temp_Sensor, ADC_SAMPLETIME_55POINT5);
    // ADC software trigger enable
    adc_software_trigger_enable(ADC_INSERTED_CHANNEL);
    
    for(TimeOut_count=0;TimeOut_count<100;TimeOut_count++);
    
    if(adc_flag_get(ADC_FLAG_EOIC))
        adc_flag_clear(ADC_FLAG_EOIC);

    adc_value_mV = ADC_IDATA0*0.61; 

    Temperature  = (( 1430 - adc_value_mV ) / 4.3 + 25 )*256;  
    
    return Temperature;
}






