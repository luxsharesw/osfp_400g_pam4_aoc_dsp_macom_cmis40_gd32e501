#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MCU_GPIO_Customize_Define.h"
#include "GD_FlahMap.h"
#include "string.h"
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
void Get_Module_Power_Monitor()
{
    uint16_t GetADC_Buffer ;
    //--------------------------------------------------------------//
    // P1V8 Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( P1V8_TX_Mon );
    CALIB_MEMORY_1_MAP.P1V8_TX_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.P1V8_TX_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // TX P3V3 Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_TX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.P3V3_TX_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.P3V3_TX_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // RX P3V3 Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( P3V3_RX_Mon )*2 ;
    CALIB_MEMORY_1_MAP.P3V3_RX_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.P3V3_RX_ADC_MSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // AVDD_0P75A Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( AVDD_0P75A );
    CALIB_MEMORY_1_MAP.AVDD_0P75A_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.AVDD_0P75A_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // AVDD_0P90 Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( AVDD_0P90 );
    CALIB_MEMORY_1_MAP.AVDD_0P90_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.AVDD_0P90_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // AVDD_0P75B Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_ADC_Value_Data( AVDD_0P75B );
    CALIB_MEMORY_1_MAP.AVDD_0P75B_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.AVDD_0P75B_ADC_LSB = GetADC_Buffer ;
    //--------------------------------------------------------------//
    // Temperature Monitor
    //--------------------------------------------------------------//
    GetADC_Buffer = GET_GD_Temperature();
	CALIB_MEMORY_1_MAP.TEMP_ADC_MSB = GetADC_Buffer >> 8 ;
    CALIB_MEMORY_1_MAP.TEMP_ADC_LSB = GetADC_Buffer;
    //--------------------------------------------------------------//
    // PowerControl Enable status
    //--------------------------------------------------------------//
    CALIB_MEMORY_1_MAP.Power_C_Status  = Get_Power_C_Status();
}

//----------------------------------------------//
// Channel 0 , 2 , 4 , 6 on Bit0-3              //
// Channel 1 , 3 , 5 , 7 on Bit4-7              //
//----------------------------------------------//
uint8_t Get_CH_Data (uint8_t Data,uint8_t CH)
{
	uint8_t Data_Buffer = 0;
	if( CH%2 == 0 )
		Data_Buffer = ( Data & 0x0F ) ;
	else
		Data_Buffer = ( ( Data & 0xF0 ) >>4 ) ;
	
	return Data_Buffer;
}

void Clear_U16_Array_Data(uint16_t *Array, uint16_t Data, uint16_t Size)
{
    for(uint16_t i=0;i<Size;i++)
    {
        *Array++=Data;
    }
}

//-----------------------------------------------------------------------------------------------------//
// Low Page Status                                                                                     //
//-----------------------------------------------------------------------------------------------------//
//------------------------------------------------------------//
// Module State Byte 0x03 and State Changed Flag Byte0x08 Bit0
//------------------------------------------------------------//
void Module_State(uint8_t ModuleState)
{
	//Module State Change
	QSFPDD_A0[3] = ModuleState ;
	//Module State Changed Flag
	if((ModuleState==MODULE_LOW_PWR)||(ModuleState==MODULE_READY))
    {
		QSFPDD_A0[8] |= 0x01 ;
		IntL_Trigger_Flag=1;
    }
}

