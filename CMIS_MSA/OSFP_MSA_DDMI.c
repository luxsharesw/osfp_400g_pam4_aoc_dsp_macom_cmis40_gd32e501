#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "stdint.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "MCU_GPIO_Customize_Define.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "LDD_TX1_TX4.h"
#include "LDD_TX5_TX8.h"
#include "TIA_RX1_RX4.h"
#include "TIA_RX5_RX8.h"

#define ADC_SM            0
#define RXP_CH0_CH3       1
#define RXP_CH4_CH7       2
#define TXB_TXP_CH01_SM   3
#define TXB_TXP_CH23_SM   4
#define TXB_TXP_CH45_SM   5
#define TXB_TXP_CH67_SM   6
#define OP_SM             7
#define TXLOLLOS          8
#define RXLOLLOS          9
#define Other_DDMI        10

#define SHOW_RSSI_Enable   1
#define SHOW_RSSI_Disable  0

uint8_t SHOW_RSSI_CH0 = 1 ;
uint8_t SHOW_RSSI_CH1 = 1 ;
uint8_t SHOW_RSSI_CH2 = 1 ;
uint8_t SHOW_RSSI_CH3 = 1 ;
uint8_t SHOW_RSSI_CH4 = 1 ;
uint8_t SHOW_RSSI_CH5 = 1 ;
uint8_t SHOW_RSSI_CH6 = 1 ;
uint8_t SHOW_RSSI_CH7 = 1 ;
int8_t Temperature_Index = 0 ;
uint16_t Bias_Current_CH0,Bias_Current_CH1,Bias_Current_CH2,Bias_Current_CH3;
uint16_t Bias_Current_CH4,Bias_Current_CH5,Bias_Current_CH6,Bias_Current_CH7;

//------------------------------------------------------------------------------------------//
// DDMI Flags
//------------------------------------------------------------------------------------------//
uint8_t DDMI_Update_C = 0 ;
uint8_t PowerON_AWEN_flag = 0 ;
uint8_t AW_DDMI_Count = 0 ;
uint16_t DDMI_DataCount = 0 ;

int16_t Temp_CAL( int16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset)
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset * 256 ;

    return Vaule ;
}

uint16_t CAL_Function( uint16_t Vaule , uint8_t Slop , uint8_t Shift , int16_t Offset )
{
	uint32_t Buffer_temp ;

	Buffer_temp = (uint32_t)Vaule * Slop ;
	Buffer_temp = (uint32_t)Buffer_temp >> Shift ;

	Vaule = (uint32_t)Buffer_temp + Offset;

	return Vaule;
}

void Temperature_Monitor(int16_t tempsensor_value)
{
	int16_t Temp_Buffer;
	int8_t  Indx_Temp ;

	Temp_Buffer = tempsensor_value ;

	Temp_Buffer = Temp_CAL(tempsensor_value,CALIB_MEMORY_MAP.TEMP_SCALE1M,CALIB_MEMORY_MAP.TEMP_SCALE1L,CALIB_MEMORY_MAP.TEMP_OFFSET1);

	QSFPDD_A0[14] = Temp_Buffer >> 8 ;
	QSFPDD_A0[15] = Temp_Buffer ;

	Indx_Temp = QSFPDD_A0[14];
    
    if(Indx_Temp<-40)
        Indx_Temp = -40 ;

    if(Indx_Temp>85)
        Indx_Temp = 86 ;

	Temperature_Index = (Indx_Temp + 40) /2;
    //CALIB_MEMORY_MAP.Temp_Index = Temperature_Indx;
	//if(Temperature_Index > 63) 
        //Temperature_Index = 63;
}

void VCC_Monitor( uint16_t VCC_Vaule)
{
    int16_t  offset_Buffer_VCC = 0 ;
    offset_Buffer_VCC = (CALIB_MEMORY_MAP.VCC_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.VCC_OFFSET_LSB );
    
	VCC_Vaule = CAL_Function( VCC_Vaule , CALIB_MEMORY_MAP.VCC_SCALEM , CALIB_MEMORY_MAP.VCC_SCALEL , offset_Buffer_VCC ) ;

	QSFPDD_A0[16] = VCC_Vaule >> 8 ;
	QSFPDD_A0[17] = VCC_Vaule ;
}

void BIAS_TxPower_Monitor( uint8_t Channel)
{
	uint16_t MOD_Temp_Buffer = 0;
	uint16_t TXP_BUFFER = 0 ;  
    int16_t  offset_Buffer_B = 0 ;
    int16_t  offset_Buffer_P = 0 ;

	switch(Channel)
	{
		case 0:
				Bias_Current_CH0 = LDD_TX1_TX4_BIAS(0) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH0>0x2000)||(Bias_Current_CH0<0x0400))
					Bias_Current_CH0 = (uint16_t) (QSFPDD_P11[42] << 8) | QSFPDD_P11[43];
                
				TXP_BUFFER = Bias_Current_CH0 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias0_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP0_OFFSET_LSB );
        
				Bias_Current_CH0 = CAL_Function( Bias_Current_CH0 , CALIB_MEMORY_MAP.IBias0_SCALEM , CALIB_MEMORY_MAP.IBias0_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP0_SCALEM , CALIB_MEMORY_MAP.TXP0_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x01) || (Tx_Disable_Flag & 0x01))
				{
					QSFPDD_P11[42] = 0 ;
					QSFPDD_P11[43] = 0 ;
					// CH0  Tx Power
					QSFPDD_P11[26] = 0 ;
					QSFPDD_P11[27] = 0 ;
                    // Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x01 ;
						QSFPDD_P11[14] |= 0x01 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x01 ;
						QSFPDD_P11[18] |= 0x01 ;
					}
				}
				else
				{
					QSFPDD_P11[42] = Bias_Current_CH0 >> 8 ;
					QSFPDD_P11[43] = Bias_Current_CH0 ;
					// CH0 TxPower
					QSFPDD_P11[26] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[27] = TXP_BUFFER ;
				}
				break;
		case 1:
				Bias_Current_CH1 = LDD_TX1_TX4_BIAS(1) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH1>0x2000)||(Bias_Current_CH1<0x0400))
					Bias_Current_CH1 = (uint16_t) (QSFPDD_P11[44] << 8) | QSFPDD_P11[45];
                        
				TXP_BUFFER = Bias_Current_CH1 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias1_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP1_OFFSET_LSB );

				Bias_Current_CH1 = CAL_Function( Bias_Current_CH1 , CALIB_MEMORY_MAP.IBias1_SCALEM , CALIB_MEMORY_MAP.IBias1_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP1_SCALEM , CALIB_MEMORY_MAP.TXP1_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x02) || (Tx_Disable_Flag & 0x02))
				{
					QSFPDD_P11[44] = 0 ;
					QSFPDD_P11[45] = 0 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = 0 ;
					QSFPDD_P11[29] = 0 ;
                    // Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x02 ;
						QSFPDD_P11[14] |= 0x02 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x02 ;
						QSFPDD_P11[18] |= 0x02 ;
					}
				}
				else
				{
					QSFPDD_P11[44] = Bias_Current_CH1 >> 8 ;
					QSFPDD_P11[45] = Bias_Current_CH1 ;
					// CH1  Tx Power
					QSFPDD_P11[28] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[29] = TXP_BUFFER ;
				}
				break;
		case 2:
				Bias_Current_CH2 = LDD_TX1_TX4_BIAS(2) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH2>0x2000)||(Bias_Current_CH2<0x0400))
					Bias_Current_CH2 = (uint16_t) (QSFPDD_P11[46] << 8) | QSFPDD_P11[47];
                        
				TXP_BUFFER = Bias_Current_CH2 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias2_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP2_OFFSET_LSB );

				Bias_Current_CH2 = CAL_Function( Bias_Current_CH2 , CALIB_MEMORY_MAP.IBias2_SCALEM , CALIB_MEMORY_MAP.IBias2_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP2_SCALEM , CALIB_MEMORY_MAP.TXP2_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x04) || (Tx_Disable_Flag & 0x04))
				{
					QSFPDD_P11[46] = 0 ;
					QSFPDD_P11[47] = 0 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = 0 ;
					QSFPDD_P11[31] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x04 ;
						QSFPDD_P11[14] |= 0x04 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x04 ;
						QSFPDD_P11[18] |= 0x04 ;
					}
				}
				else
				{
					QSFPDD_P11[46] = Bias_Current_CH2 >> 8 ;
					QSFPDD_P11[47] = Bias_Current_CH2 ;
					// CH2  Tx Power
					QSFPDD_P11[30] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[31] = TXP_BUFFER ;
				}
			    break;
		case 3:
				Bias_Current_CH3 = LDD_TX1_TX4_BIAS(3) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH3>0x2000)||(Bias_Current_CH3<0x0400))
					Bias_Current_CH3 = (uint16_t) (QSFPDD_P11[48] << 8) | QSFPDD_P11[49];
                
				TXP_BUFFER = Bias_Current_CH3 ;
        
                offset_Buffer_B = (CALIB_MEMORY_MAP.IBias3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.IBias3_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_MAP.TXP3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.TXP3_OFFSET_LSB );

				Bias_Current_CH3 = CAL_Function( Bias_Current_CH3 , CALIB_MEMORY_MAP.IBias3_SCALEM , CALIB_MEMORY_MAP.IBias3_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_MAP.TXP3_SCALEM , CALIB_MEMORY_MAP.TXP3_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x08) || (Tx_Disable_Flag & 0x08))
				{
					QSFPDD_P11[48] = 0 ;
					QSFPDD_P11[49] = 0 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = 0 ;
					QSFPDD_P11[33] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x08 ;
						QSFPDD_P11[14] |= 0x08 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x08 ;
						QSFPDD_P11[18] |= 0x08 ;
					}
				}
				else
				{
					QSFPDD_P11[48] = Bias_Current_CH3 >> 8 ;
					QSFPDD_P11[49] = Bias_Current_CH3 ;
					// CH3  Tx Power
					QSFPDD_P11[32] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[33] = TXP_BUFFER ;
				}
				break;
        case 4:
				Bias_Current_CH4 = LDD_TX5_TX8_BIAS(0) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH4>0x2000)||(Bias_Current_CH4<0x0400))
					Bias_Current_CH4 = (uint16_t) (QSFPDD_P11[50] << 8) | QSFPDD_P11[51];
                
				TXP_BUFFER = Bias_Current_CH4 ;
        
                offset_Buffer_B = (CALIB_MEMORY_1_MAP.IBias4_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.IBias4_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_1_MAP.TXP4_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.TXP4_OFFSET_LSB );

				Bias_Current_CH4 = CAL_Function( Bias_Current_CH4 , CALIB_MEMORY_1_MAP.IBias4_SCALEM , CALIB_MEMORY_1_MAP.IBias4_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP4_SCALEM , CALIB_MEMORY_1_MAP.TXP4_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x10) || (Tx_Disable_Flag & 0x10))
				{
					QSFPDD_P11[50] = 0 ;
					QSFPDD_P11[51] = 0 ;
					// CH4  Tx Power
					QSFPDD_P11[34] = 0 ;
					QSFPDD_P11[35] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x10 ;
						QSFPDD_P11[14] |= 0x10 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x10 ;
						QSFPDD_P11[18] |= 0x10 ;
					}
				}
				else
				{
					QSFPDD_P11[50] = Bias_Current_CH4 >> 8 ;
					QSFPDD_P11[51] = Bias_Current_CH4 ;
					// CH4  Tx Power
					QSFPDD_P11[34] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[35] = TXP_BUFFER ;
				}
				break;
        case 5:
				Bias_Current_CH5 = LDD_TX5_TX8_BIAS(1) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH5>0x2000)||(Bias_Current_CH5<0x0400))
					Bias_Current_CH5 = (uint16_t) (QSFPDD_P11[52] << 8) | QSFPDD_P11[53];
                
				TXP_BUFFER = Bias_Current_CH5 ;
        
                offset_Buffer_B = (CALIB_MEMORY_1_MAP.IBias5_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.IBias5_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_1_MAP.TXP5_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.TXP5_OFFSET_LSB );

				Bias_Current_CH5 = CAL_Function( Bias_Current_CH5 , CALIB_MEMORY_1_MAP.IBias5_SCALEM , CALIB_MEMORY_1_MAP.IBias5_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP5_SCALEM , CALIB_MEMORY_1_MAP.TXP5_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x20) || (Tx_Disable_Flag & 0x20))
				{
					QSFPDD_P11[52] = 0 ;
					QSFPDD_P11[53] = 0 ;
					// CH5  Tx Power
					QSFPDD_P11[36] = 0 ;
					QSFPDD_P11[37] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x20 ;
						QSFPDD_P11[14] |= 0x20 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x20 ;
						QSFPDD_P11[18] |= 0x20 ;
					}
				}
				else
				{
					QSFPDD_P11[52] = Bias_Current_CH5 >> 8 ;
					QSFPDD_P11[53] = Bias_Current_CH5 ;
					// CH5  Tx Power
					QSFPDD_P11[36] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[37] = TXP_BUFFER ;
				}
				break;
        case 6:
				Bias_Current_CH6 = LDD_TX5_TX8_BIAS(2) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH6>0x2000)||(Bias_Current_CH6<0x0400))
					Bias_Current_CH6 = (uint16_t) (QSFPDD_P11[54] << 8) | QSFPDD_P11[55];
                
				TXP_BUFFER = Bias_Current_CH6 ;
        
                offset_Buffer_B = (CALIB_MEMORY_1_MAP.IBias6_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.IBias6_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_1_MAP.TXP6_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.TXP6_OFFSET_LSB );

				Bias_Current_CH6 = CAL_Function( Bias_Current_CH6 , CALIB_MEMORY_1_MAP.IBias6_SCALEM , CALIB_MEMORY_1_MAP.IBias6_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP6_SCALEM , CALIB_MEMORY_1_MAP.TXP6_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x40) || (Tx_Disable_Flag & 0x40))
				{
					QSFPDD_P11[54] = 0 ;
					QSFPDD_P11[55] = 0 ;
					// CH6  Tx Power
					QSFPDD_P11[38] = 0 ;
					QSFPDD_P11[39] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x40 ;
						QSFPDD_P11[14] |= 0x40 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x40 ;
						QSFPDD_P11[18] |= 0x40 ;
					}
				}
				else
				{
					QSFPDD_P11[54] = Bias_Current_CH6 >> 8 ;
					QSFPDD_P11[55] = Bias_Current_CH6 ;
					// CH6  Tx Power
					QSFPDD_P11[38] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[39] = TXP_BUFFER ;
				}
				break;
        case 7:
				Bias_Current_CH7 = LDD_TX5_TX8_BIAS(3) ;
                // Vcsel Driver Max Bias is 15.2mA set max limit to 16.384mA=>0x2000 and min limit to 2.048mA
                if((Bias_Current_CH7>0x2000)||(Bias_Current_CH7<0x0400))
					Bias_Current_CH7 = (uint16_t) (QSFPDD_P11[56] << 8) | QSFPDD_P11[57];
                
				TXP_BUFFER = Bias_Current_CH7 ;
        
                offset_Buffer_B = (CALIB_MEMORY_1_MAP.IBias7_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.IBias7_OFFSET_LSB );
                offset_Buffer_P = (CALIB_MEMORY_1_MAP.TXP7_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.TXP7_OFFSET_LSB );

				Bias_Current_CH7 = CAL_Function( Bias_Current_CH7 , CALIB_MEMORY_1_MAP.IBias7_SCALEM , CALIB_MEMORY_1_MAP.IBias7_SCALE1L , offset_Buffer_B ) ;
				TXP_BUFFER = CAL_Function( TXP_BUFFER , CALIB_MEMORY_1_MAP.TXP7_SCALEM , CALIB_MEMORY_1_MAP.TXP7_SCALEL , offset_Buffer_P ) ;

				if((QSFPDD_P10[2] & 0x80) || (Tx_Disable_Flag & 0x80))
				{
					QSFPDD_P11[56] = 0 ;
					QSFPDD_P11[57] = 0 ;
					// CH7  Tx Power
					QSFPDD_P11[40] = 0 ;
					QSFPDD_P11[41] = 0 ;
					// Not support datapath do not return flag status
					if(Get_Support_DataPathState(Channel))
					{
						// Tx Power Low Alarm Warning Trigger
						QSFPDD_P11[12] |= 0x80 ;
						QSFPDD_P11[14] |= 0x80 ;
						// Tx Bias Low Alarm Warning Trigger
						QSFPDD_P11[16] |= 0x80 ;
						QSFPDD_P11[18] |= 0x80 ;
					}
				}
				else
				{
					QSFPDD_P11[56] = Bias_Current_CH7 >> 8 ;
					QSFPDD_P11[57] = Bias_Current_CH7 ;
					// CH7  Tx Power
					QSFPDD_P11[40] = TXP_BUFFER >> 8 ;
					QSFPDD_P11[41] = TXP_BUFFER ;
				}
				break;
                
		default:
				break;
	}

}

void RX_POWER_M_CH0_CH7(uint8_t Rx_CH)
{
	uint16_t RX_POWER_CH0,RX_POWER_CH1,RX_POWER_CH2,RX_POWER_CH3;
	uint16_t RX_POWER_CH4,RX_POWER_CH5,RX_POWER_CH6,RX_POWER_CH7;
    
    int16_t  offset_Buffer_RXP = 0 ;
    uint16_t RXLOS_Assert_Buffer = 0 ;
    uint16_t RXLOS_DeAssert_Buffer = 0 ;

	switch(Rx_CH)
	{
		case 0:
			    RX_POWER_CH0 = TIA_RX1_RX4_RSSI( 3 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX0_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX0_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx0_LOS_DeAssret_LSB  );

			    RX_POWER_CH0 = CAL_Function( RX_POWER_CH0 , CALIB_MEMORY_MAP.RX0_SCALEM , CALIB_MEMORY_MAP.RX0_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH0>0xF000)
			    	RX_POWER_CH0 = 0;
				// RX RSSI CH 0
				if( RX_POWER_CH0 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Disable ;

				if ( RX_POWER_CH0 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH0 = SHOW_RSSI_Enable ;

				if( SHOW_RSSI_CH0 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[58] = RX_POWER_CH0 >> 8 ;
					QSFPDD_P11[59] = RX_POWER_CH0 ;
				}
				else
				{
					QSFPDD_P11[58] = 0 ;
					QSFPDD_P11[59] = 1 ;
				}

				break;
		case 1:
			    RX_POWER_CH1 = TIA_RX1_RX4_RSSI( 2 );
        
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX1_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX1_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx1_LOS_DeAssret_LSB  );        

			    RX_POWER_CH1 = CAL_Function( RX_POWER_CH1 , CALIB_MEMORY_MAP.RX1_SCALEM , CALIB_MEMORY_MAP.RX1_SCALEL , offset_Buffer_RXP );
			    if(RX_POWER_CH1>0xF000)
			    	RX_POWER_CH1 = 0;
				// RX RSSI CH 1
				if( RX_POWER_CH1 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Disable;

				if( RX_POWER_CH1 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH1 = SHOW_RSSI_Enable;

				if( SHOW_RSSI_CH1 == SHOW_RSSI_Enable )
				{
					QSFPDD_P11[60] = RX_POWER_CH1 >> 8 ;
					QSFPDD_P11[61] = RX_POWER_CH1 ;
				}
				else
				{
					QSFPDD_P11[60] = 0;
					QSFPDD_P11[61] = 1 ;
				}

				break;
		case 2:
				RX_POWER_CH2 = TIA_RX1_RX4_RSSI( 1 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX2_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX2_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx2_LOS_DeAssret_LSB  );             
        
				RX_POWER_CH2 = CAL_Function( RX_POWER_CH2 , CALIB_MEMORY_MAP.RX2_SCALEM , CALIB_MEMORY_MAP.RX2_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH2>0xF000)
					RX_POWER_CH2 = 0;
				// RX RSSI CH 2
				if( RX_POWER_CH2 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Disable;

				if( RX_POWER_CH2 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH2 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH2 == SHOW_RSSI_Enable)
				{
					QSFPDD_P11[62] = RX_POWER_CH2 >> 8 ;
					QSFPDD_P11[63] = RX_POWER_CH2 ;
				}
				else
				{
					QSFPDD_P11[62] = 0;
					QSFPDD_P11[63] = 1 ;
				}

				break;
		case 3:
				RX_POWER_CH3 = TIA_RX1_RX4_RSSI( 0 );
                offset_Buffer_RXP = (CALIB_MEMORY_MAP.RX3_OFFSET_MSB << 8 | CALIB_MEMORY_MAP.RX3_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_Assret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_MAP.Rx3_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH3 = CAL_Function( RX_POWER_CH3 , CALIB_MEMORY_MAP.RX3_SCALEM , CALIB_MEMORY_MAP.RX3_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH3>0xF000)
					RX_POWER_CH3 = 0;
				// RX RSSI CH 3
				if( RX_POWER_CH3 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Disable;

				if( RX_POWER_CH3 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH3 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH3==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[64] = RX_POWER_CH3 >> 8 ;
					QSFPDD_P11[65] = RX_POWER_CH3 ;
				}
				else
				{
					QSFPDD_P11[64] = 0;
					QSFPDD_P11[65] = 1 ;
				}
				break;
		case 4:
				RX_POWER_CH4 = TIA_RX5_RX8_RSSI( 3 );
                offset_Buffer_RXP = (CALIB_MEMORY_1_MAP.RX4_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.RX4_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_1_MAP.Rx4_LOS_Assret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx4_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_1_MAP.Rx4_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx4_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH4 = CAL_Function( RX_POWER_CH4 , CALIB_MEMORY_1_MAP.RX4_SCALEM , CALIB_MEMORY_1_MAP.RX4_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH4>0xF000)
					RX_POWER_CH4 = 0;
				// RX RSSI CH 4
				if( RX_POWER_CH4 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH4 = SHOW_RSSI_Disable;

				if( RX_POWER_CH4 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH4 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH4==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[66] = RX_POWER_CH4 >> 8 ;
					QSFPDD_P11[67] = RX_POWER_CH4 ;
				}
				else
				{
					QSFPDD_P11[66] = 0;
					QSFPDD_P11[67] = 1 ;
				}
				break;
        case 5:
				RX_POWER_CH5 = TIA_RX5_RX8_RSSI( 2 );
                offset_Buffer_RXP = (CALIB_MEMORY_1_MAP.RX5_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.RX5_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_1_MAP.Rx5_LOS_Assret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx5_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_1_MAP.Rx5_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx5_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH5 = CAL_Function( RX_POWER_CH5 , CALIB_MEMORY_1_MAP.RX5_SCALEM , CALIB_MEMORY_1_MAP.RX5_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH5>0xF000)
					RX_POWER_CH5 = 0;
				// RX RSSI CH 5
				if( RX_POWER_CH5 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH5 = SHOW_RSSI_Disable;

				if( RX_POWER_CH5 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH5 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH5==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[68] = RX_POWER_CH5 >> 8 ;
					QSFPDD_P11[69] = RX_POWER_CH5 ;
				}
				else
				{
					QSFPDD_P11[68] = 0;
					QSFPDD_P11[69] = 1 ;
				}
				break;
        case 6:
				RX_POWER_CH6 = TIA_RX5_RX8_RSSI( 1 );
                offset_Buffer_RXP = (CALIB_MEMORY_1_MAP.RX6_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.RX6_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_1_MAP.Rx6_LOS_Assret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx6_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_1_MAP.Rx6_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx6_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH6 = CAL_Function( RX_POWER_CH6 , CALIB_MEMORY_1_MAP.RX6_SCALEM , CALIB_MEMORY_1_MAP.RX6_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH6>0xF000)
					RX_POWER_CH6 = 0;
				// RX RSSI CH 6
				if( RX_POWER_CH6 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH6 = SHOW_RSSI_Disable;

				if( RX_POWER_CH6 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH6 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH6==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[70] = RX_POWER_CH6 >> 8 ;
					QSFPDD_P11[71] = RX_POWER_CH6 ;
				}
				else
				{
					QSFPDD_P11[70] = 0;
					QSFPDD_P11[71] = 1 ;
				}
				break;
        case 7:
				RX_POWER_CH7 = TIA_RX5_RX8_RSSI( 0 );
                offset_Buffer_RXP = (CALIB_MEMORY_1_MAP.RX7_OFFSET_MSB << 8 | CALIB_MEMORY_1_MAP.RX7_OFFSET_LSB );
                RXLOS_Assert_Buffer = (CALIB_MEMORY_1_MAP.Rx7_LOS_Assret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx7_LOS_Assret_LSB  );
                RXLOS_DeAssert_Buffer = (CALIB_MEMORY_1_MAP.Rx7_LOS_DeAssret_MSB << 8 | CALIB_MEMORY_1_MAP.Rx7_LOS_DeAssret_LSB  );    
        
				RX_POWER_CH7 = CAL_Function( RX_POWER_CH7 , CALIB_MEMORY_1_MAP.RX7_SCALEM , CALIB_MEMORY_1_MAP.RX7_SCALEL , offset_Buffer_RXP );
				if(RX_POWER_CH7>0xF000)
					RX_POWER_CH7 = 0;
				// RX RSSI CH 7
				if( RX_POWER_CH7 < RXLOS_Assert_Buffer )
					SHOW_RSSI_CH7 = SHOW_RSSI_Disable;

				if( RX_POWER_CH7 >= RXLOS_DeAssert_Buffer )
					SHOW_RSSI_CH7 = SHOW_RSSI_Enable;

				if(SHOW_RSSI_CH7==SHOW_RSSI_Enable)
				{
					QSFPDD_P11[72] = RX_POWER_CH7 >> 8 ;
					QSFPDD_P11[73] = RX_POWER_CH7 ;
				}
				else
				{
					QSFPDD_P11[72] = 0;
					QSFPDD_P11[73] = 1 ;
				}
				break;
		default:
			    break;
	}
}

void QSFPDD_DDMI_StateMachine(uint8_t StateMachine )
{
	switch(StateMachine)
	{
		case ADC_SM:
					Temperature_Monitor(GET_GD_Temperature());
					VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
					break;
        
		case RXP_CH0_CH3:
					RX_POWER_M_CH0_CH7(0);
					RX_POWER_M_CH0_CH7(1);
					RX_POWER_M_CH0_CH7(2);
					RX_POWER_M_CH0_CH7(3);
					break;
        
        case RXP_CH4_CH7:
					RX_POWER_M_CH0_CH7(4);
					RX_POWER_M_CH0_CH7(5);
					RX_POWER_M_CH0_CH7(6);
					RX_POWER_M_CH0_CH7(7);
					break;
        
		case TXB_TXP_CH01_SM:
                    
					if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX1_TX4(0); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX1_TX4(1); // Bias & mod LUT Control
                    }
                    BIAS_TxPower_Monitor( 0 );
                    BIAS_TxPower_Monitor( 1 );
					break;
                    
		case TXB_TXP_CH23_SM:
                    
					if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX1_TX4(2); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX1_TX4(3); // Bias & mod LUT Control
                    }
                    BIAS_TxPower_Monitor( 2 );
                    BIAS_TxPower_Monitor( 3 );
					break;
                    
        case TXB_TXP_CH45_SM:
					if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX5_TX8(0); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX5_TX8(1); // Bias & mod LUT Control
                    }
                    BIAS_TxPower_Monitor( 4 );
                    BIAS_TxPower_Monitor( 5 );
                    break;
                    
        case TXB_TXP_CH67_SM:
					if(CALIB_MEMORY_MAP.LUT_EN)
                    {
                        IBIAS_LUT_CONTROL_TX5_TX8(2); // Bias & mod LUT Control
                        IBIAS_LUT_CONTROL_TX5_TX8(3); // Bias & mod LUT Control
                    }
                    BIAS_TxPower_Monitor( 6 );
                    BIAS_TxPower_Monitor( 7 );
                    break;
                    
		case OP_SM:
					if(PowerON_AWEN_flag==1)
						DDMI_AW_Monitor();
					break;
		case TXLOLLOS:
					if(PowerON_AWEN_flag==1)
						CMIS40_TXLOS_LOL_AW();
			        break;
		case RXLOLLOS:
					//if(PowerON_AWEN_flag==1)
						CMIS40_RXLOS_LOL_AW();
			        break;
		case Other_DDMI:
					Get_Module_Power_Monitor();
					break;

		default:
				break;
	}
}

void Initialize_TRx_DDMI()
{
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH01_SM);
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH23_SM);
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH45_SM);
    QSFPDD_DDMI_StateMachine(TXB_TXP_CH67_SM);
    QSFPDD_DDMI_StateMachine(RXP_CH0_CH3);
    QSFPDD_DDMI_StateMachine(RXP_CH4_CH7);
}

void MSA_DDMI_Function()
{
	// DDMI Function update
	if( DDMI_DataCount > 50 )
	{
		// Module in Low Power State
		if(( Module_Status == MODULE_LOW_PWR )||( CALIB_MEMORY_MAP.DDMI_DISABLE == 1))
            QSFPDD_DDMI_ONLY_ADC_GET();
        // Module in Ready State
		else
		{
			// Status Machine Loop
			QSFPDD_DDMI_StateMachine( DDMI_Update_C );
			DDMI_Update_C++;
			//Status Machine flag
			if( DDMI_Update_C >= 11 )
			{
				DDMI_Update_C = 0;
				if(PowerON_AWEN_flag==0)
				{
					AW_DDMI_Count ++ ;
					if(AW_DDMI_Count>4)
						PowerON_AWEN_flag = 1;
				}
			}
		}
		DDMI_DataCount = 0;
	}
	DDMI_DataCount++;
}

void QSFPDD_DDMI_ONLY_ADC_GET()
{
    Temperature_Monitor(GET_GD_Temperature());
    VCC_Monitor( GET_ADC_Value_Data( P3V3_TX_Mon )*2 );
	// Temp and Vcc Alarm Warning Flag
	QSFPDD_Temperature_VCC_AW();
}