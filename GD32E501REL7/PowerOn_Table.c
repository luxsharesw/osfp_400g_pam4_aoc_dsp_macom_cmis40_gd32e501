#include "gd32e501.h"
#include "core_cm33.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "GD_FlahMap.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"

//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "LDD_Tx1_Tx4.h"
#include "LDD_Tx5_Tx8.h"
#include "TIA_Rx1_Rx4.h"
#include "TIA_Rx5_Rx8.h"

uint16_t MCU_FW_VERSION = 0x0113;

uint8_t QSFPDD_A0[272]={0};
uint8_t QSFPDD_P0[128]={0};
uint8_t QSFPDD_P1[128]={0};
uint8_t QSFPDD_P2[128]={0};
uint8_t QSFPDD_P3[128]={0};
uint8_t QSFPDD_P10[128]={0};
uint8_t QSFPDD_P11[128]={0};
uint8_t QSFPDD_P13[128]={0};
uint8_t QSFPDD_P14[128]={0};
uint8_t QSFPDD_P9F[128]={0};
uint8_t QSFPDD_PFF[144]={0};
uint8_t CTEL_Table[16];
uint8_t PAM4_Pre_Table[16];
uint8_t PAM4_AMP_Table[16];
uint8_t PAM4_Post_Table[16];
uint8_t NRZ_Pre_Table[16];
uint8_t NRZ_AMP_Table[16];
uint8_t NRZ_Post_Table[16];
uint8_t PW_LEVE1[4];

struct CALIB_MEMORY CALIB_MEMORY_MAP;
struct CALIB_MEMORY_1 CALIB_MEMORY_1_MAP;

//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//

void Init_Rx_Output_Values()
{
    // RX Pre-EM
    QSFPDD_P10[34] = CALIB_MEMORY_MAP.R12_Pre_Level ;
    QSFPDD_P10[35] = CALIB_MEMORY_MAP.R34_Pre_Level ;
    QSFPDD_P10[36] = CALIB_MEMORY_MAP.R56_Pre_Level ;
    QSFPDD_P10[37] = CALIB_MEMORY_MAP.R78_Pre_Level ;
    
    // RX Post-EM
    QSFPDD_P10[38] = CALIB_MEMORY_MAP.R12_Post_Level ;
    QSFPDD_P10[39] = CALIB_MEMORY_MAP.R34_Post_Level ;
    QSFPDD_P10[40] = CALIB_MEMORY_MAP.R56_Post_Level ;
    QSFPDD_P10[41] = CALIB_MEMORY_MAP.R78_Post_Level ;

    // Rx AMP
    QSFPDD_P10[42] = CALIB_MEMORY_MAP.R12_Swining_Level_2 ;
    QSFPDD_P10[43] = CALIB_MEMORY_MAP.R34_Swining_Level_2 ;
    QSFPDD_P10[44] = CALIB_MEMORY_MAP.R56_Swining_Level_2 ;
    QSFPDD_P10[45] = CALIB_MEMORY_MAP.R78_Swining_Level_2 ;
}

//-----------------------------------------------------------------------------------------------------//
// SRAM Inititlize
//-----------------------------------------------------------------------------------------------------//
void CMIS_TABLE_INIT()
{
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_A0 , &QSFPDD_A0[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P0 , &QSFPDD_A0[128] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P0 , &QSFPDD_P0[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P1 , &QSFPDD_P1[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P2 , &QSFPDD_P2[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_QSFPDD_P3 , &QSFPDD_P3[0] , 128 );
}
// TRx Device and DSP Table Init
void DEVICE_TABLE_INIT()
{
	uint8_t Temp_Buffer[128];
	// Tx LDD MALD38435B
	GDMCU_FMC_READ_FUNCTION( FS_VCSEL_P82  , &LDD_TX1_TX4_MEMORY_MAP.CHIP_ID     , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_VCSEL_P84  , &LDD_TX5_TX8_MEMORY_MAP.CHIP_ID     , 128 );
	// Rx TIA MATA38434
	GDMCU_FMC_READ_FUNCTION( FS_TIA_P8A    , &TIA_RX1_RX4_MEMORY_MAP.CHIP_ID , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_TIA_P8B    , &TIA_RX5_RX8_MEMORY_MAP.CHIP_ID , 128 );
	// DSP Line-side & System-side CH0 - CH7
	GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P86    , &Temp_Buffer[0] , 128 );
	memcpy(  &DSP_Line_Side_PHY0_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P87    , &Temp_Buffer[0] , 128 );
	memcpy(  &DSP_System_Side_PHY0_MEMORY_MAP , &Temp_Buffer[0] , 128 );
    GDMCU_FMC_READ_FUNCTION( FS_DSP_LS_P8C    , &Temp_Buffer[0] , 128 );
	memcpy(  &DSP_Line_Side_PHY1_MEMORY_MAP   , &Temp_Buffer[0] , 128 );
	GDMCU_FMC_READ_FUNCTION( FS_DSP_SS_P8E    , &Temp_Buffer[0] , 128 );
	memcpy(  &DSP_System_Side_PHY1_MEMORY_MAP , &Temp_Buffer[0] , 128 );
}
// Channel 1-4 Calibration Data and optional flag Page
void MCU_PAGE90h_INIT()
{
    GDMCU_FMC_READ_FUNCTION( FS_Cal0_P90          , &CALIB_MEMORY_MAP.VCC_SCALEM , 128 );
    // Use on MSA_DDMI_Function() to disable ddmi
    CALIB_MEMORY_MAP.DDMI_DISABLE = 0;
    // Use on Auto_Squelch_Tx_Output() to disable auto squelch
    CALIB_MEMORY_MAP.SKIP_DSP_Tx_Los_Check = 0;
    // Use on AppSel_Control_Feedback() to enable debug for I2C Script to run test
    CALIB_MEMORY_MAP.DEBUG_TEMP1 = 0;
}
// Optional control Page
void MCU_PAGE91h_INIT()
{
    // Not use now
	GDMCU_FMC_READ_FUNCTION(   FS_MSA_O_P91      , &CTEL_Table[0]   , 16 );
    // Get Level1 Password
    GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+48 ) , &PW_LEVE1[0]     , 4  );
    // Get PAM4 Rx Output Table
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+16 ) , &PAM4_Pre_Table[0]    , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+32 ) , &PAM4_AMP_Table[0]    , 16 );
	GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+64 ) , &PAM4_Post_Table[0]   , 16 );
    // Get NRZ Rx Output Table
    GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+80 )  , &NRZ_Pre_Table[0]   , 16 );
    GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+96 )  , &NRZ_AMP_Table[0]   , 16 );
    GDMCU_FMC_READ_FUNCTION( ( FS_MSA_O_P91+112 ) , &NRZ_Post_Table[0]  , 16 );
}
// Channel 5-8 Calibration Data and optional flag Page
void MCU_PAGE92h_INIT()
{
    GDMCU_FMC_READ_FUNCTION( FS_Cal1_P92          , &CALIB_MEMORY_1_MAP.VCC1_SCALEM , 128 );
}
void CMIS_LOWPAGE_INIT()
{
	// MSA TWI Maximum speed 1M
    QSFPDD_A0[2] = 0x04 ;
    // Module LowPwr State
	QSFPDD_A0[3] = 0x02 ;
    QSFPDD_A0[4] = 0x00 ;
    // Clear Interrupt Flags
    QSFPDD_A0[9] = 0x00 ;
    QSFPDD_A0[10] = 0x00 ;
    QSFPDD_A0[11] = 0x00 ;
    // Other DDMI
    QSFPDD_A0[18] = 0x00 ;
    QSFPDD_A0[19] = 0x00 ;
    QSFPDD_A0[20] = 0x00 ;
    QSFPDD_A0[21] = 0x00 ;
    QSFPDD_A0[22] = 0x00 ;
    QSFPDD_A0[23] = 0x00 ;
    QSFPDD_A0[24] = 0x00 ;
    QSFPDD_A0[25] = 0x00 ;
    // HW INIT and SW INIT Bit4 Force LowPwr = 0 , Bit6 LowPwr = 1
	QSFPDD_A0[26] = 0x40;
    // Clear Masks
    QSFPDD_A0[31] = 0x00 ;
    QSFPDD_A0[32] = 0x00 ;
    QSFPDD_A0[33] = 0x00 ;
    QSFPDD_A0[34] = 0x00 ;
    QSFPDD_A0[35] = 0x00 ;
    QSFPDD_A0[36] = 0x00 ;
    // CDB Block 1 Status
    QSFPDD_A0[37] = 0x00 ;
    QSFPDD_A0[38] = 0x00 ;
}

void CMIS_PAGE10h_INIT()
{
	memset(&QSFPDD_P10, 0x00, 128);
	// Page10 RW Control
    // Init Rx Output Disable Value
    Bef_Rx_output=QSFPDD_P10[10];
    // Tx Adaptive Input EQ Enable
    QSFPDD_P10[25] = 0xFF ;
	// Tx CDR ON
	QSFPDD_P10[32] = 0xFF ;
	// Rx CDR ON
	QSFPDD_P10[33] = 0xFF ;
    // Init Pre Post Main Control Value on Byte162-173
	//Init Rx Pre Post Amp Values
    Init_Rx_Output_Values();  
    // OSFP NVIDIA Default
    QSFPDD_P10[127] = 0xFF ;
}

void CMIS_PAGE11h_INIT()
{
	memset(&QSFPDD_P11, 0x00, 128);
    // Tx Adaptive Input EQ Enable
    QSFPDD_P11[86] = QSFPDD_P10[25] ;
    // Active Tx CDR Bypass
	QSFPDD_P11[93] = QSFPDD_P10[32] ;
	// Active Rx CDR Bypass
	QSFPDD_P11[94] = QSFPDD_P10[33] ;
    // Host Tx Eletrical Lane Mapping on Byte240-247
	QSFPDD_P11[112] = 0x00;
	QSFPDD_P11[113] = 0x00;
	QSFPDD_P11[114] = 0x00;
	QSFPDD_P11[115] = 0x00;
    QSFPDD_P11[116] = 0x00;
	QSFPDD_P11[117] = 0x00;
	QSFPDD_P11[118] = 0x00;
	QSFPDD_P11[119] = 0x00;
    // Host Rx Eletrical Lane Mapping on Byte248-255
	QSFPDD_P11[120] = 0x00;
	QSFPDD_P11[121] = 0x00;
	QSFPDD_P11[122] = 0x00;
	QSFPDD_P11[123] = 0x00;
    QSFPDD_P11[124] = 0x00;
	QSFPDD_P11[125] = 0x00;
	QSFPDD_P11[126] = 0x00;
	QSFPDD_P11[127] = 0x00;
}

void CMIS_PAGE13h_INIT()
{
	memset(&QSFPDD_P13, 0x00, 128);
	// Loopback Capabilities
	QSFPDD_P13[0]  = 0xFF ;          // 128 pre-lane and all
    // Diagnostics Measurement Capabilities
	QSFPDD_P13[1]  = 0x00 ;          // 129 BER Not Supported
    // Diagnostic Reporting Capabilities
	QSFPDD_P13[2]  = 0x20 ;          // 130 MediaSide Input SNR Measurement Supported
    // Pattern Generation and Checking Location
	QSFPDD_P13[3]  = 0x88 ;          // 131 Host Media side Pre-FEC PRBS generator
    // Generator
	QSFPDD_P13[4]  = 0xFF ;          // 132 Host Side Generator Support Pattern PRBS 31 23 15 13
	QSFPDD_P13[5]  = 0x03 ;          // 133 Host Side Generator Support Pattern PRBS 9
	QSFPDD_P13[6]  = 0xFF ;          // 134 Media Side Generator Support Pattern PRBS 31 23 15 13
	QSFPDD_P13[7]  = 0x13 ;          // 135 Media Side Generator Support Pattern PRBS 9 SSPRQ
    // Checker
	QSFPDD_P13[8]  = 0x00 ;          // 136 Host Side Checker Supports Pattern Not Supported
	QSFPDD_P13[9]  = 0x00 ;          // 137 Host Side Checker Supports Pattern Not Supported
	QSFPDD_P13[10] = 0x00 ;          // 138 Media Side Checker Supports Pattern Not Supported
	QSFPDD_P13[11] = 0x00 ;          // 139 Media Side Checker Supports Pattern Not Supported
    // Pattern Generator and Checker swap and invert Capabilities
	QSFPDD_P13[12] = 0x00 ;          // 140 Recovered clock for generator not supported
	QSFPDD_P13[13] = 0x00 ;          // 141 Host Side Checker Data not supported
	QSFPDD_P13[14] = 0x00 ;          // 142 Media Side Checker Data not supported
	QSFPDD_P13[15] = 0x00 ;          // 143 RSVD
}

void NVIDIA_DGX_H100_PAGE_INIT()
{
    // Page9F Address 0x88 Length42
    QSFPDD_P9F[8]=0x03;
    QSFPDD_P9F[9]=0x07;
    QSFPDD_P9F[10]=0x2F;
    QSFPDD_P9F[11]=0xAB;
    // First time read
    //QSFPDD_P9F[12]=0x07;
    // Second time read
    QSFPDD_P9F[12]=0x00;
    QSFPDD_P9F[13]=0x0A;
    QSFPDD_P9F[14]=0x44;
    QSFPDD_P9F[15]=0x45;
    
    QSFPDD_P9F[16]=0x56;
    QSFPDD_P9F[17]=0x20;
    QSFPDD_P9F[18]=0x5B;
    QSFPDD_P9F[19]=0x4E;
    QSFPDD_P9F[20]=0x53;
    QSFPDD_P9F[21]=0x5D;
    
    QSFPDD_P9F[46]=0x2F;
    QSFPDD_P9F[47]=0xAB;
    QSFPDD_P9F[48]=0x00;
    QSFPDD_P9F[49]=0x0A;
    // PageFF Address 0x80 Length128
    QSFPDD_PFF[0]=0x04;
    QSFPDD_PFF[1]=0x12;
    QSFPDD_PFF[2]=0x05;
    QSFPDD_PFF[3]=0x65;
    
    QSFPDD_PFF[112]=0xE8;
    QSFPDD_PFF[113]=0xFF;
    QSFPDD_PFF[114]=0x3F;
    QSFPDD_PFF[115]=0x60;
    QSFPDD_PFF[116]=0x01;
    QSFPDD_PFF[117]=0xFF;
    QSFPDD_PFF[118]=0x13;
    QSFPDD_PFF[119]=0xD0;
    
    QSFPDD_PFF[120]=0xF0;
    QSFPDD_PFF[121]=0xFF;
    QSFPDD_PFF[122]=0x00;
    QSFPDD_PFF[123]=0x00;
    QSFPDD_PFF[124]=0x01;
    QSFPDD_PFF[125]=0x6C;
    QSFPDD_PFF[126]=0x1B;
    QSFPDD_PFF[127]=0xBE;
}
void PowerON_Table_Init()
{
	CMIS_TABLE_INIT();
	DEVICE_TABLE_INIT();
    MCU_PAGE90h_INIT();
	MCU_PAGE91h_INIT();
    MCU_PAGE92h_INIT();
    // CMIS Page Data INIT
	CMIS_LOWPAGE_INIT();
    CMIS_PAGE10h_INIT();
	CMIS_PAGE11h_INIT();
	CMIS_PAGE13h_INIT();
    NVIDIA_DGX_H100_PAGE_INIT();
}