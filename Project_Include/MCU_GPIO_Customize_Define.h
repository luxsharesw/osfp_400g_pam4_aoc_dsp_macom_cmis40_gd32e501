//----------------------------------------------------//
// ADC Analog Define (Color Pink)
//----------------------------------------------------//
//----------------------------------------------------//
//ADC 2.5/4096 = 0.00061  6.1 ( 100uV)
//----------------------------------------------------//
// Analog define 
#define ADC_CONV_Value_100uV   6.1      
#define P1V8_TX_Mon     ADC_CHANNEL_0       //PA0	P1V8_TX_MON
#define P3V3_RX_Mon     ADC_CHANNEL_8       //PB0	P3V3_RX_MON
#define AVDD_0P75A      ADC_CHANNEL_9       //PB1	AVDD_0P75A
#define P3V3_TX_Mon     ADC_CHANNEL_4       //PB2	P3V3_TX_MON
#define RSSI_1_Mon      ADC_CHANNEL_7       //PB5	RSSI_1
#define AVDD_0P90       ADC_CHANNEL_10      //PC0	AVDD_0P90
#define AVDD_0P75B      ADC_CHANNEL_11      //PC1	AVDD_0P75B
#define RSSI_2_Mon      ADC_CHANNEL_13      //PC3	RSSI_2
#define GD_Temp_Sensor  ADC_CHANNEL_16

//----------------------------------------------------//
// MCU GPIO Digital Golden Finger Define (Color Yellow)
//----------------------------------------------------//
#define INT_Rstn_G_GPIOA    GPIO_PIN_1      // PA1 
#define LPWn_PRSn_G_GPIOA   GPIO_PIN_3      // PA3 
#define IntL_GPIOC          GPIO_PIN_9      // PC9  Digital OUTPUT_PP - IntL_G
#define ModselL_G_GPIOC     GPIO_PIN_15     // PC15 Digital INPUT_OP  - ModeselL_G
//----------------------------------------------------//
// MCU GPIO Digital Internal Control Define (Color Orange)
//----------------------------------------------------//
#define P3V3_DSP_EN_GPIOA   GPIO_PIN_11     // PA11 Digital OUTPUT_PP - P3V3_DSP_EN
#define P3V3_RX_EN_GPIOA    GPIO_PIN_12     // PA12 Digital OUTPUT_PP - P3V3_RX_EN
#define P3V3_TX_EN_GPIOA    GPIO_PIN_15     // PA15 Digital OUTPUT_PP - P3V3_TX_EN
#define P1V8_TX_EN_GPIOB    GPIO_PIN_9      // PB9  Digital OUTPUT_PP - P1V8_TX_EN
#define MODSELB_DSP_GPIOB   GPIO_PIN_12     // PB12 Digital OUTPUT_PP - MODSELB DSP
#define RESET_L_DSP_GPIOB   GPIO_PIN_13     // PB13 Digital OUTPUT_PP - RESET_L DSP
#define LPMODE_DSP_GPIOB    GPIO_PIN_14     // PB14 Digital OUTPUT_PP - LPMODE DSP
#define INTR_N_DSP_GPIOB    GPIO_PIN_15     // PB15 Digital INPUT_PP  - INTR_N DSP
#define OSC_EN_GPIOC        GPIO_PIN_6      // PC6  Digital OUTPUT_PP - OSC_EN (XO_EN)
//----------------------------------------------------//
// GPIO Digital Golden Finger MSA Define
//----------------------------------------------------//
#define IntL_G_Low()      gpio_bit_reset(GPIOC, IntL_GPIOC)
#define IntL_G_High()     gpio_bit_set(GPIOC, IntL_GPIOC)
//----------------------------------------------------//
// GPIO Digital Power Control Define
//----------------------------------------------------//
//TRx 3V3 1V8 Control
#define P3V3_DSP_EN_High()         gpio_bit_set(GPIOA, P3V3_DSP_EN_GPIOA)
#define P3V3_RX_EN_High()          gpio_bit_set(GPIOA, P3V3_RX_EN_GPIOA)
#define P3V3_TX_EN_High()          gpio_bit_set(GPIOA, P3V3_TX_EN_GPIOA)
#define P1V8_TX_EN_High()          gpio_bit_set(GPIOB, P1V8_TX_EN_GPIOB)
#define P3V3_DSP_EN_Low()          gpio_bit_reset(GPIOA, P3V3_DSP_EN_GPIOA)
#define P3V3_RX_EN_Low()           gpio_bit_reset(GPIOA, P3V3_RX_EN_GPIOA)
#define P3V3_TX_EN_Low()           gpio_bit_reset(GPIOA, P3V3_TX_EN_GPIOA)
#define P1V8_TX_EN_Low()           gpio_bit_reset(GPIOB, P1V8_TX_EN_GPIOB)
//DSP Control
#define OSC_EN_DSP_High()          gpio_bit_set(GPIOC, OSC_EN_GPIOC)            //High Active
#define RESET_L_DSP_High()         gpio_bit_set(GPIOB, RESET_L_DSP_GPIOB)       //DSP unReset , DSP Download SPI EEPROM Image
#define LPMODE_DSP_High()          gpio_bit_set(GPIOB, LPMODE_DSP_GPIOB)        //High Active to Low Power Mode
#define MODSELB_DSP_High()         gpio_bit_set(GPIOB, MODSELB_DSP_GPIOB)
#define OSC_EN_DSP_Low()           gpio_bit_reset(GPIOC, OSC_EN_GPIOC)         
#define RESET_L_DSP_Low()          gpio_bit_reset(GPIOB, RESET_L_DSP_GPIOB)
#define LPMODE_DSP_Low()           gpio_bit_reset(GPIOB, LPMODE_DSP_GPIOB)      //Low Active to High Power Mode
#define MODSELB_DSP_Low()          gpio_bit_reset(GPIOB, MODSELB_DSP_GPIOB)