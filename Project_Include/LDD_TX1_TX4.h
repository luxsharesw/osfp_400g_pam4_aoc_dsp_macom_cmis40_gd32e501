//------------------------------------------------//
//---------------- LDD define---------------------//
//------------------------------------------------//

#ifndef INC_LDD_TX1_TX4_H_
#define INC_LDD_TX1_TX4_H_

extern struct LDD_TX1_TX4_MEMORY   LDD_TX1_TX4_MEMORY_MAP;
//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
#define LDD_SlaveAddress    0x1C

void LDD_TX1_TX4_PowerOn_Reset();
void LDD_TX1_TX4_PowerOn_init();
void LDD_TX1_TX4_WRITE_P82();
void LDD_TX1_TX4_READ_P82();
void LDD_TX1_TX4_Tx_Disable(uint8_t Channel_Control);
void IBIAS_LUT_CONTROL_TX1_TX4(uint8_t CHSEL );
uint16_t LDD_TX1_TX4_BIAS(uint8_t Bias_Channel);
uint8_t LDD_TX1_TX4_LOS();

struct LDD_TX1_TX4_MEMORY
{
	// Read only
	uint8_t CHIP_ID ;                 // reg. address 00  R   SRAM Address 80
	uint8_t REVID ;                   // reg. address 01  R   SRAM Address 81
	uint8_t LOS_Status ;              // reg. address 17  R   SRAM Address 82
	uint8_t TX_Status ;               // reg. address 18  R   SRAM Address 83
	uint8_t Tx_Fault_State ;          // reg. address 1A  R   SRAM Address 84
	uint8_t ADC_out0_msbs ;           // reg. address 68  R   SRAM Address 85
	uint8_t ADC_out0_lsbs ;           // reg. address 69  R   SRAM Address 86
	uint8_t RSVD_0 ;                  //                  R   SRAM Address 87
	uint8_t RSVD_1 ;                  //                  R   SRAM Address 88
	uint8_t RSVD_2 ;                  //                  R   SRAM Address 89
	uint8_t RSVD_3 ;                  //                  R   SRAM Address 8A
	// Write Function
	uint8_t RESET ;                   // reg. address 02  RW  SRAM Address 8B
	uint8_t IO_Control ;              // reg. address 03  RW  SRAM Address 8C
	uint8_t IC_RSVD_04 ;              // reg. address 04  RW  SRAM Address 8D
	uint8_t I2C_ADR_MODE ;            // reg. address 05  RW  SRAM Address 8E
	uint8_t CH_MODE ;                 // reg. address 10  RW  SRAM Address 8F
	uint8_t LOS_MASK ;                // reg. address 16  RW  SRAM Address 90
	uint8_t LOS_ALARM ;               // reg. address 19  RW  SRAM Address 91
	uint8_t TX_F_A_MASK ;             // reg. address 1B  RW  SRAM Address 92
	uint8_t TX_F_Alarm ;              // reg. address 1C  RW  SRAM Address 93
	uint8_t Ignore_TXF ;              // reg. address 1D  RW  SRAM Address 94
	uint8_t IC_RSVD_1E ;              // reg. address 1E  RW  SRAM Address 95
	uint8_t LOS_THRS ;     			// reg. address 21  RW  SRAM Address 96
	uint8_t LOS_HYST ; 			 	// reg. address 22  RW  SRAM Address 97
	uint8_t Rvcsel_CH0 ;  			// reg. address 25  RW  SRAM Address 98
	uint8_t Rvcsel_CH1 ;              // reg. address 26  RW  SRAM Address 99
	uint8_t Rvcsel_CH2 ;              // reg. address 27  RW  SRAM Address 9A
	uint8_t Rvcsel_CH3 ;              // reg. address 28  RW  SRAM Address 9B
	uint8_t IC_RSVD_29 ;              // reg. address 29  RW  SRAM Address 9C
	uint8_t OUTPUT_MUTE ;             // reg. address 40  RW  SRAM Address 9D
	uint8_t TX_DISABLE ;              // reg. address 41  RW  SRAM Address 9E
	uint8_t IBias_CH0 ;               // reg. address 42  RW  SRAM Address 9F
	uint8_t IBias_CH1 ;               // reg. address 43  RW  SRAM Address A0
	uint8_t IBias_CH2 ;               // reg. address 44  RW  SRAM Address A1
	uint8_t IBias_CH3 ;               // reg. address 45  RW  SRAM Address A2
	uint8_t Imod_CH0 ;                // reg. address 46  RW  SRAM Address A3
	uint8_t Imod_CH1 ;                // reg. address 47  RW  SRAM Address A4
	uint8_t Imod_CH2 ;                // reg. address 48  RW  SRAM Address A5
	uint8_t Imod_CH3 ;                // reg. address 49  RW  SRAM Address A6
	uint8_t AC_Gain_CH0 ;             // reg. address 4A  RW  SRAM Address A7
	uint8_t AC_Gain_CH1 ;             // reg. address 4B  RW  SRAM Address A8
	uint8_t AC_Gain_CH2 ;             // reg. address 4C  RW  SRAM Address A9
	uint8_t AC_Gain_CH3 ;             // reg. address 4D  RW  SRAM Address AA
	uint8_t DC_Gain_CH0 ;             // reg. address 4F  RW  SRAM Address AB
	uint8_t DC_Gain_CH1 ;  		    // reg. address 50  RW  SRAM Address AC
	uint8_t DC_Gain_CH2 ; 			// reg. address 51  RW  SRAM Address AD
	uint8_t DC_Gain_CH3 ;  			// reg. address 52  RW  SRAM Address AE
	uint8_t IC_RSVD_5A ;              // reg. address 5A  RW  SRAM Address AF

	uint8_t IBurnIn ;                 // reg. address 5E  RW  SRAM Address B0
	uint8_t BurnIn_EN ;               // reg. address 5F  RW  SRAM Address B1
	uint8_t ADC_Config0 ;             // reg. address 60  RW  SRAM Address B2
	uint8_t ADC_Config2 ;             // reg. address 62  RW  SRAM Address B3
	uint8_t IC_RSVD_64 ;              // reg. address 64  RW  SRAM Address B4

	uint8_t Driver_ADC_CHSEL ;        // reg. address 91  RW  SRAM Address B5
	uint8_t Driver_ADC_Measmt ;       // reg. address 92  RW  SRAM Address B6

	uint8_t RSVD_Buffer[65] ;         //                  RW  SRAM Address B7 - F7
	// Direct_Control
	uint8_t Direct_AD;                // Direct_Control Address RW  SRAM Address 0xF8
	uint8_t Direct_Data[5];           // Direct_Control Data    RW  SRAM Address 0xF9 - 0xFD
	uint8_t Direct_RW;                // Direct_Control RW      RW  SRAM Address 0xFE
	uint8_t Direct_EN;                // Direct_Control RW      RW  SRAM Address 0xFF
};

#endif /* INC_MA38435_TX1_TX4_H_ */
