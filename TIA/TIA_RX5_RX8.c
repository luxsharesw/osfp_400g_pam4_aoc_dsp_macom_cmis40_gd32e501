/*
 * MA38434_RX5_RX8.c
 *
 *  Created on: 2019�~6��20��
 *      Author: Ivan_Lin
 */
#include "gd32e501.h"
#include "systick.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Master_I2C2_PC78.h"
#include "TIA_RX5_RX8.h"
#include "MCU_GPIO_Customize_Define.h"

struct TIA_RX5_RX8_MEMORY   TIA_RX5_RX8_MEMORY_MAP;

uint8_t TIA_RX5_RX8_LOS_Status = 0x00;
//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX5_RX8 I2C PAGE SEL ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX5_RX8_SET_PAGE( uint8_t PAGE )
{
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xFF , PAGE );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX5_RX8 I2C Write    ---------------------------------------------//
// 2019-11-10 0xC2 setting to 0x31 - Macom Hank Suggest
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX5_RX8_W_CH_SET(uint8_t Channel)
{
	switch( Channel )
	{
		case TIA_CH0 :
			 	 	// CDR PAGE 20 CH0
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_20 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x00 , TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH0          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x01 , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH0  );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH0     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH0 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x04 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH0 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x05 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH0 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0A , TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH0   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0C , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH0      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0D , TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH0   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x19 , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH0     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x1A , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH0      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC2 , TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH0        );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE2 , TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH0          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD2 , TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH0        );
					if( ( TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC0 , TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH0 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC1 , TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH0 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE0 , TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH0 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE1 , TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH0 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH0 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD0 , TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH0 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD1 , TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH0 );
					}

					break;
		case TIA_CH1 :
					// CDR PAGE 21 CH1
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_21 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x00 , TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH1          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x01 , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH1  );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH1     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH1 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x04 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH1 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x05 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH1 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0A , TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH1   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0C , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH1      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0D , TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH1   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x19 , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH1     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x1A , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH1      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC2 , TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH1        );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE2 , TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH1          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD2 , TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH1        );

					if( ( TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC0 , TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH1 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC1 , TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH1 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE0 , TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH1 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE1 , TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH1 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH1 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD0 , TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH1 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD1 , TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH1 );
					}

					break;
		case TIA_CH2 :
					// CDR PAGE 22 CH2
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_22 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x00 , TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH2          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x01 , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH2  );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH2     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH2 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x04 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH2 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x05 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH2 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0A , TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH2   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0C , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH2      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0D , TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH2   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x19 , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH2     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x1A , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH2      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC2 , TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH2        );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE2 , TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH2          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD2 , TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH2        );

					if( ( TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC0 , TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH2 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC1 , TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH2 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE0 , TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH2 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE1 , TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH2 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH2 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD0 , TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH2 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD1 , TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH2 );
					}

					break;
		case TIA_CH3 :
					// CDR PAGE 23 CH3
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_23 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x00 , TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH3          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x01 , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH3  );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH3     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH3 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x04 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH3 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x05 , TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH3 );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0A , TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH3   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0C , TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH3      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x0D , TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH3   );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x19 , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH3     );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x1A , TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH3      );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC2 , TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH3        );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE2 , TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH3          );
					Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD2 , TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH3        );

					if( ( TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC0 , TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH3 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC1 , TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH3 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE0 , TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH3 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE1 , TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH3 );
					}

					if( ( TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH3 & 0x01 ) == 0x00 )
					{
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD0 , TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH3 );
						Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD1 , TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH3 );
					}

					break;
		default :
				 break;
	}
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434_RX5_RX8 I2C READ     ---------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_RX5_RX8_READ_ONLY()
{
	// PAGE 00
	MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_0_00 );
	TIA_RX5_RX8_MEMORY_MAP.CHIP_ID          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x00 );
	TIA_RX5_RX8_MEMORY_MAP.REVID            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x01 );
	TIA_RX5_RX8_MEMORY_MAP.I2C_ADR_MODE     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x03 );
}

void MATA38434_RX5_RX8_R_CH_SET(uint8_t Channel)
{
	switch( Channel )
	{
		case TIA_CH0 :
			 	 	// CDR PAGE 20 CH0
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_20 );
					TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH0          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x00 );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH0  = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x01 );
					TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH0     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x02 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH0 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x03 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH0 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x04 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH0 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x05 );
					TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH0   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0A );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH0      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0C );
					TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH0   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0D );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH0     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH0      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x1A );
					TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH0            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC0 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH0            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC1 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH0        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC2 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH0           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE0 );
			 	 	TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH0           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE1 );
			 	 	TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH0          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE2 );
			 	 	TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH0            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD0 );
			 	 	TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH0            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD1 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH0        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD2 );
					break;
		case TIA_CH1 :
					// CDR PAGE 21 CH1
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_21 );
					TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH1          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x00 );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH1  = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x01 );
					TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH1     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x02 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH1 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x03 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH1 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x04 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH1 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x05 );
					TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH1   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0A );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH1      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0C );
					TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH1   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0D );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH1     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH1      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x1A );
					TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH1            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC0 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH1            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC1 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH1        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC2 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH1           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE0 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH1           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE1 );
					TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH1          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE2 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH1            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD0 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH1            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD1 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH1        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD2 );

					break;

		case TIA_CH2 :
					// CDR PAGE 22 CH2
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_22 );
					TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH2          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x00 );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH2  = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x01 );
					TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH2     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x02 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH2 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x03 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH2 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x04 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH2 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x05 );
					TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH2   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0A );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH2      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0C );
					TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH2   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0D );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH2     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH2      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x1A );
					TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH2            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC0 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH2            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC1 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH2        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC2 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH2           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE0 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH2           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE1 );
					TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH2          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE2 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH2            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD0 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH2            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD1 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH2        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD2 );
					break;

		case TIA_CH3 :
					// CDR PAGE 22 CH2
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_23 );
					TIA_RX5_RX8_MEMORY_MAP.OUTPUT_EQ_CH3          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x00 );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_REGULATOR_CH3  = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x01 );
					TIA_RX5_RX8_MEMORY_MAP.REFERNCE_SWING_CH3     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x02 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA0_Filter_CH3 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x03 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA1_Filter_CH3 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x04 );
					TIA_RX5_RX8_MEMORY_MAP.Manual_VGA2_Filter_CH3 = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x05 );
					TIA_RX5_RX8_MEMORY_MAP.Tune_REF_SEL_LOW_CH3   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0A );
					TIA_RX5_RX8_MEMORY_MAP.TIA_VGA_SWEEP_CH3      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0C );
					TIA_RX5_RX8_MEMORY_MAP.TIA_Filter_Swing_CH3   = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x0D );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESH_HST_CH3     = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 );
					TIA_RX5_RX8_MEMORY_MAP.LOS_THRESHOLD_CH3      = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x1A );
					TIA_RX5_RX8_MEMORY_MAP.AGC_LSB_CH3            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC0 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_MSB_CH3            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC1 );
					TIA_RX5_RX8_MEMORY_MAP.AGC_AD_MODE_CH3        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xC2 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_LSB_CH3           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE0 );
					TIA_RX5_RX8_MEMORY_MAP.Bias_MSB_CH3           = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE1 );
					TIA_RX5_RX8_MEMORY_MAP.B_AD_MODE_CH3          = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xE2 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_LSB_CH3            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD0 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_MSB_CH3            = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD1 );
					TIA_RX5_RX8_MEMORY_MAP.DCD_AD_MODE_CH3        = Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0xD2 );

					break;
		default :
				 break;
	}
}

void TIA_RX5_RX8_R_LOS(uint8_t Channel)
{
    switch( Channel )
	{
		case TIA_CH0 :
			 	 	// CDR PAGE 20 CH0
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_20 );
                    if(Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 )&0x10)
                        TIA_RX5_RX8_LOS_Status |= 0x08;
                    else
                        TIA_RX5_RX8_LOS_Status &= ~0x08;

					break;
		case TIA_CH1 :
					// CDR PAGE 21 CH1
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_21 );
                    if(Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 )&0x10)
                        TIA_RX5_RX8_LOS_Status |= 0x04;
                    else
                        TIA_RX5_RX8_LOS_Status &= ~0x04;

					break;

		case TIA_CH2 :
					// CDR PAGE 22 CH2
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_22 );
                    if(Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 )&0x10)
                        TIA_RX5_RX8_LOS_Status |= 0x02;
                    else
                        TIA_RX5_RX8_LOS_Status &= ~0x02;;
                    
					break;

		case TIA_CH3 :
					// CDR PAGE 22 CH2
					MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_23 );
                    if(Master_I2C2_ByteREAD_PC78( TIA_SlaveAddress , 0x19 )&0x10)
                        TIA_RX5_RX8_LOS_Status |= 0x01;
                    else
                        TIA_RX5_RX8_LOS_Status &= ~0x01;

					break;
		default :
				 break;
	}
}

void MATA38434_RX5_RX8_W_GLOBAL_SET()
{
	MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.I2C_ADR_MODE );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ MATA38434 Macom TIA Suggest reg. Write -------------------------------------//
// 2019 - 10 - 01 Ivan modify
//-----------------------------------------------------------------------------------------------------------------//
void MATA38434_MACOM_Suggest_R58_1F_SETTING()
{
	MATA38434_RX5_RX8_SET_PAGE( 0x1F );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xF8 , TIA_RX5_RX8_MEMORY_MAP.AGC_Sequence_normal );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xF0 , TIA_RX5_RX8_MEMORY_MAP.DAC67_SET_normal );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x3A , TIA_RX5_RX8_MEMORY_MAP.DAC6 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x3B , TIA_RX5_RX8_MEMORY_MAP.DAC7 );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xF1 , TIA_RX5_RX8_MEMORY_MAP.DAC1415_SET_normal );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x42 , TIA_RX5_RX8_MEMORY_MAP.DAC14 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x43 , TIA_RX5_RX8_MEMORY_MAP.DAC15 );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xF4 , TIA_RX5_RX8_MEMORY_MAP.DAC3435_SET_normal );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x57 , TIA_RX5_RX8_MEMORY_MAP.DAC34 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x56 , TIA_RX5_RX8_MEMORY_MAP.DAC35 );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC5 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_C5 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC6 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_C6 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xC7 , TIA_RX5_RX8_MEMORY_MAP.SET_Average_C7 );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD5 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_D5 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD6 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_D6 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xD7 , TIA_RX5_RX8_MEMORY_MAP.SET_Average_D7 );

	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE5 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_E5 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE6 , TIA_RX5_RX8_MEMORY_MAP.Control_TC_E6 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0xE7 , TIA_RX5_RX8_MEMORY_MAP.SET_Average_E7 );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ TIA_TX1_TX4 I2C Write-------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

void TIA_RX5_RX8_Control_WRITE_ALL()
{
	MATA38434_RX5_RX8_W_GLOBAL_SET();
	MATA38434_RX5_RX8_W_CH_SET( TIA_CH0 );
	MATA38434_RX5_RX8_W_CH_SET( TIA_CH1 );
	MATA38434_RX5_RX8_W_CH_SET( TIA_CH2 );
	MATA38434_RX5_RX8_W_CH_SET( TIA_CH3 );
	MATA38434_MACOM_Suggest_R58_1F_SETTING();
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------ TIA_RX5_RX8 I2C READ--------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

void TIA_RX5_RX8_Control_READ_ALL()
{
	MATA38434_RX5_RX8_READ_ONLY();
	MATA38434_RX5_RX8_R_CH_SET( TIA_CH0 );
	MATA38434_RX5_RX8_R_CH_SET( TIA_CH1 );
	MATA38434_RX5_RX8_R_CH_SET( TIA_CH2 );
	MATA38434_RX5_RX8_R_CH_SET( TIA_CH3 );
}

void TIA_RX5_RX8_READ_LOS()
{
    MATA38434_RX5_RX8_READ_ONLY();
    TIA_RX5_RX8_R_LOS( TIA_CH0 );
	TIA_RX5_RX8_R_LOS( TIA_CH1 );
	TIA_RX5_RX8_R_LOS( TIA_CH2 );
	TIA_RX5_RX8_R_LOS( TIA_CH3 );
}

//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//
void TIA_RX5_RX8_PowerOn_Reset()
{
	MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , 0xAA );
	delay_1ms(1);
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x02 , 0x00 );
	delay_1ms(1);
}

void TIA_RX5_RX8_WRITE_P8B()
{   
	TIA_RX5_RX8_Control_WRITE_ALL();
}

void TIA_RX5_RX8_READ_P8B()
{
    TIA_RX5_RX8_Control_READ_ALL();
}

uint16_t TIA_RX5_RX8_RSSI(uint8_t RSSI_CH)
{
	uint16_t RSSI_Value;
	uint8_t TEMP ;

	TEMP = ( RSSI_CH << 5 ) & 0x60 ;
	TIA_RX5_RX8_MEMORY_MAP.I2C_ADR_MODE = TEMP ;
	MATA38434_RX5_RX8_SET_PAGE( CDR_PAGE_0_00 );
	Master_I2C2_ByteWrite_PC78( TIA_SlaveAddress , 0x03 , TIA_RX5_RX8_MEMORY_MAP.I2C_ADR_MODE );

	RSSI_Value = GET_ADC_Value_Data( RSSI_2_Mon );

	return RSSI_Value;
}

void TIA_RX5_RX8_PowerOn_init()
{
    TIA_RX5_RX8_WRITE_P8B(); 
}

uint8_t GET_TIA_LOS_RX5_RX8()
{
    uint8_t LOS_Status = 0x0000;
    
    TIA_RX5_RX8_READ_LOS();
    LOS_Status=TIA_RX5_RX8_LOS_Status;
    
    return LOS_Status;
}
