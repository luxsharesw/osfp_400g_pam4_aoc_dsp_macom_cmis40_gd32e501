#include "gd32e501.h"
#include "core_cm33.h"
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "GD_FlahMap.h"
#include "string.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
#include "DSP_System_Side.h"
//------------------------------------------------------------------------------------------//
// Page13h Flags
//------------------------------------------------------------------------------------------//
uint8_t P13_B144_Bef = 0;
uint8_t P13_B152_Bef = 0;
uint8_t P13_B160_Bef = 0;
uint8_t P13_B168_Bef = 0;
uint8_t P13_B180_Bef = 0;
uint8_t P13_B181_Bef = 0;
uint8_t P13_B182_Bef = 0;
uint8_t P13_B183_Bef = 0;
uint8_t P13_B155_Bef = 0;
uint8_t P13_B147_Bef = 0;
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
uint8_t Get_SystemSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_SystemSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_SystemSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_SystemSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_SystemSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_SystemSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_SystemSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_SystemSide;
    }
    
    return PRBS_Pattern;
}

uint8_t Get_LineSide_PRBS_Pattern(uint8_t Data)
{
    uint8_t PRBS_Pattern = 0x00;
    
    if(Signal_Status==PAM4_to_PAM4)
    {
        if(Data==0x00)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x02)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x04)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x06)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x08)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0A)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    else
    {
        if(Data==0x01)
            PRBS_Pattern=PRBS31Q_LineSide;
        else if(Data==0x03)
            PRBS_Pattern=PRBS23Q_LineSide;
        else if(Data==0x05)
            PRBS_Pattern=PRBS15Q_LineSide;
        else if(Data==0x07)
            PRBS_Pattern=PRBS13Q_LineSide;
        else if(Data==0x09)
            PRBS_Pattern=PRBS9Q_LineSide;
        else if(Data==0x0B)
            PRBS_Pattern=PRBS7Q_LineSide;
        else if(Data==0x0C)
            PRBS_Pattern=SSPRQ_LineSide;
    }
    
    return PRBS_Pattern;
}
//-----------------------------------------------------------------------------------------------------//
// Page13h Controls                                                                                    //
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Host Side Pattern Generator Controls Byte144
//-----------------------------------------------------------------------------------------------------//
void Host_Side_Pattern_Generator_Control()
{
    //Host Side Generator Enable
    if(P13_B144_Bef!=QSFPDD_P13[16])
    {
		//Lane0-Lane3
        if(QSFPDD_P13[16] & 0x01)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],0)), 0 , 0);
        
        if(QSFPDD_P13[16] & 0x02)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[20],1)), 1 , 0);
        
        if(QSFPDD_P13[16] & 0x04)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],2)), 2 , 0);
        
        if(QSFPDD_P13[16] & 0x08)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[21],3)), 3 , 0);

		//Lane4-Lane7
        if(QSFPDD_P13[16] & 0x10)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],4)), 4 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],4)), 4 , 0);
        
        if(QSFPDD_P13[16] & 0x20)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],5)), 5 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[22],5)), 5 , 0);
        
        if(QSFPDD_P13[16] & 0x40)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],6)), 6 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],6)), 6 , 0);
        
        if(QSFPDD_P13[16] & 0x80)
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],7)), 7 , 1);
        else
            DSP_System_Side_PRBS_SET(Get_SystemSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[23],7)), 7 , 0);

        // unsquelch system side output
        Rx_output_flag=1;
        Rx_Output_Disable(~QSFPDD_P13[16]);
        // Recheck AutoSquelchRx
        if(QSFPDD_P13[16]==0x00)
            Rx_Los_Enable_Flag=1;
        P13_B144_Bef = QSFPDD_P13[16];
    }
}
//-----------------------------------------------------------------------------------------------------//
// Media Side Pattern Generator Controls Byte152
//-----------------------------------------------------------------------------------------------------//
void Media_Side_Pattern_Generator_Control()
{
    //Media Side Generator Enable
    if(P13_B152_Bef!=QSFPDD_P13[24])
    {
		//Lane0-Lane3
        if(QSFPDD_P13[24] & 0x01)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],0)), 0 , 0);
        
        if(QSFPDD_P13[24] & 0x02)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[28],1)), 1 , 0);
        
        if(QSFPDD_P13[24] & 0x04)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],2)), 2 , 0);
        
        if(QSFPDD_P13[24] & 0x08)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[29],3)), 3 , 0);
		
        //Lane4-Lane7
        if(QSFPDD_P13[24] & 0x10)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],4)), 4 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],4)), 4 , 0);
        
        if(QSFPDD_P13[24] & 0x20)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],5)), 5 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[30],5)), 5 , 0);
        
        if(QSFPDD_P13[24] & 0x40)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],6)), 6 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],6)), 6 , 0);
        
        if(QSFPDD_P13[24] & 0x80)
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],7)), 7 , 1);
        else
            DSP_Line_Side_PRBS_SET(Get_LineSide_PRBS_Pattern(Get_CH_Data (QSFPDD_P13[31],7)), 7 , 0);
        // Enable or Disable Tx output
        for(uint8_t Lane=0;Lane<8;Lane++)
        {
            if(QSFPDD_P13[24]&(0x01<<Lane))
            {
                Tx_Disable(Lane,0);
            }
                
            else
            {
                Tx_Disable(Lane,1);
            }
        }
        // Recheck AutoSquelchTx
        if(QSFPDD_P13[24]==0x00)
            Tx_Los_Enable_Flag=1;
        P13_B152_Bef = QSFPDD_P13[24];
    }
}

//-----------------------------------------------------------------------------------------------------//
// Pattern Checker Controls Page13h Byte129 , Byte177
//-----------------------------------------------------------------------------------------------------//
// Page13h Byte129
uint8_t Get_Per_Lane_Gating_Timer()
{
    uint8_t Result=0;
    // Page13h Byte129 0b Two timers for Host PRBS Checker and Media PRBS Checkers
    //                 1b Per lane gating timer supported.
    if(QSFPDD_P13[1]&0x08)
        Result=1;
    
    return Result;
}
// Page13h Byte177
uint8_t Get_Reset_All_Lanes()
{
    uint8_t Result=Individual_lane;
    // Bit7
    if(QSFPDD_P13[49]&0x80)
        Result=All_lanes_banks;
    
    return Result;
}

uint8_t Get_Reset_Error_Information()
{
    uint8_t Result=0;
    // Page13h Byte129 bit3 Per Lane Gating Timer is 0b , not support Per lane gating timer
    // if Page13h Byte129 bit3 bit0 , this bit 1 keep gate timers reset
    //                                this bit 0 will start the single gating timer
    // Bit5
    // bit 1b Error Information Selector 01h to 5h are frozen
    // bit 0b Error Information Selector 01h to 5h will be reset to 0
    // bit 1b if implemented 11h-15h will be updated with the Error Information.
    // bit 0b if implemented 11h-15h do nothing
    if(QSFPDD_P13[49]&0x20)
        Result=1;
    
    return Result;
}

uint8_t Get_Auto_Restart_Gate_Time()
{
    uint8_t Result=0;
    
    // 0b The module will update the latched error counters and stop error detection
    // 1b host muse read latched error count then it will get new counts
    if(QSFPDD_P13[49]&0x10)
        Result=1;
    
    return Result;
}

uint8_t Get_Gate_Time()
{
    uint8_t Result=0;
    
    if((QSFPDD_P13[49]&0x0E)==Not_gated)
        Result=Not_gated;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_5_sec)
        Result=gate_time_5_sec;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_10_sec)
        Result=gate_time_10_sec;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_30_sec)
        Result=gate_time_30_sec;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_60_sec)
        Result=gate_time_60_sec;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_120_sec)
        Result=gate_time_120_sec;
    else if((QSFPDD_P13[49]&0x0E)==gate_time_300_sec)
        Result=gate_time_300_sec;
    
    return Result;
}

uint8_t Get_Host_Side_Checker_Pattern_Select(uint8_t Lane)
{
    uint8_t pattern_select;
    if(Lane==0)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[36]&0x0F);
    }
    else if(Lane==1)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[36]&0xF0)>>4);
    }
    else if(Lane==2)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[37]&0x0F);
    }
    else if(Lane==3)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[37]&0xF0)>>4);
    }
    else if(Lane==4)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[38]&0x0F);
    }
    else if(Lane==5)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[38]&0xF0)>>4);
    }
    else if(Lane==6)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[39]&0x0F);
    }
    else if(Lane==7)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[39]&0xF0)>>4);
    }
    return pattern_select;
}

uint8_t Get_Media_Side_Checker_Pattern_Select(uint8_t Lane)
{
    uint8_t pattern_select;
    if(Lane==0)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[44]&0x0F);
    }
    else if(Lane==1)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[44]&0xF0)>>4);
    }
    else if(Lane==2)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[45]&0x0F);
    }
    else if(Lane==3)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[45]&0xF0)>>4);
    }
    else if(Lane==4)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[46]&0x0F);
    }
    else if(Lane==5)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[46]&0xF0)>>4);
    }
    else if(Lane==6)
    {
        // Bits3-0
        pattern_select=(QSFPDD_P13[47]&0x0F);
    }
    else if(Lane==7)
    {
        // Bits7-4
        pattern_select=((QSFPDD_P13[47]&0xF0)>>4);
    }
    return pattern_select;
}

//-----------------------------------------------------------------------------------------------------//
// D.2.2 Not Gated (Continuous) Error Counters, Individual Lanes,
// Host Selected Mode of Operation :
// a. Page 13h, byte 177, bits 3-1 = 0 (not gated)
// b. Page 13h, byte 177, bit 5 = 0 (do not hold checkers in reset)
// c. Page 13h, byte 177, bit 7 = 0 (reset error counts per lane)
// d. Page 13h, byte 129, bit 4 = 0
// �E  Page 13h, byte 177, bit 0 = X. (disable)
// D.2.3 Not Gated (Continuous) Error Counters, Individual Lanes, Reset Error Counter
// Configuration assumptions:
// a. Page 13h, byte 177, bits 3-1 = 0 (not gated)
// e. Page 13h, byte 177, bit 5 = 0 (do not hold checkers in reset)
// f. Page 13h, byte 177, bit 7 = 0 (reset error counts per lane)
// g. Page 13h, byte 129, bit 4 = 1 (polling enabled)
// �E  Page 13h, byte 177, bit 0 = 0. (poll every 1 sec.)
// D.2.4 Not Gated (Continuous) Error Counters, All Lanes, all banks
// Configuration assumptions:
// a. Page 13h, byte 177, bits 3-1 = 0 (not gated)
// b. Page 13h, byte 177, bit 5 = 0 (do not hold checkers in reset)13h
// c. Page 13h, byte 177, bit 7 = 1 (reset error counts on all banks all enabled lane)13h
// d. Page 13h, byte 129, bit 4 = 0 (polling enabled)
// �E  Page 13h, byte 177, bit 0 = 1. (poll every 5 sec.)13h
//-----------------------------------------------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------//
// Host Side Pattern Checker Controls Byte160
// 1.Bank and page select 13h
// 2.Byte177 for desired mode of operation.
// 3.Byte161-167 desired pattern checker configuration and lane pattern.
// 4.Byte176-179 with the desired control options.
// 5.Byte160 to enable the pattern checker on the selected lanes.
//-----------------------------------------------------------------------------------------------------//
void Host_Side_Pattern_Checker_Enable()
{
    uint16_t Pattern;
    // Page13h Byte160 host side chekcer enable
    if(P13_B160_Bef!=QSFPDD_P13[32])
    {
        // Clear PRBS Error Count
        DSP_System_Side_PRBS_Clear(All_CH);
        // Enable PRBS Checker
        for(uint8_t Lane=0;Lane<8;Lane++)
        {
            Pattern=DSP_System_Side_Get_Pattern_Type(Get_Host_Side_Checker_Pattern_Select(Lane));
            if(Pattern==Unknown)
            {
                DSP_System_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_DIS);
            }
            else
            {
                if(QSFPDD_P13[32]&(0x01<<Lane))
                    DSP_System_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_EN);
                else
                    DSP_System_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_DIS);
            }
        }
        P13_B160_Bef=QSFPDD_P13[32];
    }
}
//-----------------------------------------------------------------------------------------------------//
// Media Side Pattern Checker Controls Byte168
// 1.Bank and page select 13h
// 2.Byte177 for desired mode of operation.
// 3.Byte169-175 desired pattern checker configuration and lane pattern.
// 4.Byte176-179 with the desired control options.
// 5.Byte168 to enable the pattern checker on the selected lanes.
//-----------------------------------------------------------------------------------------------------//
void Media_Side_Pattern_Checker_Enable()
{
    uint16_t Pattern;
    // Page13h Byte160 host side chekcer enable
    if(P13_B168_Bef!=QSFPDD_P13[40])
    {
        // Clear PRBS Error Count
        DSP_Line_Side_PRBS_Clear(All_CH);
        // Enable PRBS Checker
        for(uint8_t Lane=0;Lane<8;Lane++)
        {
            Pattern=DSP_Line_Side_Get_Pattern_Type(Get_Media_Side_Checker_Pattern_Select(Lane));
            if(Pattern==Unknown)
            {
                DSP_Line_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_DIS);
            }
            else
            {
                if(QSFPDD_P13[40]&(0x01<<Lane))
                    DSP_Line_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_EN);
                else
                    DSP_Line_Side_PRBS_Checker_Enable(Pattern , Lane ,Function_DIS);
            }
        }
        P13_B168_Bef=QSFPDD_P13[40];
    }
}
//-----------------------------------------------------------------------------------------------------//
// Loopback Controls - Media Byte180-181
//-----------------------------------------------------------------------------------------------------//
void Media_Side_LoopBack_Control()
{
	// Page13h 180 / 52 Media Output ( Line Side Digital Loopback )
	// Page13h 181 / 53 Media Input  ( Line Side Remote Loopback )
	//---------------------------------------------------------------------------//
	// Media Output  ( Line Side Digital Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B180_Bef!=QSFPDD_P13[52])
	{
        //Lane0-Lane3
		if(QSFPDD_P13[52] & 0x01)
            DSP_Line_Side_Digital_Loopback_SET(0,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(0,0);
        
		if(QSFPDD_P13[52] & 0x02)
            DSP_Line_Side_Digital_Loopback_SET(1,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(1,0);
        
		if(QSFPDD_P13[52] & 0x04)
            DSP_Line_Side_Digital_Loopback_SET(2,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(2,0);
        
		if(QSFPDD_P13[52] & 0x08)
            DSP_Line_Side_Digital_Loopback_SET(3,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[52] & 0x10)
            DSP_Line_Side_Digital_Loopback_SET(4,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(4,0);
        
		if(QSFPDD_P13[52] & 0x20)
            DSP_Line_Side_Digital_Loopback_SET(5,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(5,0);
        
		if(QSFPDD_P13[52] & 0x40)
            DSP_Line_Side_Digital_Loopback_SET(6,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(6,0);
        
		if(QSFPDD_P13[52] & 0x80)
            DSP_Line_Side_Digital_Loopback_SET(7,1);
        else
            DSP_Line_Side_Digital_Loopback_SET(7,0);
        P13_B180_Bef = QSFPDD_P13[52];
    }
	//---------------------------------------------------------------------------//
	// Media Input  ( Line Side Remote Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B181_Bef!=QSFPDD_P13[53])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[53] & 0x01)
            DSP_Line_Side_Remote_Loopback_SET(0,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(0,0);
        
		if(QSFPDD_P13[53] & 0x02)
            DSP_Line_Side_Remote_Loopback_SET(1,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(1,0);
        
		if(QSFPDD_P13[53] & 0x04)
            DSP_Line_Side_Remote_Loopback_SET(2,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(2,0);
        
		if(QSFPDD_P13[53] & 0x08)
            DSP_Line_Side_Remote_Loopback_SET(3,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[53] & 0x10)
            DSP_Line_Side_Remote_Loopback_SET(4,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(4,0);
        
		if(QSFPDD_P13[53] & 0x20)
            DSP_Line_Side_Remote_Loopback_SET(5,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(5,0);
        
		if(QSFPDD_P13[53] & 0x40)
            DSP_Line_Side_Remote_Loopback_SET(6,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(6,0);
        
		if(QSFPDD_P13[53] & 0x80)
            DSP_Line_Side_Remote_Loopback_SET(7,1);
        else
            DSP_Line_Side_Remote_Loopback_SET(7,0);
        P13_B181_Bef = QSFPDD_P13[53];
	}
}

//-----------------------------------------------------------------------------------------------------//
// Loopback Controls - Host Byte182-183
//-----------------------------------------------------------------------------------------------------//
void Host_Side_LoopBack_Control()
{
	// Page13h 182 / 54 Host Output ( System Side Digital loopback )
	// Page13h 183 / 55 Host Input  ( System Side Remote loopback )
	//---------------------------------------------------------------------------//
	// Host Output ( System Side Digital Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B182_Bef!=QSFPDD_P13[54])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[54] & 0x01)
            DSP_System_Side_Digital_Loopback_SET(0,1);
        else
            DSP_System_Side_Digital_Loopback_SET(0,0);
        
		if(QSFPDD_P13[54] & 0x02)
            DSP_System_Side_Digital_Loopback_SET(1,1);
        else
            DSP_System_Side_Digital_Loopback_SET(1,0);
        
		if(QSFPDD_P13[54] & 0x04)
            DSP_System_Side_Digital_Loopback_SET(2,1);
        else
            DSP_System_Side_Digital_Loopback_SET(2,0);
        
		if(QSFPDD_P13[54] & 0x08)
            DSP_System_Side_Digital_Loopback_SET(3,1);
        else
            DSP_System_Side_Digital_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[54] & 0x10)
            DSP_System_Side_Digital_Loopback_SET(4,1);
        else
            DSP_System_Side_Digital_Loopback_SET(4,0);
        
		if(QSFPDD_P13[54] & 0x20)
            DSP_System_Side_Digital_Loopback_SET(5,1);
        else
            DSP_System_Side_Digital_Loopback_SET(5,0);
        
		if(QSFPDD_P13[54] & 0x40)
            DSP_System_Side_Digital_Loopback_SET(6,1);
        else
            DSP_System_Side_Digital_Loopback_SET(6,0);
        
		if(QSFPDD_P13[54] & 0x80)
            DSP_System_Side_Digital_Loopback_SET(7,1);
        else
            DSP_System_Side_Digital_Loopback_SET(7,0);
        P13_B182_Bef = QSFPDD_P13[54];
	}
	//---------------------------------------------------------------------------//
	// Host Input ( System Side Remote Loopback )
	//---------------------------------------------------------------------------//
	if(P13_B183_Bef!=QSFPDD_P13[55])
    {
        //Lane0-Lane3
		if(QSFPDD_P13[55] & 0x01)
            DSP_System_Side_Remote_Loopback_SET(0,1);
        else
            DSP_System_Side_Remote_Loopback_SET(0,0);
        
		if(QSFPDD_P13[55] & 0x02)
            DSP_System_Side_Remote_Loopback_SET(1,1);
        else
            DSP_System_Side_Remote_Loopback_SET(1,0);
        
		if(QSFPDD_P13[55] & 0x04)
            DSP_System_Side_Remote_Loopback_SET(2,1);
        else
            DSP_System_Side_Remote_Loopback_SET(2,0);
        
		if(QSFPDD_P13[55] & 0x08)
            DSP_System_Side_Remote_Loopback_SET(3,1);
        else
            DSP_System_Side_Remote_Loopback_SET(3,0);
        
        //Lane4-Lane7
		if(QSFPDD_P13[55] & 0x10)
            DSP_System_Side_Remote_Loopback_SET(4,1);
        else
            DSP_System_Side_Remote_Loopback_SET(4,0);
        
		if(QSFPDD_P13[55] & 0x20)
            DSP_System_Side_Remote_Loopback_SET(5,1);
        else
            DSP_System_Side_Remote_Loopback_SET(5,0);
        
		if(QSFPDD_P13[55] & 0x40)
            DSP_System_Side_Remote_Loopback_SET(6,1);
        else
            DSP_System_Side_Remote_Loopback_SET(6,0);
        
		if(QSFPDD_P13[55] & 0x80)
            DSP_System_Side_Remote_Loopback_SET(7,1);
        else
            DSP_System_Side_Remote_Loopback_SET(7,0);
        P13_B183_Bef = QSFPDD_P13[55];
	}
}

void CMIS_PAGE13h_Diagnostics_Function()
{
	Host_Side_Pattern_Generator_Control();
	Media_Side_Pattern_Generator_Control();
	Media_Side_LoopBack_Control();
	Host_Side_LoopBack_Control();
    Host_Side_Pattern_Checker_Enable();
    Media_Side_Pattern_Checker_Enable();
}
