//------------------------------------------------------------------------------------------//
// define
//------------------------------------------------------------------------------------------//
//------------------------------------------------------------------------------------------//
// Function
//------------------------------------------------------------------------------------------//
void Response_Diagnostics_Data(uint8_t Selector);
void Reply_Process_Status();
//------------------------------------------------------------------------------------------//
// Flags
//------------------------------------------------------------------------------------------//
extern uint8_t Diagnostics_Selector_Flag;
extern uint8_t L_Host_Checker_LOL;
extern uint8_t L_Media_Checker_LOL;
//------------------------------------------------------------------------------------------//
// typedef struct
//------------------------------------------------------------------------------------------//
typedef struct both_side_ber
{
    float host_ber[8];
    float media_ber[8];
}sel_01_t;

typedef struct host_side_lane0_lane3
{
    uint64_t host_error_count_lane03[4];
    uint64_t host_total_bits_count_lane03[4];
}sel_02_t;

typedef struct host_side_lane4_lane7
{
    uint64_t host_error_count_lane47[4];
    uint64_t host_total_bits_count_lane47[4];
}sel_03_t;

typedef struct media_side_lane0_lane3
{
    uint64_t media_error_count_lane03[4];
    uint64_t media_total_bits_count_lane03[4];
}sel_04_t;

typedef struct media_side_lane4_lane7
{
    uint64_t media_error_count_lane47[4];
    uint64_t media_total_bits_count_lane47[4];
}sel_05_t;

typedef struct both_data_snr
{
    uint16_t host_snr[8];
    uint16_t media_snr[8];
}sel_06_t;
//------------------------------------------------------------------------------------------//
//  struct
//------------------------------------------------------------------------------------------//
struct diag_data
{
    sel_01_t sel_01;
    sel_02_t sel_02;
    sel_03_t sel_03;
    sel_04_t sel_04;
    sel_05_t sel_05;
    sel_06_t sel_06;
};
//----------------------------------------------//
// Functions
//----------------------------------------------//
void CMIS_PAGE14h_Diagnostics_Function();
