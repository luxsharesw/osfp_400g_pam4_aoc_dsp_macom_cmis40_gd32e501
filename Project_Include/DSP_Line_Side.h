/*
 * BRCM_DSP58281_Optional.h
 *
 *  Created on: 2019�~8��28��
 *      Author: Ivan_Lin
 */
#ifndef INC_DSP_LINE_SIDE_H_
#define INC_DSP_LINE_SIDE_H_
//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
void Line_Side_ALL_CH1_CH4_Control_P86();
void Line_Side_ALL_CH1_CH4_Control_P8C();
uint8_t DSP_Line_Side_CDR_Lock();
void DSP_Line_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_Line_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_Line_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable);
void DSP_Line_Side_SNR_LTP_GET(uint8_t Lane_CH,uint8_t PHY_ID);
void DSP_Line_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL);
void DSP_Line_Side_TX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable);
void DSP_Line_Side_RX_Squelch_SET(uint8_t Lane_CH ,uint8_t Enable);
void DSP_Line_Side_PRBS_Checker_Enable(uint16_t Prbs_mode , uint32_t Lane , uint8_t Enable);
void DSP_Line_Side_PRBS_Clear(uint32_t Lane);
uint64_t DSP_Line_Side_PRBS_Get_Error_Count(uint32_t Lane);
uint8_t DSP_Line_Side_Get_Pattern_Type(uint8_t Pattern_Coding);

extern uint16_t DSP_LineSide_SNR;
extern uint16_t DSP_LineSide_LTP;

#endif  /*INC_DSP_LINE_SIDE_H_*/
