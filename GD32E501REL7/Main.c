#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "CMIS_MSA.h"
#include "Master_I2C1_PB34.h"
#include "Master_I2C2_PC78.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "LDD_TX1_TX4.h"
#include "LDD_TX5_TX8.h"
#include "TIA_RX1_RX4.h"
#include "TIA_RX5_RX8.h"

int main(void)
{
    // GD MCU Initialize
    GD32E501_Power_on_Initial();
    // Timer Initialize
    Timer_Initialize();
    // Power on timing control
    PowerOn_Sequencing_Control();
    // Power on HW RESET PIN Check
    PowerOn_Reset_Check();
    // Flash & SRAM Upload
    PowerON_Table_Init();
    // LDD initialize
    LDD_TX1_TX4_PowerOn_Reset();
    LDD_TX5_TX8_PowerOn_Reset();
    LDD_TX1_TX4_PowerOn_init();
    LDD_TX5_TX8_PowerOn_init();
    // TIA initialize
    TIA_RX1_RX4_PowerOn_Reset();
    TIA_RX5_RX8_PowerOn_Reset();
    TIA_RX1_RX4_PowerOn_init();
    TIA_RX5_RX8_PowerOn_init();

    /* configure ADC_CMP NVIC */
    Get_CMP_Current_State();
    nvic_irq_enable(ADC_CMP_IRQn, 2);
    // I2C Enable Control
    i2c_enable(I2C0);
    while(1)
    {
        // MSA StateMachine Control
        QSFPDD_MSA_StateMachine();
        // CMIS DataPath State Machine
        MSA_DataPath_StateMachine();
        // Luxshare-OET internal control
        MCU_READ_WRITE_DEIVCE_COMMAND_CONTROL();
        // Move Sram & Flash read/write
        SRMA_Flash_Function();
        // Report SW and HW IntL Status By all of interrupt Flags
		if(IntL_Trigger_Flag)
		{
			IntL_Function();
			IntL_Trigger_Flag=0;
		}
    }
}

