#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include <stdint.h>
#include "CMIS_MSA.h"
#include "Calibration_Struct.h"
#include "Calibration_Struct_2.h"
#include "GD_FlahMap.h"
#include "Master_I2C1_PB34.h"
#include "MCU_GPIO_Customize_Define.h"
#include "CMIS_Page14h.h"
//--------------------------------------------------------//
// Chip ICs .h                                            //
//--------------------------------------------------------//
#include "DSP.h"
#include "DSP_Line_Side.h"
//------------------------------------------------------------------------------------------//
// State Machine Flags
//------------------------------------------------------------------------------------------//
//PAM4 or NRZ Mode Select
uint8_t Signal_Status=PAM4_to_PAM4;
uint8_t Module_Status = 0;
uint8_t DSP_MODE_SET = Mode4_1x53G_PAM4_1x53G_PAM4_4PORT;
uint8_t Bef_DSP_MODE_SET = 0xFF;
uint8_t DDMI_Trigger_Flag=0;
//-----------------------------------------------------------------------------------------------------//
// Functions
//-----------------------------------------------------------------------------------------------------//
void Get_Staged_Control_Set_0()
{
    uint8_t i;
	// Get Start Mode From AppSelCode1
	// 1 Port 8 Lanes
	if(QSFPDD_A0[89]==0x01)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[17+i]=0x10;
	}
	// 2 Port 4Lanes
	else if(QSFPDD_A0[89]==0x11)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-3
			if(i<4)
				QSFPDD_P10[17+i]=0x10;
			// Lane4-7
			else
				QSFPDD_P10[17+i]=0x18;
		}
	}
	// 4 Port 2Lanes
	else if(QSFPDD_A0[89]==0x55)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-1
			if(i<=2)
				QSFPDD_P10[17+i]=0x10;
			// Lane2-3
			else if(i<=4)
				QSFPDD_P10[17+i]=0x14;
			// Lane4-5
			else if(i<=6)
				QSFPDD_P10[17+i]=0x18;
			// Lane6-7
			else if(i<=8)
				QSFPDD_P10[17+i]=0x1C;
		}
	}
	// 8Port 1Lane
	else if(QSFPDD_A0[89]==0xFF)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[17+i]=(0x10+(i*2));
	}
    // AppSelCode1 not define use default
    else
    {
        for(i=0;i<8;i++)
            QSFPDD_P10[17+i]=0x10;
    }
    // Update Active AppSelCodep[]
	for(i=0;i<8;i++)
    {
        Active_AppSelCode[i]=QSFPDD_P10[17+i];
        // Update Page11h Active AppSelCode
        QSFPDD_P11[78+i]=QSFPDD_P10[17+i];
    }

}
uint8_t Get_Staged_Control_Set_1()
{
    uint8_t i,Supported=1;
	// Get Start Mode From AppSelCode9
	// 1 Port 8 Lanes
	if(QSFPDD_P1[98]==0x01)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[52+i]=0x90;
	}
	// 2 Port 4Lanes
	else if(QSFPDD_P1[98]==0x11)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-3
			if(i<4)
				QSFPDD_P10[52+i]=0x90;
			// Lane4-7
			else
				QSFPDD_P10[52+i]=0x98;
		}
	}
	// 4 Port 2Lanes
	else if(QSFPDD_P1[98]==0x55)
	{
		for(i=0;i<8;i++)
		{
			// Lane0-1
			if(i<=2)
				QSFPDD_P10[52+i]=0x90;
			// Lane2-3
			else if(i<=4)
				QSFPDD_P10[52+i]=0x94;
			// Lane4-5
			else if(i<=6)
				QSFPDD_P10[52+i]=0x98;
			// Lane6-7
			else if(i<=8)
				QSFPDD_P10[52+i]=0x9C;
		}
	}
	// 8Port 1Lane
	else if(QSFPDD_P1[98]==0xFF)
	{
		for(i=0;i<8;i++)
			QSFPDD_P10[52+i]=(0x90+(i*2));
	}
    // Not Define
    else if(QSFPDD_P1[98]==0x00)
    {
        for(i=0;i<8;i++)
            QSFPDD_P10[52+i]=0x00;
        Supported=0;
    }
    return Supported;
}
void Copy_Staged_TRx_Controls()
{
    // Move Staged Control Set 0 TRx Controls to Staged Control Set 1
    // TRx Controls
    for(uint8_t i=0;i<21;i++)
    {
        QSFPDD_P10[60+i]=QSFPDD_P10[25+i];
    }
}
//-----------------------------------------------------------------------------------------------------//
// CMIS State Machine
//-----------------------------------------------------------------------------------------------------//
//------------------------------------------------------//
// Modeule_MGMT_INIT
//------------------------------------------------------//
void Modeule_MGMT_INIT()
{
    uint8_t i;
    // DSP LDD TIA to Low Power Mode
    LPMODE_DSP_High();
    Get_Staged_Control_Set_0();
    if(Get_Staged_Control_Set_1())
    {
        // Move Staged Control Set 0 TRx Controls to Staged Control Set 1
        Copy_Staged_TRx_Controls();
    }
	Module_Status = MODULE_LOW_PWR;
    Module_State(Module_Status);
	//IntL_G_High();
	//IntL_G_Low();
	MSA_DataPathState(DataPathDeactivated);
    // Lane State Not changed on first power on
    QSFPDD_P11[6]=0x00;
    // Trigger to show config status
	//QSFPDD_P10[15]=0x01;
}

//------------------------------------------------------//
// QSFPDD_MSA_StateMachine
//------------------------------------------------------//
void QSFPDD_MSA_StateMachine()
{
	uint8_t i = 0;

	switch( Module_Status )
	{
		// Transient State
		case MODULE_MGMT_INIT:
			 Modeule_MGMT_INIT();
			 break;

		// Steady State
		case MODULE_LOW_PWR:

			 // ResetS T or F Function
			 ResetS();
			 // SW and HW Init Mode can use LowPwrS to check it power up or not
			 if(LowPwrS()==0)
			 {
				 Module_Status = MODULE_PWR_UP ;
				 Module_State(Module_Status);
				 // For DSP_Tx_Los_Check check again to change data path state by tx lol
				 Tx_Los_Enable_Flag=1;
			 }
			 // MSA Host Request function
			 Apply_DataPathInit_Immediate(MODULE_LOW_PWR);
             if(DDMI_Trigger_Flag)
             {
                  Initialize_TRx_DDMI();
                 // Clear all off interrupt flags exclude temp and vcc
                 for(i=0;i<18;i++)
                     Clear_Flag(i+7);
             }
			 break;

		// Transient State
		case MODULE_PWR_UP:

             // DSP LDD TIA to High Power Mode
             LPMODE_DSP_Low();
			 if(Bef_DSP_MODE_SET!=DSP_MODE_SET)
			 {
				 DSP_Init();
				 Bef_DSP_MODE_SET=DSP_MODE_SET;
			 }
			 if(LowPwrS())
			 {
				 Module_Status = MODULE_PWR_DN ;
				 Module_State(Module_Status);
			 }
			 else
			 {
				 Module_Status = MODULE_READY ;
				 Module_State(Module_Status);
				 //IntL_G_High();
				 //IntL_G_Low();
			 }

			 break;

		// Transient State
		case MODULE_PWR_DN:

             // DSP LDD TIA to Low Power Mode
			 LPMODE_DSP_High();
			 Module_Status = MODULE_LOW_PWR ;
			 Module_State(Module_Status);
			 //IntL_G_High();
			 //IntL_G_Low();
             Initialize_TRx_DDMI();
			 // Clear all off interrupt flags exclude temp and vcc
			 for(i=0;i<18;i++)
				 Clear_Flag(i+7);
			 break;

		// Steady State
		case MODULE_READY:
			// LowPwrS for Arista to force to low pow mode
			if((LowPwrExS())||(LowPwrS()))
			{
				 Module_Status = MODULE_PWR_DN ;
				 Module_State(Module_Status);
			}
			else
			{
                // CMIS Controls
                CMIS_PAGE10h_Control();
				CMIS_PAGE13h_Diagnostics_Function();
                CMIS_PAGE14h_Diagnostics_Function();
				// ResetS T or F Function
				ResetS();
				// MSA Host Request function
				Apply_DataPathInit_Immediate(MODULE_READY);
				// Data Path State Machine for DataPathDeactivateS or DataPathReDeinitS
			}
			break;

		case MODULE_FAULT:
			 break;
	}
	MSA_DDMI_Function();
}