/*
 * BRCM_DSP_D87540.h
 *
 *  Created on: 2018Ǿ11ū13ũ
 *      Author: Ivan_Lin
 */

#ifndef INC_DSP_SYSTEM_SIDE_H_
#define INC_DSP_SYSTEM_SIDE_H_
//------------------------------------------------//
//---------------- API----------------------------//
//------------------------------------------------//
void DSP_System_Side_TX_FIR_SET( uint8_t Lane_CH , uint8_t PHY_ID);
void System_Side_ALL_CH1_CH4_Control_P87();
void System_Side_ALL_CH1_CH4_Control_P8E();
void DSP_System_Side_TX_Squelch_SET(uint8_t Channel,uint8_t Control_Data);
void DSP_System_Side_RX_Squelch_SET(uint8_t Channel,uint8_t Control_Data);
void DSP_System_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_System_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable);
void DSP_System_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable);
void DSP_System_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL);
uint8_t DSP_System_Side_CDR_Lock();
void DSP_System_Side_PRBS_Checker_Enable(uint16_t Prbs_mode , uint32_t Lane , uint8_t Enable);
void DSP_System_Side_PRBS_Clear(uint32_t Lane);
uint64_t DSP_System_Side_PRBS_Get_Error_Count(uint32_t Lane);
uint8_t DSP_System_Side_Get_Pattern_Type(uint8_t Pattern_Coding);

#endif  /*INC_DSP_SYSTEM_SIDE_H_*/