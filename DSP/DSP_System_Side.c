#include "gd32e501.h"
#include "core_cm33.h"
#include "systick.h"
#include "string.h"
#include "CMIS_MSA.h"
#include "Master_I2C2_PC78.h"
#include "DSP.h"

//-----------------------------------------------------------------------------------------------------//
// DSP System Side TRX Polarity
// TRX_Side_SEL = 1  System side Tx
// TRX_Side_SEL = 0  System side Rx
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_TRX_Polarity_SET(uint8_t Lane_CH , uint8_t TRX_Side_SEL, uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000002 | (New_Tokan<<16);
    if(Lane_CH==0)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH0);
            else
               COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH0); 
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH0);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH0);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008004 );
    }
    else if(Lane_CH==1)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH1);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH1);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH1);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH1);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008004 );        
    }
    else if(Lane_CH==2)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH2);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH2);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH2);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH2);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008004 );      
    }
    else if(Lane_CH==3)
    {
        if(TRX_Side_SEL)
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_TX_Polarity_CH3);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_TX_Polarity_CH3);
        }
        else
        {
            if(PHY_ID==DSP_PHY_ID_0)
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_RX_Polarity_CH3);
            else
                COMMAND_4700_DATA[1] = 0xCCCC0000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_RX_Polarity_CH3);
        }
        
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008004 );  
    }  
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,PHY_ID);
}

//-----------------------------------------------------------------------------------------------------//
// DSP SystemSide TRX Squelch
// TRX_Side = 1  TX Squelch ( Moudle Rx ouput )
// TRX_Side = 0  RX Squelch
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_TRX_Squelch_SET(uint8_t Lane_CH ,uint8_t TRX_Side,uint8_t Enable, uint8_t PHY_ID)
{  
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000008 | (New_Tokan<<16);
    if(Lane_CH==0)    
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==1)    
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==2)   
    {    
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==3)   
    {      
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==4)    
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==5)   
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==6)   
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==7)   
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008006 );
    }
    else if(Lane_CH==All_CH)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x00008006 );
    }        
     
    if(TRX_Side)
    {
        COMMAND_4700_DATA[1] = 0x00000200;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00000800 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00000800 ;
    }
    else
    {
        COMMAND_4700_DATA[1] = 0x00000400;
        if(Enable)
            COMMAND_4700_DATA[2] = 0x00000000 | 0x00001000 ;
        else
            COMMAND_4700_DATA[2] = 0x00000000 & ~0x00001000 ;
    }  
    
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,PHY_ID);
}

//-----------------------------------------------------------------------------------------------------//
// DSP SystemSide Digital/Remote Loopback function
// LoopBack_mode = 1  Digital Loopback
// LoopBack_mode = 0  Remote Loopbcak
//-----------------------------------------------------------------------------------------------------//
void DSP_SystemSide_Loopback_SET(uint8_t LoopBack_mode,uint8_t Lane_CH,uint8_t Enable, uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    COMMAND_4700_DATA[0] = 0x00000002 | (New_Tokan<<16);
    if(Lane_CH==0)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==1)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==2)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800A );
    }
    else if(Lane_CH==3)
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800A );
    }
     //Digital Loopback
    if(LoopBack_mode==1)
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0000 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0000 & ~0x0100;
    }
    //Remote Loopback
    else
    {
        if(Enable)
            COMMAND_4700_DATA[1]  = 0xCCCC0001 | 0x0100 ;
        else
            COMMAND_4700_DATA[1]  = 0xCCCC0001 & ~0x0100 ;
    }
           
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,PHY_ID);
}
//-----------------------------------------------------------------------------------------------------------------//
//------------------------------------------------- API------------------------------------------------------------//
//-----------------------------------------------------------------------------------------------------------------//

//-----------------------------------------------------------------------------------------------------//
// DSP System Side TX FIR Setting
//-----------------------------------------------------------------------------------------------------//
void DSP_System_Side_TX_FIR_SET( uint8_t Lane_CH , uint8_t PHY_ID)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(PHY_ID);
	GET_DSP_FW_VERSION(PHY_ID);
    if(DSP_FW_Version>=0xD00B)
    {
        COMMAND_4700_DATA[0]   = 0x00000040  | (New_Tokan<<16);
        COMMAND_4700_DATA[16]  = 0x00000000 ;
    }
    else
    {
        COMMAND_4700_DATA[0]  = 0x0000003C  | (New_Tokan<<16);
    }
    COMMAND_4700_DATA[1]  = 0x00000002 ;
    COMMAND_4700_DATA[2]  = 0x00000060 ;
    if(Lane_CH==0)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH0) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH0) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH0) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH0) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH0) ;
        }
        else
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH0) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH0) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH0) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH0) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH0) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH0) ;
        }
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==1)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH1) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH1) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH1) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH1) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH1) ;
        }
        else
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH1) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH1) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH1) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH1) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH1) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH1) ;
        }
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==2)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH2) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH2) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH2) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH2) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH2) ;
        }
        else
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH2) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH2) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH2) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH2) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH2) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH2) ;
        }
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x00008012 );
    }
    else if(Lane_CH==3)
    {
        if(PHY_ID==DSP_PHY_ID_0)
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_Main_CH3) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_PRE1_CH3) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST2_CH3) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST1_CH3) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY0_MEMORY_MAP.System_Side_POST3_CH3) ;
        }
        else
        {
            if(Signal_Status==PAM4_to_PAM4)
                //TXFIR_TAPS_PAM4_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_PAM4_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
            else
                //TXFIR_TAPS_NRZ_6TAP
                COMMAND_4700_DATA[3]  = 0x00000000 | TXFIR_TAPS_NRZ_6TAP
                                                   | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE2_CH3) << 16 ;
            
            COMMAND_4700_DATA[4]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_Main_CH3) << 16  
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_PRE1_CH3) ;
            COMMAND_4700_DATA[5]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST2_CH3) << 16 
                                               | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST1_CH3) ;
            COMMAND_4700_DATA[6]  = 0x00000000 | Swap_Bytes(DSP_System_Side_PHY1_MEMORY_MAP.System_Side_POST3_CH3) ;
        }
        COMMAND_4700_DATA[7]  = 0x00000000 ;
        COMMAND_4700_DATA[8]  = 0x00000000 ;
        COMMAND_4700_DATA[9]  = 0x00000000 ;
        COMMAND_4700_DATA[10] = 0x00000000 ;
        COMMAND_4700_DATA[11] = 0x00000000 ;
        COMMAND_4700_DATA[12] = 0x00000000 ;
        COMMAND_4700_DATA[13] = 0x00000000 ;
        COMMAND_4700_DATA[14] = 0x00000000 ;
        COMMAND_4700_DATA[15] = 0x00000000 ;
        COMMAND_4700_DATA[16] = 0x00000000 ;
        COMMAND_4700_DATA[17] = 0x00000000 ;
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x00008012 );
    }
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,PHY_ID);
    DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,PHY_ID);
}

//----------------------------------------------------------------------------------//
// DSP System_Side_ALL_CH1_CH4_Control_P87
//----------------------------------------------------------------------------------//
void System_Side_ALL_CH1_CH4_Control_P87()
{
    DSP_SystemSide_TRX_Polarity_SET( 0 , TX_Side , DSP_PHY_ID_0);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , TX_Side , DSP_PHY_ID_0);
    DSP_SystemSide_TRX_Polarity_SET( 2 , TX_Side , DSP_PHY_ID_0);
    DSP_SystemSide_TRX_Polarity_SET( 3 , TX_Side , DSP_PHY_ID_0);
    
    DSP_SystemSide_TRX_Polarity_SET( 0 , RX_Side , DSP_PHY_ID_0);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , RX_Side , DSP_PHY_ID_0);
    DSP_SystemSide_TRX_Polarity_SET( 2 , RX_Side , DSP_PHY_ID_0);
    DSP_SystemSide_TRX_Polarity_SET( 3 , RX_Side , DSP_PHY_ID_0);
    
    DSP_System_Side_TX_FIR_SET( 0 , DSP_PHY_ID_0);
    DSP_System_Side_TX_FIR_SET( 1 , DSP_PHY_ID_0);
    DSP_System_Side_TX_FIR_SET( 2 , DSP_PHY_ID_0);
    DSP_System_Side_TX_FIR_SET( 3 , DSP_PHY_ID_0);
    
}

//----------------------------------------------------------------------------------//
// DSP System_Side_ALL_CH5_CH8_Control_P8E
//----------------------------------------------------------------------------------//
void System_Side_ALL_CH1_CH4_Control_P8E()
{
    DSP_SystemSide_TRX_Polarity_SET( 0 , TX_Side , DSP_PHY_ID_1);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , TX_Side , DSP_PHY_ID_1);
    DSP_SystemSide_TRX_Polarity_SET( 2 , TX_Side , DSP_PHY_ID_1);
    DSP_SystemSide_TRX_Polarity_SET( 3 , TX_Side , DSP_PHY_ID_1);
    
    DSP_SystemSide_TRX_Polarity_SET( 0 , RX_Side , DSP_PHY_ID_1);   
    DSP_SystemSide_TRX_Polarity_SET( 1 , RX_Side , DSP_PHY_ID_1);
    DSP_SystemSide_TRX_Polarity_SET( 2 , RX_Side , DSP_PHY_ID_1);
    DSP_SystemSide_TRX_Polarity_SET( 3 , RX_Side , DSP_PHY_ID_1);
    
    DSP_System_Side_TX_FIR_SET( 0 , DSP_PHY_ID_1);
    DSP_System_Side_TX_FIR_SET( 1 , DSP_PHY_ID_1);
    DSP_System_Side_TX_FIR_SET( 2 , DSP_PHY_ID_1);
    DSP_System_Side_TX_FIR_SET( 3 , DSP_PHY_ID_1);
    
}

//----------------------------------------------------------------------------------//
// DSP System_Side_TRX_Squelch_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_TX_Squelch_SET(uint8_t Channel,uint8_t Control_Data)
{
    if(Channel==All_CH)
    {
        DSP_SystemSide_TRX_Squelch_SET( All_CH , TX_Side , Control_Data , DSP_PHY_ID_0);
        DSP_SystemSide_TRX_Squelch_SET( All_CH , TX_Side , Control_Data , DSP_PHY_ID_1);
    }
    else if(Channel<4)
        //Lane0 to Lane3 Control
        DSP_SystemSide_TRX_Squelch_SET( Channel , TX_Side , Control_Data , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_SystemSide_TRX_Squelch_SET( (Channel-4) , TX_Side , Control_Data , DSP_PHY_ID_1);
}

void DSP_System_Side_RX_Squelch_SET(uint8_t Channel,uint8_t Control_Data)
{
    if(Channel<4)
        //Lane0 to Lane3 Control
        DSP_SystemSide_TRX_Squelch_SET( Channel , RX_Side , Control_Data , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_SystemSide_TRX_Squelch_SET( (Channel-4) , RX_Side , Control_Data , DSP_PHY_ID_1);
}

//----------------------------------------------------------------------------------//
// DSP System_Side_CDR Lock
//----------------------------------------------------------------------------------//
uint8_t DSP_System_Side_CDR_Lock()
{
    uint8_t LOL_STATUS = 0x00;
    
    // TX LOL bit0 - bit3, Lock:1,nolock:0
    // System Side Input
    //Get Lane4-Lane7 LOL Data
    LOL_STATUS = (~BRCM_Control_READ_Data(0x5201CA38,DSP_PHY_ID_1))&0x0F;
    LOL_STATUS = (LOL_STATUS<<4);
    //Get Lane0-Lane3 LOL Data
    LOL_STATUS |= (~BRCM_Control_READ_Data(0x5201CA38,DSP_PHY_ID_0))&0x0F;
    
    return LOL_STATUS;
}

void DSP_System_Side_LOS_LOL(uint8_t *LOS,uint8_t *LOL)
{
    uint16_t LOL_LOS_STATUS,CDR_LOL_STAUTS;
    uint32_t Data_Buffer;
    // Read CH0-CH3 LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA68,DSP_PHY_ID_0);
    // LOS Bit0-7
    *LOS=LOL_LOS_STATUS&0x000F;
    // LOL Bit8-15
    *LOL=(LOL_LOS_STATUS>>8)&0x000F;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA38,DSP_PHY_ID_0);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA70,Data_Buffer,BRCM_Default_Length,DSP_PHY_ID_0);
        BRCM_Control_READ_Data (0x5201CA68,DSP_PHY_ID_0);
        BRCM_Control_READ_Data (0x5201CA38,DSP_PHY_ID_0);
        BRCM_Control_WRITE_Data(0x5201CA70,0x00000000,BRCM_Default_Length,DSP_PHY_ID_0);
    }
    // Read CH4-CH7 LOL LOS Status
    LOL_LOS_STATUS=BRCM_Control_READ_Data(0x5201CA68,DSP_PHY_ID_1);
    // LOS Bit0-7
    *LOS|=(LOL_LOS_STATUS&0x000F)<<4;
    // LOL Bit8-15
    *LOL|=((LOL_LOS_STATUS>>8)&0x000F)<<4;
     /* Read the CDR LOL status */
    CDR_LOL_STAUTS=BRCM_Control_READ_Data(0x5201CA38,DSP_PHY_ID_1);
    CDR_LOL_STAUTS=CDR_LOL_STAUTS<<8;
    Data_Buffer=CDR_LOL_STAUTS&(LOL_LOS_STATUS&0x0F00);
    if(Data_Buffer>0)
    {
        BRCM_Control_WRITE_Data(0x5201CA70,Data_Buffer,BRCM_Default_Length,DSP_PHY_ID_1);
        BRCM_Control_READ_Data (0x5201CA68,DSP_PHY_ID_1);
        BRCM_Control_READ_Data (0x5201CA38,DSP_PHY_ID_1);
        BRCM_Control_WRITE_Data(0x5201CA70,0x00000000,BRCM_Default_Length,DSP_PHY_ID_1);
    }
}

//----------------------------------------------------------------------------------//
// DSP System_Side_Loopback_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_Digital_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH<4)
        //Lane0 to Lane3 Control
        DSP_SystemSide_Loopback_SET( Digital_LoopBack_mode , Lane_CH , Enable , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_SystemSide_Loopback_SET( Digital_LoopBack_mode , (Lane_CH-4) , Enable , DSP_PHY_ID_1);
}

void DSP_System_Side_Remote_Loopback_SET(uint8_t Lane_CH,uint8_t Enable)
{
    if(Lane_CH<4)
        //Lane0 to Lane3 Control
        DSP_SystemSide_Loopback_SET( Remote_Loopback_mode , Lane_CH , Enable , DSP_PHY_ID_0);
    else
        //Lane4 to Lane7 Control
        DSP_SystemSide_Loopback_SET( Remote_Loopback_mode , (Lane_CH-4) , Enable , DSP_PHY_ID_1);
}
//----------------------------------------------------------------------------------//
// DSP System_Side_PRBS_Control
//----------------------------------------------------------------------------------//
void DSP_System_Side_PRBS_SET(uint8_t Pattern ,uint8_t Lane_CH ,uint8_t Enable)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000002C | (New_Tokan<<16);
    //Lane Select
    if((Lane_CH==0)||(Lane_CH==4))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000001 , 0x5201CA84 , 0x0000800C );
    }
    else if((Lane_CH==1)||(Lane_CH==5))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000002 , 0x5201CA84 , 0x0000800C );
    }
    else if((Lane_CH==2)||(Lane_CH==6))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000004 , 0x5201CA84 , 0x0000800C );
    }
    else if((Lane_CH==3)||(Lane_CH==7))
    {
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000008 , 0x5201CA84 , 0x0000800C );
    }
    
    //PRBS Generator
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[1]  = 0x00000001;
    else
        COMMAND_4700_DATA[1]  = 0x00000004;
    
    //PRBS Enable
    if(Enable)
        COMMAND_4700_DATA[2]  = 0x00000001;
    else
        COMMAND_4700_DATA[2]  = 0x00000000;
    
    //PRBS Pattern
    if(Pattern!=SSPRQ_SystemSide)
        COMMAND_4700_DATA[3] = 0x00000000 | (Pattern<<8);
    //SSPRQ
    else
        COMMAND_4700_DATA[3] = 0x00000000;
    
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    
    //Lane0-Lane3
    if(Lane_CH<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);   
    }
    //Lane4-Lane7
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);   
    }
}

//----------------------------------------------------------------------------------//
// DSP System_Side_PRBS_Checker
//----------------------------------------------------------------------------------//
void DSP_System_Side_PRBS_Checker_Enable(uint16_t Prbs_mode , uint32_t Lane , uint8_t Enable)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x0000002C | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    
    if(Enable)
        COMMAND_4700_DATA[2] = 0x00000001;
    else
        COMMAND_4700_DATA[2] = 0x00000000;
    // Prbs mode
    // [15:7]        
    // 0:prbs7 10:Prbs13 3:prbs15 4:prbs23 5:prbs31 
    COMMAND_4700_DATA[3] = (0x00000000 | Prbs_mode<<8 );
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    COMMAND_4700_DATA[7] = 0x00000000;
    COMMAND_4700_DATA[8] = 0x00000000;
    COMMAND_4700_DATA[9] = 0x00000000;
    COMMAND_4700_DATA[10] = 0x00000000;
    COMMAND_4700_DATA[11] = 0x00000000;
    // set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane==All_CH)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x0000800C );
        // DSP Lane0 - Lane3
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);   
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        // DSP Lane4 - Lane7
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);      
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);
    }
    // DSP Lane0 - Lane3
    else if(Lane<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000 | (0x01<<Lane) , 0x5201CA84 , 0x0000800C );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);   
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);
    }
    // DSP Lane4 - Lane7
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000 | (0x01<<(Lane-4)) , 0x5201CA84 , 0x0000800C );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);      
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);
    }
}

void DSP_System_Side_PRBS_Clear(uint32_t Lane)
{
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    COMMAND_4700_DATA[0] = 0x00000018 | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    // set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane==All_CH)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x0000800E );
        // DSP Lane0 - Lane3 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        // DSP Lane4 - Lane7 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);
    }
    else if(Lane==CH_03)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x0000800E );
        // DSP Lane0 - Lane3 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);
    }
    else if(Lane==CH_47)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x0000000F , 0x5201CA84 , 0x0000800E );
        // DSP Lane4 - Lane7 
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);
    }
    // DSP Lane0 - Lane3
    else if(Lane<4)
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_0);
        GET_DSP_FW_VERSION(DSP_PHY_ID_0);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000 | (0x01<<Lane) , 0x5201CA84 , 0x0000800E );
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_0);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_0);
    }
    // DSP Lane4 - Lane7 
    else
    {
        // Get PHY Chip ID for sync
        DSP_GET_CHIP_ID(DSP_PHY_ID_1);
        GET_DSP_FW_VERSION(DSP_PHY_ID_1);
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000 | (0x01<<(Lane-4)) , 0x5201CA84 , 0x0000800E );
        // Write command to dsp fw command control
        DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,DSP_PHY_ID_1);    
        DSP87580_CAPI_Command_response(0x5201CA8C,0x5201CA90,DSP_PHY_ID_1);
    }
}

uint64_t DSP_System_Side_PRBS_Get_Error_Count(uint32_t Lane)
{
    uint64_t GetErrorCount=0;
    uint32_t ReadBuffer[7]={0};
    uint64_t GetMSB_EC=0;
    uint64_t GetLSB_EC=0;
    uint8_t  SEL_PHY=0;
    uint32_t New_Tokan;
    intf_util_assign_tocken(&New_Tokan);
    // PHY Select
    if(Lane<4)
        SEL_PHY=DSP_PHY_ID_0;
    else
        SEL_PHY=DSP_PHY_ID_1;
    COMMAND_4700_DATA[0] = 0x00000018 | (New_Tokan<<16);
    COMMAND_4700_DATA[1] = 0x00000002;
    COMMAND_4700_DATA[2] = 0x00000000;
    COMMAND_4700_DATA[3] = 0x00000000;
    COMMAND_4700_DATA[4] = 0x00000000;
    COMMAND_4700_DATA[5] = 0x00000000;
    COMMAND_4700_DATA[6] = 0x00000000;
    // Get PHY Chip ID for sync
    DSP_GET_CHIP_ID(SEL_PHY);
    GET_DSP_FW_VERSION(SEL_PHY);
    //Set DSP_READ_ADDRESS_0 DSP_SET_DATA_0 DSP_READ_ADDRESS_1 DSP_SET_DATA_1 to sram
    if(Lane<4)
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000|(0x01<<Lane) , 0x5201CA84 , 0x0000800F );
    else
        DSP87580_CAPI_CW_CMD( 0x5201CA88 , 0x00000000|(0x01<<(Lane-4)) , 0x5201CA84 , 0x0000800F );
    // Write command to dsp fw command control
    DSP87580_PAM4_COMMAND_ID_SET_CONFIG_INFO_CORE_IP_CW(0x5201CA84,SEL_PHY);     
    // INTF_CAPI2FW_HOST_CMD_RESPONSE_GPREG : cmd_response_address
    ReadBuffer[0] = BRCM_Control_READ_Data(0x5201CA8C,SEL_PHY);
    // INTF_CAPI2FW_HOST_RSP_LANE_MASK_GPREG : rsp_lane_mask_address
    ReadBuffer[1] = BRCM_Control_READ_Data(0x5201CA90,SEL_PHY);
    ReadBuffer[2] = BRCM_Control_READ_Data(0x00047400,SEL_PHY);
    // CAPI_PRBS_MONITOR
    ReadBuffer[3] = BRCM_Control_READ_Data(0x00047404,SEL_PHY);
    // lock
    ReadBuffer[4] = BRCM_Control_READ_Data(0x00047408,SEL_PHY);
    // lock loss
    ReadBuffer[5] = BRCM_Control_READ_Data(0x0004740C,SEL_PHY); 
    ReadBuffer[6] = BRCM_Control_READ_Data(0x00047410,SEL_PHY);  
    // Get Error count  
    GetLSB_EC = BRCM_Control_READ_Data(0x00047414,SEL_PHY); 
    GetMSB_EC = BRCM_Control_READ_Data(0x00047418,SEL_PHY); 
    
    BRCM_Control_WRITE_Data(0x5201CA84,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA94,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA74,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA8C,0x00000000,BRCM_Default_Length,SEL_PHY);
    BRCM_Control_WRITE_Data(0x5201CA90,0x00000000,BRCM_Default_Length,SEL_PHY);
    
    if(ReadBuffer[4]==1)
        GetErrorCount = (GetMSB_EC<<32|GetLSB_EC);
    else
        GetErrorCount = 0xFFFFFFFFFFFFFFFF;
    
    return GetErrorCount;
}

uint8_t DSP_System_Side_Get_Pattern_Type(uint8_t Pattern_Coding)
{
    uint8_t PRBS_TYPE=0xFF;
    
    switch(Pattern_Coding)
    {
        // PAM4
        case PRBS_31Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS31Q_SystemSide;
                        break;
        case PRBS_23Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS23Q_SystemSide;
                        break;
        case PRBS_15Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS15Q_SystemSide;
                        break;
        case PRBS_13Q :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS13Q_SystemSide;
                        break;
        // not support it
        case PRBS_9Q  :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS9Q_SystemSide;
                        break;
        // not support it
        case PRBS_7Q  :
                        if(Signal_Status==PAM4_to_PAM4)
                            PRBS_TYPE=PRBS7Q_SystemSide;
                        break;
        // NRZ
        case PRBS_31  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS31Q_SystemSide;
                        break;
        case PRBS_23  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS23Q_SystemSide;
                        break;
        case PRBS_15  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS15Q_SystemSide;
                        break;
        case PRBS_13  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS13Q_SystemSide;
                        break;
        case PRBS_9  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS9Q_SystemSide;
                        break;
        case PRBS_7  :
                        if(Signal_Status==NRZ_to_NRZ)
                            PRBS_TYPE=PRBS7Q_SystemSide;
                        break;        
        default      :
                        break;
    }
    return PRBS_TYPE;
}
